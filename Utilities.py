#!/usr/bin/env python

# Version 1.5
# 21-DEC-2016

import os.path
import sys
import time
import RPi.GPIO as GPIO
from smbus import SMBus
import serial
import spidev
from subprocess import call
import datetime
import wave
import thread
import subprocess
import shlex
import gc
import shutil
from array import *
from abc import ABCMeta, abstractmethod

execfile ("Parameters.py")


######################################
## 		 Standalone Functions 		##
######################################

# Log functions
def create_log_file():
	global log_file
	LOG_FILENAME = "Log_VT_"+datetime.datetime.today().strftime('%d_%m_%Y__%H_%M_%S_%f')+'.txt'
	if not os.path.isdir ("Log") :
		os.makedirs ("Log")
		shell_command("sudo chown pi:pi Log")
	log_file = open('Log/'+LOG_FILENAME, "a")
	
def create_debug_log_file():
	global debug_log_file
	LOG_FILENAME = "Digital_recording_"+datetime.datetime.today().strftime('%d_%m_%Y__%H_%M_%S_%f')+'.bin'
	if not os.path.isdir ("Log") :
		os.makedirs ("Log")
		shell_command("sudo chown pi:pi Log")
	debug_log_file = open('Log/'+LOG_FILENAME, "w+b")	

	
def close_file (file_name):
	file_name.close()
				
def write_to_file(time, user_input):
	global log_file
	log_file.write("%s\t" %time)
	log_file.write("%s\n" %user_input)

def write_to_debug_file(user_input):
	global debug_log_file
	debug_log_file.write(user_input)

def write_to_log(user_input):
	time = datetime.datetime.today().strftime('%d/%m/%Y %H:%M:%S:%f')
	print time, '\t', user_input
	write_to_file(time, user_input)

def set_date_time(user_input):
	arr = user_input.split('_')
	arr= filter(None, arr)		# remove empty cells from array	
	for i in range (0, len(arr)):
		arr[i] = int(arr[i])

	current_date_time = datetime.datetime.today()
	new_date = current_date_time.replace(arr[2],arr[1],arr[0],arr[3],arr[4],arr[5],arr[6])
	new_date = new_date+ datetime.timedelta(seconds=1)
	subprocess.call (['sudo', 'date', '-s',new_date.ctime()])
	#subprocess.call (['sync'])
	time.sleep(0.1)


# Statistics functions	
def enable_statistic():
	global STATISTIC
	STATISTIC = True
	write_to_log("Statistics enabled")

def disable_statistic():
	global STATISTIC
	STATISTIC = False
	write_to_log("Statistics disabled")
	

# Audio functions
def convert_to_wav(source_file_name, sample_rate, number_of_channels, output_file_name=WAV_AUDIO_FILENAME):
	if not (os.path.isfile(source_file_name)):
		write_to_log("File " + source_file_name + " not exist")
		return
	pcmfile = open (source_file_name ,'rb')
	pcmdata = pcmfile.read()
	wavfile = wave.open(output_file_name ,'wb')
	sample_rate = int(sample_rate)
	number_of_channels = int(number_of_channels)
	wavfile.setparams((number_of_channels, 2, sample_rate, 0, 'NONE', 'NONE'))
	wavfile.writeframes(pcmdata)
	wavfile.close()
	pcmfile.close()
	write_to_log("Done convert to wav")

def play_audio (cardID, wave_filename, volume=100, run_in_background=True):
	
	global music_on
	global song_completed
	
	if (os.path.isfile(wave_filename)):
		#~ write_to_log ("Playing " + wave_filename)
		
		if (run_in_background):
			character = "&"
		else:
			character = ""
		
		if (type(cardID) != str):
			cardID = str(cardID)
			
		if (cardID == "0"):
			shell_command ("amixer -q sset PCM,0 " + str(volume) + "%") # set the volume
			time.sleep(0.05)
			shell_command ("aplay -q -D hw:0,0 " + str(wave_filename) + character) # play the audio file
		else:
			shell_command ("amixer -q sset -c 1 'Master' " + str(volume))
			time.sleep(0.05)
			shell_command ("aplay -D " + str(cardID) + " " + str(wave_filename) + character) # play the audio file
			time.sleep(0.2)
			music_on = True
			song_completed = False
	else:
		write_to_log ("File " + wave_filename + " not found")
	
def stop_audio ():
	global music_on
	global song_completed
	
	write_to_log ("Stopping playback")
	music_on = False
	song_completed = True
	shell_command ("sudo pkill -f aplay")
	time.sleep(0.5)


# External values configuration functions
def import_values_from_file(values_filename):
	if (os.path.isfile(values_filename) == False):
		write_to_log ("File " + values_filename + " not exist !")
		return
		
	global LIST_OF_VALUES
	with open(values_filename) as f:
		for line in f:
			if line.strip() == '': 		 # check if the line is empty
				continue
			line = line.rstrip('\n') 	 # remove '\n'
			line = line.replace(' ', '') # remove blanks
			(key, val) = line.split('=')
			LIST_OF_VALUES[key] = val	
	write_to_log ("Loaded values from file " + values_filename + " successfully")

def show_values():
	write_to_log("{:<15} {:<12}".format('Register', 'Value'))
	for item in LIST_OF_VALUES.iteritems():
		reg, val = item
		write_to_log("{:<15} {:<12}".format(reg, val)) 


# Checksum functions
def get_value_from_bytes(bytes_in_list):
    result = 0
    number_of_bytes = len (bytes_in_list) -1
    while (number_of_bytes >= 0):
		single_byte = bytes_in_list[number_of_bytes] << ((number_of_bytes)*8)
		result = result | single_byte	
		number_of_bytes -=1
		
    return result
	
def read_checksum_from_fw_file(file_name):
	infile = open(file_name, "rb")
	list_file = list(infile.read())
	j=len(list_file) - 4
	checksum_data = []
	for i in xrange(0, 4):
		checksum_data.append(str(hex(ord(list_file[j+i]))))
	return checksum_data
		
def calc_checksum(file_name, unsent_bytes):
	infile = open(file_name,"rb")
	list_file_temp = list(infile.read())
	list_file = [ord(i) for i in list_file_temp]

	i=0
	sum_value=0
	while(i < len(list_file) - unsent_bytes) :
		current_byte = hex(list_file[i])
		next_byte = hex(list_file[i+1])

		if ((current_byte == '0x5a') and ((next_byte == '0x2') or (next_byte == '0x1'))):
			# header field
			for j in xrange(0, 2):
				header = list_file[i+j]
				sum_value+=header
			i+=2

			# number of words field
			num_of_words = get_value_from_bytes(list_file[i:(i+4)])
			sum_value += num_of_words
			i+=4

			# address field
			address = get_value_from_bytes(list_file[i:(i+4)])
			sum_value += address
			i+=4

			# data words
			while (j < num_of_words*2):
				data = get_value_from_bytes(list_file[i:(i+2)])
				sum_value += data
				j+=2
				i+=2
        
		else:
			break
    
	sum_value += 104 # add the checksum value to the total sum : 0x5A + 0x0E = 0x68 = 104
	sum_value = str(hex(sum_value))[2:].zfill(8)
	
	sum_value_list = []
	j=8
	while (j>0):
		string = hex(int(sum_value [j-2:j] ,16))
		sum_value_list.append(string)
		j-=2

	return sum_value_list
	

# General functions
def blink_LED(times, end):
	count = 0
	while count < times:
		GPIO.output(LED_GPIO,True)
		time.sleep(0.1)
		GPIO.output(LED_GPIO,False)
		time.sleep(0.1)
		count = count + 1
	if end == 1:
		GPIO.output(LED_GPIO,True)
	else:
		GPIO.output(LED_GPIO,False)

def shell_command(user_command):
	try:
		#write_to_log ('About to run shell command: "' + user_command + '"')
		os.system (user_command)
		#subprocess.Popen(shlex.split(user_command)).wait()
	except:
		write_to_log ('command "' + user_command + '" not found')

def create_acoustic_model(am_file, search_file):
	if not (os.path.isfile(am_file)):
		write_to_log("File "+ am_file +" not exist")
		return 0
	
	if not (os.path.isfile(search_file)):
		write_to_log("File "+ search_file +" not exist")
		return 0
		
	if os.path.exists('./A-Model.bin'):
		shutil.copyfile('A-Model.bin', 'A-Model_old.bin')
	
	#Copying search_file.bin + header
	gram_size = os.path.getsize(search_file)
	hex_array = array('H')
	hex_array.append(int('025a',16))
	hex_array.append (gram_size/2 & 0xffff)
	hex_array.append ((gram_size/2 >> 16) & 0xffff)
	hex_array.append (int('0001',16))
	hex_array.append (int('0000',16))
	f = file('A-Model.bin', 'wb')
	hex_array.tofile(f)
	with open(search_file, 'rb') as g:
		f.write (g.read())
	f.close()

	#Copying am_file.bin + header
	net_size = os.path.getsize(am_file)
	hex_array = array('H')
	hex_array.append(int('025a',16))
	hex_array.append (net_size/2 & 0xffff)
	hex_array.append ((net_size/2 >> 16) & 0xffff)
	hex_array.append (int('0002',16))
	hex_array.append (int('0000',16))
	f = file('A-Model.bin', 'ab')
	hex_array.tofile(f)
	with open(am_file, 'rb') as g:
		f.write (g.read())
	f.close()

	# Adding run code

	hex_array = array('H')
	hex_array.append(int('0b5a',16))
	f = file('A-Model.bin', 'ab')
	hex_array.tofile(f)
	
	write_to_log ("A new acoustic model \"A-Model.bin\" was generated")
	return 1

def stop_streaming():
	global STOP_STREAMING_FLAG
	STOP_STREAMING_FLAG = True
	write_to_log("Stop streaming")

def swap_bytes(word):
	previous_LSB = (word & 0xff)
	previous_MSB = (word & 0xff00) >> 8
	Swapped_word = (previous_LSB << 8) + previous_MSB
	return Swapped_word;

def write_to_I2C_bus(i2c_address, reg_address, reg_value):
	
	if (type(i2c_address) == str):
		i2c_address = int(i2c_address, 16)

	if (type(reg_address) == str):
		reg_address = int(reg_address, 16)
		
	if (type(reg_value) == str):
		reg_value = int(reg_value, 16)
	
	i2c.write_byte_data (i2c_address , reg_address, reg_value)
	write_to_log("Writing to I2C address: " + hex(i2c_address) + ";  register address: " + hex(reg_address) + " ;  value: " + hex(reg_value))
	time.sleep(0.005)
	
def read_from_I2C_bus(i2c_address, reg_address):
	if (type(i2c_address) == str):
		i2c_address = int(i2c_address, 16)

	if (type(reg_address) == str):
		reg_address = int(reg_address, 16)

	reg_value = i2c.read_byte_data (i2c_address , reg_num)
	write_to_log("Reading I2C address: " + hex(i2c_address) + ";  register number: " + hex(reg_address) + " ;  value: " + hex(reg_value))
	return reg_value

def set_default_wakeup(bool_value):
	global USE_WAKEUP
	if (isinstance (bool_value, bool)):
		USE_WAKEUP = bool_value
	else:
		if ((str(bool_value)).upper() == "FALSE" or str(bool_value) == "0") :
			USE_WAKEUP = False
		else:
			USE_WAKEUP = True
	write_to_log("Current \"USE_WAKEUP\" = " + str(USE_WAKEUP))

def get_default_wakeup():
	write_to_log("Current \"USE_WAKEUP\" = " + str(USE_WAKEUP))
	return USE_WAKEUP

def set_default_sleep_time(time):
	global DEFAULT_SLEEP_TIME
	if ((type(time) == str) or ((type(time) == int))):
		time = float(time)
	DEFAULT_SLEEP_TIME = time
	print "Type:", type(DEFAULT_SLEEP_TIME)
	write_to_log("Current \"DEFAULT_SLEEP_TIME\" = " + str(DEFAULT_SLEEP_TIME))

def get_default_sleep_time():
	write_to_log("Current \"DEFAULT_SLEEP_TIME\" = " + str(DEFAULT_SLEEP_TIME))
	return DEFAULT_SLEEP_TIME

def show_debug_messages():
	global SHOW_DEBUG_MESSAGES
	SHOW_DEBUG_MESSAGES = True
