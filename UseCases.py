
execfile ("Driver.py")
def init_d4():
	
	disable_statistic()
	Driver.WriteRegisterShort(dbmd4,HOST_INTERFACE_SUPPORT,"1020")		
	Driver.ReadRegister(dbmd4,"0")     	          	
	Driver.WriteRegister(dbmd4,MASTER_CLOCK_FREQUENCY,"6000")
	Driver.WriteRegister(dbmd4,UART_BAUD_RATE,"7530")				
	Driver.WriteRegisterShort(dbmd4,GENERAL_CONFIGURATION_1,"1086")	      		
	Driver.WriteRegisterShort(dbmd4,GENERAL_CONFIGURATION_2,"0000")       		
	Driver.WriteRegister(dbmd4,PHRASE_DETECTION_GPIO_CONFIGURATION,"9090")			
	Driver.WriteRegister(dbmd4,AUDIO_BUFFER_READOUT_CONFIGURATION,"0002")	
	Driver.WriteRegisterShort(dbmd4,DSP_CLOCK_CONFIGURATION,"3000",0.01)			
	### DEBUG
	if (DEBUG_MSGS):
		Driver.WriteRegisterShort(dbmd4,"18","0005")	
	if (STREAMING == False):
		Driver.WriteRegister(dbmd4,"17","0008")			
	Driver.WriteRegisterShort(dbmd4, AUDIO_BUFFER_SIZE, "1000")
	write_to_log("Init D4 ended")	
	
def init_d2():

	Driver.WriteRegisterShort(dbmd2,HOST_INTERFACE_SUPPORT,"1070")	
	Driver.ReadRegister(dbmd2,"0")     	          	
	Driver.WriteRegister(dbmd2,MASTER_CLOCK_FREQUENCY,"6000")
	Driver.WriteRegister(dbmd2,UART_BAUD_RATE,"7530")			
	Driver.WriteRegisterShort(dbmd2,GENERAL_CONFIGURATION_1,"0000")	      		
	Driver.WriteRegisterShort(dbmd2,GENERAL_CONFIGURATION_2,"0200")       
	Driver.WriteRegisterShort(dbmd2,DSP_CLOCK_CONFIGURATION,"c001",0.005)	
	### DEBUG
	if (DEBUG_MSGS):
		Driver.WriteRegisterShort(dbmd2,"18","0005")
	Driver.WriteRegisterShort(dbmd2, AUDIO_BUFFER_SIZE, "0020")
	write_to_log("Init D2 ended")	

def activate_tdms(tdm_activation_d2,tdm_activation_d4):
	
	## Enable TDM and moving To Detection	
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,str(tdm_activation_d4))
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,str(tdm_activation_d2))
	
def change_clock_source(freq,bit_depth):

	d2_clock = 294912
	d4_clock = 73728
	m_clck = 24576

	sclck = freq * bit_depth * 2

	if (sclck != 0):
		d2_pll_factor = d2_clock / sclck
		d4_pll_factor = d4_clock / sclck
		
		Driver.WriteRegisterShort(dbmd2,TDM_SCLK_CLOCK_FREQUENCY,hex(sclck))
		Driver.WriteRegisterShort(dbmd2,DSP_CLOCK_CONFIGURATION_EXTENSION, hex(int(0xd000) + d2_pll_factor))
		Driver.WriteRegisterShort(dbmd4,TDM_SCLK_CLOCK_FREQUENCY,hex(sclck))
		Driver.WriteRegisterShort(dbmd4,DSP_CLOCK_CONFIGURATION_EXTENSION,hex(int(0xd000) + d4_pll_factor))
	else:
		d2_pll_factor = d2_clock / m_clck
		d4_pll_factor = d4_clock / m_clck
		
		Driver.WriteRegisterShort(dbmd2,TDM_SCLK_CLOCK_FREQUENCY,hex(sclck))
		Driver.WriteRegisterShort(dbmd2,DSP_CLOCK_CONFIGURATION_EXTENSION,hex(int(0x9000) + d2_pll_factor))
		Driver.WriteRegisterShort(dbmd4,TDM_SCLK_CLOCK_FREQUENCY,hex(sclck))
		Driver.WriteRegisterShort(dbmd4,DSP_CLOCK_CONFIGURATION_EXTENSION,hex(int(0x9000) + d4_pll_factor))

	time.sleep(0.1)
	
def use_case_0():						##Moving To Idle
	
	write_to_log("use_case_0")
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0002")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0003")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd2,GENERAL_CONFIGURATION_2,"0000")
	
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd4,TDM_TX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd4,TDM_RX_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd4,AUDIO_PROCESSING_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd4,FIRST_MICROPHONE_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd4,SECOND_MICROPHONE_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd4,THIRD_MICROPHONE_CONFIGURATION,"0000")
	Driver.WriteRegisterShort(dbmd4,GENERAL_CONFIGURATION_2,"0000")
	
	change_clock_source(0,0)

def Enter_Idle():
	
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0000")
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0000")
	
	change_clock_source(0,0)		
	
def use_case_1():			###Just Trigger No Music
	
	global ASRP_Rec_points

	write_to_log("use_case_1")
	Driver.LoadAsrpParam(dbmd2,ASRP_PARAM_NR)
	
	Driver.WriteRegisterShort(dbmd4,GENERAL_CONFIGURATION_1,"1086")	      		
	Driver.WriteRegisterShort(dbmd4,GENERAL_CONFIGURATION_2,"0000")
	#Audio  Config
	Driver.WriteRegisterShort(dbmd4,AUDIO_PROCESSING_CONFIGURATION,"0440")
	Driver.WriteRegisterShort(dbmd4,AUDIO_STREAMING_SOURCE_SELECTION,"fff4")
	# Mics Config
	Driver.WriteRegisterShort(dbmd4,FIRST_MICROPHONE_CONFIGURATION,"a455")
	Driver.WriteRegisterShort(dbmd4,SECOND_MICROPHONE_CONFIGURATION,"5442")
	Driver.WriteRegisterShort(dbmd4,THIRD_MICROPHONE_CONFIGURATION,"5454")
	
	#TDM HW Regs config
	# D4 TDM 1 Transmit
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd4,TDM_TX_CONFIGURATION,"a203")
	Driver.WriteIOPort(dbmd4,D4_TDM_1_TX_ADDR,"00800010")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_TX_ADDR + 4),"00073007")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_TX_ADDR + 6),"101f003f")
	# D4 TDM 1 Recive 
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd4,TDM_RX_CONFIGURATION,"0013")
	Driver.WriteIOPort(dbmd4,D4_TDM_1_RX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_RX_ADDR + 4),"00070007")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_RX_ADDR + 6),"101f003f")

	#Audio  Config
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"0210",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"1eee",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"2e03",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"3eee",0.005)
	
	Driver.WriteRegisterShort(dbmd2,GENERAL_CONFIGURATION_2,"0000")
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_CONFIGURATION,"0003")
	
	# D2 TDM 2 Recive Master
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0002")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"a203")
	Driver.WriteIOPort(dbmd2,D2_TDM_2_RX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_RX_ADDR + 4),"00000027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_RX_ADDR + 6),"101f003f")
	# D2 TDM 2 Transmit
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0002")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"000f")
	Driver.WriteIOPort(dbmd2,D2_TDM_2_TX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_TX_ADDR + 4),"00072007")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_TX_ADDR + 6),"100f003f")
	
	if (DEBUG_REC == "YES" and Demo_Mode == "NR"):

		Driver.WriteRegisterShort(dbmd2, IO_PORT_ADDRESS_LOW,ASRP_Rec_points_5)
		Driver.WriteRegisterShort(dbmd2, IO_PORT_ADDRESS_HIGH,ASRP_Rec_points_6)
		Driver.WriteRegisterShort(dbmd2, IO_PORT_VALUE_LOW,ASRP_Rec_points_7)
		if (bit_exact==1):
			start_debug_rec()
			
	## Enable TDM and moving To Detection
	activate_tdms("3008","0c04")	

def use_case_3():						
	
	global ASRP_Rec_points
	global bit_exact
	global first_run
	global dbmd2_uart
	
	first_run = True
	
	write_to_log("use_case_3")

	Driver.LoadAsrpParam(dbmd2,ASRP_PARAM_AEC)
	
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_CONFIGURATION,"0000")
	
	#Audio Routing Config
	Driver.WriteRegisterShort(dbmd2,GENERAL_CONFIGURATION_2,"0200")
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_CONFIGURATION,"0003")
	
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"0210",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"154e",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"2e03",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"3eee",0.005)
	
	#TDM HW Regs config

	# D2 TDM 0 Recive Salve
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"9a13")
	Driver.WriteIOPort(dbmd2,D2_TDM_0_RX_ADDR,"0080001D")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_RX_ADDR + 4),"00072027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_RX_ADDR + 6),"100f001f")
	
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"8bc3")
	Driver.WriteIOPort(dbmd2,D2_TDM_0_TX_ADDR,"0080001D")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_TX_ADDR + 4),"00070027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_TX_ADDR + 6),"100f001f")
	# D2 TDM 3 Transmit
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0003")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"9a13")
	Driver.WriteIOPort(dbmd2,D2_TDM_3_TX_ADDR,"0080001D")
	Driver.WriteIOPort(dbmd2,(D2_TDM_3_TX_ADDR + 4),"00070027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_3_TX_ADDR + 6),"100f001f")
	
	# D2 TDM 1 Transmit
	#~ Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0001")
	#~ Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"9a13")
	#~ Driver.WriteIOPort(dbmd2,D2_TDM_1_TX_ADDR,"0080001f")
	#~ Driver.WriteIOPort(dbmd2,(D2_TDM_1_TX_ADDR + 4),"00070027")
	#~ Driver.WriteIOPort(dbmd2,(D2_TDM_1_TX_ADDR + 6),"100f001f")
	
	Driver.WriteRegisterShort(dbmd4,GENERAL_CONFIGURATION_2,"0000")
	#Audio  Config
	Driver.WriteRegisterShort(dbmd4,AUDIO_PROCESSING_CONFIGURATION,"0443")
	Driver.WriteRegisterShort(dbmd4,AUDIO_STREAMING_SOURCE_SELECTION,"fff4")
	# Mics Config
	Driver.WriteRegisterShort(dbmd4,FIRST_MICROPHONE_CONFIGURATION,"a455")
	Driver.WriteRegisterShort(dbmd4,SECOND_MICROPHONE_CONFIGURATION,"5442")
	Driver.WriteRegisterShort(dbmd4,THIRD_MICROPHONE_CONFIGURATION,"5454")
	#TDM HW Regs config
	# D4 TDM 1 Transmit
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd4,TDM_TX_CONFIGURATION,"a203")
	Driver.WriteIOPort(dbmd4,D4_TDM_1_TX_ADDR,"00800010")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_TX_ADDR + 4),"00073007")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_TX_ADDR + 6),"101f003f")
	
	# D4 TDM 1 Recive 
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd4,TDM_RX_CONFIGURATION,"0013")
	Driver.WriteIOPort(dbmd4,D4_TDM_1_RX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_RX_ADDR + 4),"00070007")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_RX_ADDR + 6),"101f003f")
		
	# D2 TDM 2 Recive Master
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0002")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"a203")
	Driver.WriteIOPort(dbmd2,D2_TDM_2_RX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_RX_ADDR + 4),"00000027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_RX_ADDR + 6),"101f003f")
	# D2 TDM 2 Transmit
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0002")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"000f")
	Driver.WriteIOPort(dbmd2,D2_TDM_2_TX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_TX_ADDR + 4),"00072007")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_TX_ADDR + 6),"101f003f")

	if (DEBUG_REC == "YES"):
		#~ 
		Driver.WriteRegisterShort(dbmd2, "3d","0128")
		Driver.WriteRegisterShort(dbmd2, "3e","3f00")
		Driver.WriteRegisterShort(dbmd2, IO_PORT_ADDRESS_LOW,ASRP_Rec_points_5)
		Driver.WriteRegisterShort(dbmd2, IO_PORT_ADDRESS_HIGH,ASRP_Rec_points_6)
		Driver.WriteRegisterShort(dbmd2, IO_PORT_VALUE_LOW,ASRP_Rec_points_7)
		if (bit_exact == 1):
			start_debug_rec()
	
def use_case_2():						
	
	global ASRP_Rec_points
	global bit_exact
	global first_run
	global dbmd2_uart
	
	first_run = True

	write_to_log("use_case_2")
	Driver.LoadAsrpParam(dbmd2,ASRP_PARAM_AEC)
	
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_CONFIGURATION,"0000")
	
	#Audio Routing Config
	Driver.WriteRegisterShort(dbmd2,GENERAL_CONFIGURATION_2,"0200")
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_CONFIGURATION,"0003")
	
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"0210",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"154e",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"2e03",0.005)
	Driver.WriteRegisterShort(dbmd2,AUDIO_PROCESSING_ROUTING,"3eee",0.005)
	
	#TDM HW Regs config

	# D2 TDM 0 Recive Salve
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"9a13")
	Driver.WriteIOPort(dbmd2,D2_TDM_0_RX_ADDR,"0080001D")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_RX_ADDR + 4),"00072027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_RX_ADDR + 6),"100f001f")
	
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0000")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"8bc3")
	Driver.WriteIOPort(dbmd2,D2_TDM_0_TX_ADDR,"0080001D")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_TX_ADDR + 4),"00070027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_0_TX_ADDR + 6),"100f001f")
	# D2 TDM 3 Transmit
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0003")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"9a13")
	Driver.WriteIOPort(dbmd2,D2_TDM_3_TX_ADDR,"0080001D")
	Driver.WriteIOPort(dbmd2,(D2_TDM_3_TX_ADDR + 4),"00070027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_3_TX_ADDR + 6),"100f001f")
	
	# D2 TDM 1 Transmit
	#~ Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0001")
	#~ Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"9a13")
	#~ Driver.WriteIOPort(dbmd2,D2_TDM_1_TX_ADDR,"0080001f")
	#~ Driver.WriteIOPort(dbmd2,(D2_TDM_1_TX_ADDR + 4),"00070027")
	#~ Driver.WriteIOPort(dbmd2,(D2_TDM_1_TX_ADDR + 6),"100f001f")
	
	Driver.WriteRegisterShort(dbmd4,GENERAL_CONFIGURATION_2,"0000")
	#Audio  Config
	Driver.WriteRegisterShort(dbmd4,AUDIO_PROCESSING_CONFIGURATION,"0443")
	Driver.WriteRegisterShort(dbmd4,AUDIO_STREAMING_SOURCE_SELECTION,"fff4")
	# Mics Config
	Driver.WriteRegisterShort(dbmd4,FIRST_MICROPHONE_CONFIGURATION,"a055")
	Driver.WriteRegisterShort(dbmd4,SECOND_MICROPHONE_CONFIGURATION,"5042")
	Driver.WriteRegisterShort(dbmd4,THIRD_MICROPHONE_CONFIGURATION,"5054")
	#TDM HW Regs config
	# D4 TDM 1 Transmit
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd4,TDM_TX_CONFIGURATION,"a203")
	Driver.WriteIOPort(dbmd4,D4_TDM_1_TX_ADDR,"00800010")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_TX_ADDR + 4),"00073007")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_TX_ADDR + 6),"101f003f")
	
	# D4 TDM 1 Recive 
	Driver.WriteRegisterShort(dbmd4,TDM_ACTIVATION_CONTROL,"0001")
	Driver.WriteRegisterShort(dbmd4,TDM_RX_CONFIGURATION,"0013")
	Driver.WriteIOPort(dbmd4,D4_TDM_1_RX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_RX_ADDR + 4),"00070007")
	Driver.WriteIOPort(dbmd4,(D4_TDM_1_RX_ADDR + 6),"101f003f")
		
	# D2 TDM 2 Recive Master
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0002")
	Driver.WriteRegisterShort(dbmd2,TDM_RX_CONFIGURATION,"a203")
	Driver.WriteIOPort(dbmd2,D2_TDM_2_RX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_RX_ADDR + 4),"00000027")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_RX_ADDR + 6),"101f003f")
	# D2 TDM 2 Transmit
	Driver.WriteRegisterShort(dbmd2,TDM_ACTIVATION_CONTROL,"0002")
	Driver.WriteRegisterShort(dbmd2,TDM_TX_CONFIGURATION,"000f")
	Driver.WriteIOPort(dbmd2,D2_TDM_2_TX_ADDR,"00800015")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_TX_ADDR + 4),"00072007")
	Driver.WriteIOPort(dbmd2,(D2_TDM_2_TX_ADDR + 6),"101f003f")

	if (DEBUG_REC == "YES"):
		#~ 
		Driver.WriteRegisterShort(dbmd2, "3d","0128")
		Driver.WriteRegisterShort(dbmd2, "3e","3f00")
		Driver.WriteRegisterShort(dbmd2, IO_PORT_ADDRESS_LOW,ASRP_Rec_points_5)
		Driver.WriteRegisterShort(dbmd2, IO_PORT_ADDRESS_HIGH,ASRP_Rec_points_6)
		Driver.WriteRegisterShort(dbmd2, IO_PORT_VALUE_LOW,ASRP_Rec_points_7)
		if (bit_exact == 1):
			start_debug_rec()
	
