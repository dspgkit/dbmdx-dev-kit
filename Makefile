CC = gcc
CFLAGS = -c -Wall
LIBPATH=
INCLUDE_DIRS +=

CFLAGS += $(addprefix -I,$(INCLUDE_DIRS))

LDFLAGS = -L$(LIBPATH) -g -Wl,-rpath=$(LIBPATH)
SOURCES = dbmdx_host_refcode.c
OBJECTS = $(SOURCES:.c=.o)
EXECUTABLE = demo

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@ -lwiringPi -pthread

.c.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm -f *.o *.a *.raw demo
