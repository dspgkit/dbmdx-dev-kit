#!/usr/bin/env python

# Version 1.5
# 21-DEC-2016

import os.path
import sys
import time
import RPi.GPIO as GPIO
from smbus import SMBus
import serial
import spidev
from subprocess import call
import datetime
import wave
import thread
import subprocess
import shlex
import gc
import shutil
from array import *
from abc import ABCMeta, abstractmethod

execfile ("Utilities.py")


class DeviceBase(object):
	__metaclass__ = ABCMeta

	@abstractmethod
	def __init__(self):
		pass

	@abstractmethod
	def Wakeup(self):
		pass

	@abstractmethod
	def Sleep(self):
		pass

	@abstractmethod
	def ReadRegister(self, register_address, use_wakeup):
		pass
		
	@abstractmethod	
	def	WriteRegister(self, reg_address, reg_value, sleep_time, use_wakeup):
		pass
	
	@abstractmethod	
	def ReadBytes(self, num_of_bytes, use_wakeup):
		pass
	
	@abstractmethod		
	def WriteBytes(self, data_list, use_wakeup):
		pass
			
	@abstractmethod		
	def LoadFile(self, filename):
		pass
		
	@abstractmethod		
	def ActivateBinFile(self):
		pass


class SpiDevice (DeviceBase):
	def __init__(self, name, chip_type, address, speed, chunk_size, mode=0):
		super(SpiDevice, self).__init__()
		global WAKEUP_GPIO
		try:
			speed = int(speed)
			chunk_size = int(chunk_size)
			address = int(address)
			mode = int(mode)
			chip_type_upeer = chip_type.upper()
			if (chip_type_upeer in wakeup_gpio_select):
				WAKEUP_GPIO = wakeup_gpio_select[chip_type_upeer]
			else:
				raise Exception ("Chip type \"" + str(chip_type) +"\" not exist!")
		except Exception, e:
			raise Exception ("Error while creating new SPI device: " + str(e))
		
		self.name = name
		self.spi = spidev.SpiDev()
		self.chunk_size = chunk_size
		self.spi.open(0, address)
		self.spi.mode = mode
		self.spi.max_speed_hz = speed
		GPIO.setup(WAKEUP_GPIO, GPIO.OUT, initial=GPIO.HIGH)
	
	def __str__(self):
		return "SPI"
		
	def Wakeup(self):
		GPIO.output(WAKEUP_GPIO, False)
		#time.sleep(0.03)
		time.sleep(0.001)
	
	def Sleep(self):
		GPIO.output(WAKEUP_GPIO, True)
		#time.sleep(0.03)
		time.sleep(0.001)
				
	def ReadRegister(self, reg_address, use_wakeup=None):
		global READ_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
		
		while (1):
			if (READ_IN_PROGRESS == False):
				READ_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
				
				if (type(reg_address) == str):
					reg_address = hex(int (reg_address, 16))[2:].zfill(3)
				elif (type(reg_address) == int):
					reg_address = hex(int(reg_address))[2:].zfill(3)
				else :
					READ_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None

				self.spi.xfer2 ([ord(reg_address[0]), ord(reg_address[1]), ord(reg_address[2]), ord('r')])
				time.sleep(0.01)
			
				value = self.spi.readbytes(5)
				value = ''.join(map(chr, value))
				value = value[1:]		

				READ_IN_PROGRESS = False
				if use_wakeup:
					self.Sleep()
				return value
			else:
				write_to_log("reading in use... please wait...")
				time.sleep(0.01)
	
	def ReadBytes(self, num_of_bytes, use_wakeup=None):
		global READ_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
		
		while (1):
			if (READ_IN_PROGRESS == False):
				READ_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
				
				if (type(num_of_bytes) == str):
					num_of_bytes = int(num_of_bytes)

				value = self.spi.readbytes(num_of_bytes)
				value = ''.join(map(chr, value))
				value = value[1:]		

				READ_IN_PROGRESS = False
				if use_wakeup:
					self.Sleep()
				return value
			else:
				write_to_log("reading in use... please wait...")
				time.sleep(0.01)

	def WriteRegister(self, reg_address, reg_value, sleep_time=None, use_wakeup=None):
		global WRITE_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
			
		while (1):
			if (WRITE_IN_PROGRESS == False):
				WRITE_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
			
				if (type(reg_address) == str):
					reg_address = hex(int (reg_address, 16))[2:].zfill(3)
				elif (type(reg_address) == int):
					reg_address = hex(int(reg_address))[2:].zfill(3)
				else :
					READ_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None

				if (reg_value in LIST_OF_VALUES):
					reg_value = LIST_OF_VALUES[reg_value]
				elif (type(reg_value) == str):
					reg_value = hex(int (reg_value, 16))[2:].zfill(4)
				elif (type(reg_value) == int):
					reg_value = hex(int(reg_value))[2:].zfill(4)
				else :
					WRITE_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None
					
				if (sleep_time == None):
					sleep_time = DEFAULT_SLEEP_TIME
				elif ((type(sleep_time) == str) or ((type(sleep_time) == int)) or ((type(sleep_time) == float))):
					sleep_time = float(sleep_time)
				else :
					WRITE_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None

				self.spi.xfer2 ([ord(reg_address[0]), ord(reg_address[1]), ord(reg_address[2]), ord('w'), ord(reg_value[0]), ord(reg_value[1]), ord(reg_value[2]), ord(reg_value[3])])
				if use_wakeup:
					self.Sleep()
				if (sleep_time):
					time.sleep(sleep_time)
				WRITE_IN_PROGRESS = False
				break
			else:
				write_to_log("writing in use... please wait...")
				time.sleep(0.01)
	
	def WriteBytes(self, data, use_wakeup=None):
		global WRITE_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
			
		while (1):
			if (WRITE_IN_PROGRESS == False):
				WRITE_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
				
				data_list = list(data)
				
				if (type(data_list[0]) == str) :
					i=0
					for item in data_list:
						data_list[i] = int(item,16)
						i+=1
				
				self.spi.writebytes([j for j in data_list])

				if use_wakeup:
					self.Sleep()
					
				WRITE_IN_PROGRESS = False
				break
			else:
				write_to_log("writing in use... please wait...")
				time.sleep(0.01)

	def LoadFile(self, filename, number_of_bytes_to_skip_from_end=0):
		infile = open(filename, "rb")
		list_file = list(infile.read())
		list_file = [ord(i) for i in list_file]
		if (type(number_of_bytes_to_skip_from_end) == str):
			number_of_bytes_to_skip_from_end = int(number_of_bytes_to_skip_from_end)
		j=0
		while(j < (len(list_file) - self.chunk_size - number_of_bytes_to_skip_from_end)) :
			self.spi.writebytes([i for i in list_file[(j):(j+self.chunk_size)]])
			j = j + self.chunk_size
		# Loading last bytes
		self.spi.writebytes([i for i in list_file[(j):(len(list_file)-number_of_bytes_to_skip_from_end)]])
		time.sleep(0.01)
		
	def ActivateBinFile(self):
		self.spi.xfer2 ([0x5A, 0x0B])

	def ReadChecksumFromFw(self):
		self.Wakeup()
		self.spi.xfer2 ([0x5A, 0x0E])
		readSpi = self.spi.readbytes(7)
		for i in range (0, len(readSpi)):
			readSpi[i] = hex(readSpi[i])
		time.sleep(0.01)
		self.Sleep()
		return readSpi[3:]

	def LoadModelFile(self, file_name, unsent_bytes):
		infile = open(file_name,"rb")
		list_file_temp = list(infile.read())
		list_file = [ord(i) for i in list_file_temp]
				
		i=0
		while (i < (len(list_file) - unsent_bytes)):
			current_byte = hex(list_file[i])
			next_byte = hex(list_file[i+1])
			
			if ((current_byte == '0x5a') and ((next_byte == '0x2') or (next_byte == '0x1'))):
				#send the header to the FW
				self.spi.writebytes([k for k in list_file[i:(i+10)]])
				time.sleep(0.0001)
				
				i+=2
				num_of_words = get_value_from_bytes(list_file[i:(i+4)])

				num_of_bytes = 2*num_of_words
				i+=8
				
				while (num_of_bytes >= self.chunk_size):
					self.spi.writebytes([k for k in list_file[i:(i+self.chunk_size)]])
					num_of_bytes -= self.chunk_size
					i+=self.chunk_size
				
				# send the rest of the data until the next header
				self.spi.writebytes([k for k in list_file[i:(i+num_of_bytes)]])
				i+=num_of_bytes
				
			else:
				write_to_log ("BAD HEADER !")
				sys.exit()		
	
	def ChangeInterfaceSpeed(self, interface_speed):
		self.spi.max_speed_hz = int(interface_speed)
		write_to_log("Changed interface speed to " + str(interface_speed))
	

class I2cDevice (DeviceBase):
	def __init__(self, name, chip_type, bus=1, address=0x3E):
		super(I2cDevice, self).__init__()
		
		global WAKEUP_GPIO
		try:
			if (type(bus) == str):
				bus = int(bus, 16)
			if (type(address) == str):
				address = int(address, 16)
			chip_type_upeer = chip_type.upper()
			if (chip_type_upeer in wakeup_gpio_select):
				WAKEUP_GPIO = wakeup_gpio_select[chip_type_upeer]
			else:
				raise Exception ("Chip type \"" + str(chip_type) +"\" not exist!")
		except Exception, e:
			raise Exception ("Error while creating new I2C device: " + str(e))
		
		self.name = name
		self.i2c = SMBus(bus)
		self.i2c_addr = address
		GPIO.setup(WAKEUP_GPIO, GPIO.OUT, initial=GPIO.HIGH)
			
	def __str__(self):
		return "I2C"
		
	def Wakeup(self):
		GPIO.output(WAKEUP_GPIO,False)
		#time.sleep(0.001)	# Not Working !!!
		time.sleep(0.01)

	def Sleep(self):
		GPIO.output(WAKEUP_GPIO,True)
		#time.sleep(0.001)	# Not Working !!!
		time.sleep(0.01)
	
	def ReadRegister(self, reg_address, use_wakeup=None):
		global READ_IN_PROGRESS	
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
			
		while (1):
			if (READ_IN_PROGRESS == False):
				READ_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()

				if (type(reg_address) == str):
					reg_address = int (reg_address, 16)
				elif (type(reg_address) == int):
					reg_address = int(reg_address)
				else :
					READ_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None
				
				
				
				if (reg_address > 99): # use Indirect Access Registers
					self.i2c.write_word_data(self.i2c_addr, INDIRECT_ACCESS_REGISTER_NUMBER, swap_bytes(reg_address))
					value = self.i2c.read_word_data(self.i2c_addr, INDIRECT_ACCESS_REGISTER_READ)
				else:	
					value = self.i2c.read_word_data(self.i2c_addr, reg_address)
				
				
				value = str(hex(swap_bytes(value)))[2:]
				value = value.zfill(4)
				
				READ_IN_PROGRESS = False
				if use_wakeup:
					self.Sleep()
				return value
			else:
				write_to_log("reading in use... please wait...")
				time.sleep(0.01)
	
	def ReadBytes(self, num_of_bytes, use_wakeup=None):
		global READ_IN_PROGRESS	
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
		
		chunk_size = 32
		while (1):
			if (READ_IN_PROGRESS == False):
				READ_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
				
				if (type(num_of_bytes) == str):
					num_of_bytes = int(num_of_bytes)
				
				value = ''
				while (num_of_bytes > chunk_size):
					data_block = self.i2c.read_i2c_block_data(self.i2c_addr, 0, chunk_size)				
					for i in data_block :
						value+=chr(i)
					num_of_bytes-=chunk_size
				data_block = self.i2c.read_i2c_block_data(self.i2c_addr, 0, num_of_bytes)
				value += ''.join(chr(x) for x in data_block)

				READ_IN_PROGRESS = False
				if use_wakeup:
					self.Sleep()
				return value
			else:
				write_to_log("reading in use... please wait...")
				time.sleep(0.01)
	
	def WriteRegister(self, reg_address, reg_value, sleep_time=None, use_wakeup=None):
		global WRITE_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
			
		while (1):
			if (WRITE_IN_PROGRESS == False):
				WRITE_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
			
				if (type(reg_address) == str):
					reg_address = int (reg_address, 16)
				elif (type(reg_address) == int):
					reg_address = int(reg_address)
				else :
					READ_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None
				
				if (reg_value in LIST_OF_VALUES):
					reg_value = LIST_OF_VALUES[reg_value]
				
				if (type(reg_value) == str):
					reg_value = int (reg_value, 16)
				elif (type(reg_value) == int):
					reg_value = int(reg_value)
				else :
					WRITE_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None
				
				if (sleep_time == None):
					sleep_time = DEFAULT_SLEEP_TIME
				elif ((type(sleep_time) == str) or ((type(sleep_time) == int)) or ((type(sleep_time) == float))):
					sleep_time = float (sleep_time)
				else :
					WRITE_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None
				
				if (reg_address > 99): # use Indirect Access Registers
					self.i2c.write_word_data(self.i2c_addr, INDIRECT_ACCESS_REGISTER_NUMBER, swap_bytes(reg_address))	
					self.i2c.write_word_data(self.i2c_addr, INDIRECT_ACCESS_REGISTER_WRITE, swap_bytes(reg_value))	
				else:	
					self.i2c.write_word_data(self.i2c_addr, reg_address, swap_bytes(reg_value))	
					
				
				if use_wakeup:
					self.Sleep()
				if (sleep_time):
					time.sleep(sleep_time)
				WRITE_IN_PROGRESS = False
				break
			else:
				write_to_log("writing in use... please wait...")
				time.sleep(0.01)
	
	def WriteBytes(self, data, use_wakeup=None):
		global WRITE_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
		
		chunk_size = 32	
		while (1):
			if (WRITE_IN_PROGRESS == False):
				WRITE_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
				
				data_list = list(data)
				for i,val in enumerate(data_list):
					data_list[i] = ord(val)
				
				i=0
				num_of_bytes = len(data_list)
				
				while (num_of_bytes > chunk_size):
					self.i2c.write_i2c_block_data(self.i2c_addr, data_list[i], [k for k in data_list[(i+1):(i+chunk_size)]])
					num_of_bytes-= chunk_size
					i+= chunk_size			
				# send the rest of the data
				self.i2c.write_i2c_block_data(self.i2c_addr, data_list[i], [k for k in data_list[(i+1):(i+num_of_bytes)]])

				if use_wakeup:
					self.Sleep()
					
				WRITE_IN_PROGRESS = False
				break
			else:
				write_to_log("writing in use... please wait...")
				time.sleep(0.01)

	def LoadFile(self, filename,number_of_bytes_to_skip_from_end=0):
		infile = open(filename, "rb")
		list_file = list(infile.read())
		list_file = [ord(i) for i in list_file]
		if (type(number_of_bytes_to_skip_from_end) == str):
			number_of_bytes_to_skip_from_end = int (number_of_bytes_to_skip_from_end)
		j=0
		while(j < (len(list_file)-32-number_of_bytes_to_skip_from_end)):
			self.i2c.write_i2c_block_data(self.i2c_addr, list_file[j], [i for i in list_file[(j+1):(j+32)]])
			j = j + 32
		self.i2c.write_i2c_block_data(self.i2c_addr, list_file[j], [i for i in list_file[(j+1):(len(list_file)-number_of_bytes_to_skip_from_end)]])

		time.sleep(0.01)
		
	def ActivateBinFile(self):
		self.i2c.write_byte(self.i2c_addr, int((0x05A))) 
		self.i2c.write_byte(self.i2c_addr, int((0x00B)))

	def ReadChecksumFromFw(self):
		self.Wakeup()
		self.i2c.write_byte(self.i2c_addr, int((0x5A))) 
		self.i2c.write_byte(self.i2c_addr, int((0x0E)))
		time.sleep(0.01)
		# dump the echo 0x5A, 0x0E from the checksum
		self.i2c.read_byte(self.i2c_addr) 
		self.i2c.read_byte(self.i2c_addr)
		checksum = [0,0,0,0]
		for i in range (0, 4):
			checksum[i]  = str(hex(self.i2c.read_byte(self.i2c_addr)))
		time.sleep(0.01)
		return checksum

	def LoadModelFile(self, file_name, unsent_bytes):
		infile = open(file_name, "rb")
		list_file_temp = list(infile.read())
		list_file = [ord(i) for i in list_file_temp]
		chunk_size = 16
		
		i=0
		while (i < (len(list_file) - unsent_bytes)):
			current_byte = hex(ord(list_file_temp[i]))
			next_byte = hex(ord(list_file_temp[i+1]))
			
			if ((current_byte == '0x5a') and ((next_byte == '0x2') or (next_byte == '0x1'))):
				#send the header to the FW
				self.i2c.write_i2c_block_data(self.i2c_addr, list_file[i], [k for k in list_file[(i+1):(i+10)]])
				time.sleep(0.001)
				
				i+=2
				# number of words field
				num_of_words = get_value_from_bytes(list_file[i:(i+4)])
				num_of_bytes = 2*num_of_words
				i+=8
						
				while (num_of_bytes > chunk_size):
					self.i2c.write_i2c_block_data(self.i2c_addr, list_file[i], [k for k in list_file[(i+1):(i+chunk_size)]])
					num_of_bytes-= chunk_size
					i+= chunk_size
				
				# send the rest of the data until the next header
				self.i2c.write_i2c_block_data(self.i2c_addr, list_file[i], [k for k in list_file[(i+1):(i+num_of_bytes)]])
				i+=num_of_bytes

			else:
				write_to_log ("BAD HEADER !")
				sys.exit()
				
	def ChangeInterfaceSpeed(self, interface_speed):
		write_to_log("Can't change I2C interface speed from command line")


class UartDevice (DeviceBase):
	__metaclass__ = ABCMeta
	
	@abstractmethod
	def __init__(self):
		pass
				
	def __str__(self):
		return "UART"
		
	@abstractmethod
	def Wakeup(self):
		pass
	
	@abstractmethod
	def Sleep(self):
		pass
	
	def ReadRegister(self, reg_address, use_wakeup=None):
		global READ_IN_PROGRESS	
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
		
		while (1):
			if (READ_IN_PROGRESS == False):
				READ_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
				
				if (type(reg_address) == str):
					reg_address = hex(int (reg_address, 16))[2:].zfill(3)
				elif (type(reg_address) == int):
					reg_address = hex(int(reg_address))[2:].zfill(3)
				else :
					READ_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None

				self.ser.flushInput()
				self.ser.write(reg_address + "r")
				time.sleep(0.001)
				value = self.ser.read(5)[:4]
				
				READ_IN_PROGRESS = False
				return value
			else:
				write_to_log("reading in use... please wait...")
				time.sleep(0.01)
	
	def ReadBytes(self, num_of_bytes, use_wakeup=None):
		global READ_IN_PROGRESS	
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
			
		while (1):
			if (READ_IN_PROGRESS == False):
				READ_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
				
				if (type(num_of_bytes) == str):
					num_of_bytes = int(num_of_bytes)
				
				value = self.ser.read(num_of_bytes)
				
				READ_IN_PROGRESS = False
				return value
			else:
				write_to_log("reading in use... please wait...")
				time.sleep(0.01)
	
	def WriteRegister(self, reg_address, reg_value, sleep_time=None, use_wakeup=None):
		global WRITE_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
			
		while (1):
			if (WRITE_IN_PROGRESS == False):
				WRITE_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()
			
				if (type(reg_address) == str):
					reg_address = hex(int (reg_address, 16))[2:].zfill(3)
				elif (type(reg_address) == int):
					reg_address = hex(int(reg_address))[2:].zfill(3)
				else :
					READ_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None

				if (reg_value in LIST_OF_VALUES):
					reg_value = LIST_OF_VALUES[reg_value]
				elif (type(reg_value) == str):
					reg_value = hex(int (reg_value, 16))[2:].zfill(4)
				elif (type(reg_value) == int):
					reg_value = hex(int(reg_value))[2:].zfill(4)
				else :
					WRITE_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None
				
				if (sleep_time == None):
					sleep_time = DEFAULT_SLEEP_TIME
				elif ((type(sleep_time) == str) or ((type(sleep_time) == int)) or ((type(sleep_time) == float))):
					sleep_time = float (sleep_time)
				else :
					WRITE_IN_PROGRESS = False
					write_to_log("Error: Unknown input !")
					return None
				
				self.ser.flushInput()
				self.ser.write(reg_address + "w" + reg_value)
				if (sleep_time):
					time.sleep(sleep_time)
				WRITE_IN_PROGRESS = False
				break
			else:
				write_to_log("writing in use... please wait...")
				time.sleep(0.01)
	
	def WriteBytes(self, data_list, use_wakeup=None):
		global WRITE_IN_PROGRESS
		if (use_wakeup == None):
			use_wakeup = USE_WAKEUP
			
		while (1):
			if (WRITE_IN_PROGRESS == False):
				WRITE_IN_PROGRESS = True
				if use_wakeup:
					self.Wakeup()

				self.ser.write(data_list)
					
				WRITE_IN_PROGRESS = False
				break
			else:
				write_to_log("writing in use... please wait...")
				time.sleep(0.01)

	@abstractmethod
	def LoadFile(self, filename, number_of_bytes_to_skip_from_end):
		pass
	
	def ActivateBinFile(self):
		self.ser.write(chr(0x5A) + chr(0x0B))

	def ReadChecksumFromFw(self):
		self.ser.flushInput()
		self.ser.write(chr(0x5A) + chr(0x0E))
		readSerial = self.ser.read(6)
		readSerialList = list(readSerial)
			
		
		for i in range (0, len(readSerialList)):
			readSerialList[i] = str(hex(ord(readSerialList[i])))
		
		time.sleep(0.01)
		return readSerialList[2:]
	
	def LoadModelFile(self, file_name, unsent_bytes):
		infile = open(file_name,"rb")
		list_file_temp = list(infile.read())
		list_file = [ord(i) for i in list_file_temp]
		
		i=0
		while (i < (len(list_file) - unsent_bytes)):
			current_byte = hex(list_file[i])
			next_byte = hex(list_file[i+1])
			
			if ((current_byte == '0x5a') and ((next_byte == '0x2') or (next_byte == '0x1'))):
				#send the header to the FW
				j=0
				while(j < 10) :
					self.ser.write(list_file_temp[i+j])
					j += 1
				time.sleep(0.0001)
				
				i+=2
				# number of words field
				num_of_words = get_value_from_bytes(list_file[i:(i+4)])
				num_of_bytes = 2*num_of_words
				i+=8
				
				j=0
				while (j < num_of_bytes):
					self.ser.write(list_file_temp[i+j])
					j+=1			
				i+=num_of_bytes
			
			else:
				write_to_log ("BAD HEADER !")
				sys.exit()
	
	def ChangeInterfaceSpeed(self, interface_speed):
		speed = int(interface_speed)
		self.WriteRegister (UART_BAUD_RATE, str(hex(int(interface_speed)/100))[2:])
		self.ser = serial.Serial(
			port='/dev/ttyAMA0',
			baudrate=speed,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE,
			bytesize=serial.EIGHTBITS,
			timeout=0.01 
			)
		write_to_log("Changed interface speed to " + str(interface_speed))


class UartOld(UartDevice):
	def __init__(self, name, speed):
		super(UartOld, self).__init__()
		try:
			speed = int(speed)			
		except Exception, e:
			raise Exception ("Error while creating new UART device: " + str(e))
			
		self.name = name
		self.speed = speed
		
		global WAKEUP_GPIO
		WAKEUP_GPIO = 24
		GPIO.setup(WAKEUP_GPIO, GPIO.OUT, initial=GPIO.HIGH)
		self.ser = serial.Serial(
			port='/dev/ttyAMA0',
			baudrate=self.speed, 
			parity=serial.PARITY_EVEN,
			stopbits=serial.STOPBITS_TWO,
			bytesize=serial.EIGHTBITS,
			timeout=0.01
			)
	
	def Wakeup(self):
		GPIO.output(WAKEUP_GPIO, True)
		time.sleep(0.001)	
	
	def Sleep(self):
		GPIO.output(WAKEUP_GPIO, False)
		time.sleep(0.001)
	
	def LoadFile(self, filename, number_of_bytes_to_skip_from_end=0):
		received_word = ''
		timeout_start = time.time()
		timeout = 10
		self.ser.flushInput()
		
		if (type(number_of_bytes_to_skip_from_end) == str):
				number_of_bytes_to_skip_from_end = int (number_of_bytes_to_skip_from_end)
		
		if (number_of_bytes_to_skip_from_end == 4): # Firmware file
			
			while ((received_word != "OK") and (time.time() < (timeout_start + timeout))):
				self.ser.write("Ac")
				received_word = self.ser.read(2)
			
			if received_word == "OK":
				write_to_log("Got Sync")
			else:
				write_to_log("No Sync")
				raise Exception ("No Sync")
				
			time.sleep(0.1)
		
		# Download the file to the chip	
		infile = open(filename, "rb")
		list_file = list(infile.read())
		j=0
		while(j < len(list_file) - number_of_bytes_to_skip_from_end) :
			self.ser.write(str(list_file[j]))
			j = j + 1
		time.sleep(0.01)


class UartNew(UartDevice):
	def __init__(self, name, speed):
		super(UartNew, self).__init__()
		try:
			speed = int(speed)
		except Exception, e:
			raise Exception ("Error while creating new UART device: " + str(e))

		self.name = name
		self.speed = speed
		
		self.ser = serial.Serial(
			port='/dev/ttyAMA0',
			baudrate=self.speed,
			parity=serial.PARITY_NONE,
			stopbits=serial.STOPBITS_ONE,
			bytesize=serial.EIGHTBITS,
			timeout=0.01 
			)
		shell_command("gpio -g mode 14 alt0")
	
	def Wakeup(self):
		self.ser.write(chr(0x00))
		time.sleep(0.001)
		self.ser.write(chr(0x00))
		self.ser.flushInput()
		time.sleep(0.001)
	
	def Sleep(self):
		pass
	
	def LoadFile(self, filename, number_of_bytes_to_skip_from_end=0):
		received_word = ''
		timeout_start = time.time()
		timeout = 10
		self.ser.flushInput()
		
		if (type(number_of_bytes_to_skip_from_end) == str):
			number_of_bytes_to_skip_from_end = int (number_of_bytes_to_skip_from_end)
		
		if (number_of_bytes_to_skip_from_end == 4): # Firmware file
			
			while ((received_word != "OK") and (time.time() < (timeout_start + timeout))):
				self.ser.write(chr(0x0)+chr(0x0)+ chr(0x0)+ chr(0x0)+chr(0x0)+chr(0x0)+ chr(0x0)+ chr(0x0)+chr(0x0)+chr(0x0)+ chr(0x0)+ chr(0x0)+chr(0x0)+chr(0x0)+ chr(0x0)+ chr(0x0)) 
				received_word = self.ser.read(2)

			if received_word == "OK":
				write_to_log("Got Sync")
			else:
				write_to_log("No Sync")
				raise Exception ("No Sync")
			time.sleep(0.1)
		
		# Download the file to the chip
		infile = open(filename, "rb")
		list_file = list(infile.read())
		j=0
		while(j < len(list_file) - number_of_bytes_to_skip_from_end) :
			self.ser.write(str(list_file[j]))
			j = j + 1
		time.sleep(0.01)

			
class DebugDevice(object):
	def __init__(self, name, port_type, speed):
		try:
			speed = int(speed)
			self.name = name
			self.speed = speed
			
			port_type = port_type.upper()
			if (port_type == "USB0"):
				# Add: check if FTDI device is connected to USB
				port_type = '/dev/ttyUSB0'
			elif (port_type == "USB1"):
				# Add: check if FTDI device is connected to USB
				port_type = '/dev/ttyUSB1'
			elif (port_type == "UART"):
				shell_command("gpio -g mode 15 alt0")
				port_type = '/dev/ttyAMA0'
			else:
				raise Exception ("Port type not exist ! select USB0 or USB1 or UART only")
				
			self.ser = serial.Serial(
				port=port_type,
				baudrate=self.speed,
				parity=serial.PARITY_NONE,
				stopbits=serial.STOPBITS_ONE,
				bytesize=serial.EIGHTBITS,
				timeout=0
				)
				
		except Exception, e:
			raise Exception ("Error while creating Debug device: " + str(e))
			
	def __str__(self):
		if (self.ser.port == '/dev/ttyUSB0'):
			return "Debug_USB0"
		elif (self.ser.port == '/dev/ttyUSB1'):
			return "Debug_USB1"
		else:
			return "Debug_UART"
	
	def ChangeInterfaceSpeed(self, interface_speed):
		speed = int(interface_speed)
		self.ser.baudrate = speed
			
		write_to_log("Changed interface speed to " + str(speed))
		write_to_log("Don't forget to set register 0x0C to the new Debug Device speed !")

	def ShowOnScreen(self):
		global DEBUG_IN_PROGRESS
		self.ser.flushInput()
			
		while (SHOW_DEBUG_MESSAGES):
			if (DEBUG_IN_PROGRESS == False):
				DEBUG_IN_PROGRESS = True
				
				num_of_bytes = self.ser.inWaiting()
				if (num_of_bytes):
					value = self.ser.read(num_of_bytes)
					print "Debug: " + value[:-1]	 # Remove \n from the end
				time.sleep(0.001)
				
				DEBUG_IN_PROGRESS = False
			else:
				write_to_log("Debug in use... please wait...")
				time.sleep(0.01)
				
	def WriteToFile(self):
		self.ser.flushInput()
		create_debug_log_file()		
		value = ''

		while (RECORDING_TO_FILE):			
			num_of_bytes = self.ser.inWaiting()
			#print "number of bytes: ", num_of_bytes
			if (num_of_bytes):
				value += (self.ser.read(num_of_bytes))
		
		write_to_debug_file(value)
		close_file(debug_log_file)
		write_to_log ("Digital recording was save to file: " + str(debug_log_file.name))

	
