
import os.path
import sys
import time
import RPi.GPIO as GPIO
from smbus import SMBus
import serial
import spidev
from subprocess import call
import datetime
import wave
import thread
import subprocess
import shlex
import gc
import shutil
from array import *
from abc import ABCMeta, abstractmethod
from Tkinter import *
import tkMessageBox
import tkFont
import re
import importlib
import fileinput


cnt = 0

execfile ("UseCases.py")

global running
global music_on
global music_index
global music_volume
global music_duc
global command_recieved
global volume_step
global songs_duration
global song_completed
global duc_volume
global first_run
global dbmd2_uart
global dbmd2
global dbmd4

CURRENT_DEVICE = None
LIST_OF_VALUES = {}
LIST_OF_DEVICES =[]

# Device functions
def add_device(device):
		global LIST_OF_DEVICES
		for item in LIST_OF_DEVICES:
			if (item.name == device.name):
				write_to_log("Device already existed")
				return
	
		LIST_OF_DEVICES.append(device)
		write_to_log("Device \"" + str(device.name) +"\" created successfully")
		
def get_all_devices():
	if (len(LIST_OF_DEVICES) == 0):
		write_to_log("No device configured in the system")
	else:
		write_to_log("The following devices configured in the system:")
		for item in LIST_OF_DEVICES:
			write_to_log("Device name: \"" + str(item.name) +"\"") 

def set_current_device(device_name):
	global CURRENT_DEVICE
	
	if (type(device_name) != str): # the input is a device class
		device_name = device_name.name
	
	for item in LIST_OF_DEVICES:
		if (item.name == device_name):
			CURRENT_DEVICE = item
			write_to_log("Current device is \"" + item.name +"\"")
			write_to_log("Current device is \"" + item.name +"\"")
			return
	# device_name no found in list
	write_to_log("Device \"" + device_name + "\" not exist in Device List")

def get_device_by_name(device_name):
	for item in LIST_OF_DEVICES:
		if (item.name == device_name):
			return item
			
	# device_name no found in list
	write_to_log("Device \"" + device_name + "\" not exist in Device List")
	sys.exit()

def get_current_device():
	if (CURRENT_DEVICE == None):
		write_to_log("Current device not defined")
		return
#~ 
	write_to_log ("Current device type: " + str(CURRENT_DEVICE) + " Device") 
	write_to_log ("Current device name = " + CURRENT_DEVICE.name)
	return CURRENT_DEVICE


gc.enable()
create_log_file()

dbmd4 = SpiDevice("dbmd4","d4","0",3000000,15000,0)			### Dbmd4 device creation
#~ add_device(dbmd4)


dbmd2_pre = I2cDevice("dbmd2_pre","d2")				### Dbmd2 device creation
add_device(dbmd2_pre)

dbmd2 = SpiDevice("dbmd2","d2","1",3000000,15000,0)			### Dbmd2 device creation
add_device(dbmd2)

#dbmd2_uart = DebugDevice("dbmd2_uart","USB1","3000000")			### Dbmd2 device creation
#add_device(dbmd2_uart)

### Demo Functions
	
def set_iom():

	Driver.WriteIOPort(dbmd2,"03000050","00100000")
	Driver.WriteIOPort(dbmd2,"03000048","80008017")
	Driver.WriteIOPort(dbmd2,"0300004c","00000000")

def start_debug_rec():

	print "\n ####	DEBUG RECORDING STARTED	#### \n"
	Driver.WriteRegisterShort(dbmd2, UART_DEBUG_RECORDING,"0001")
	Driver.StartDigitalRecording(dbmd2_uart)

def stop_debug_rec():
	
	print "\n ####	DEBUG RECORDING STOPPED	#### \n"
	
	Driver.WriteRegisterShort(dbmd2,"30","0")
	
	Driver.WriteRegisterShort(dbmd2, "3d","0128")
	Driver.WriteRegisterShort(dbmd2, "3e","0000")
	
	Driver.StopDigitalRecording()
	
# Callback function
	
def interrupt_callback(self):
	
	global stop_streaming_flag
	global music_volume
	global music_duc
	global command_recieved
	global music_on
	global music_index
	global wordID
	global statistic
	global cnt
	
	
	#	print "Got Callback"
	
	stop_streaming_flag = True
	time.sleep(0.05)
	if ( (DEMO == True) and (STREAMING == True) ):
		thread.start_new_thread(blink_LED,(2, 0))
		Driver.DumpBuffer(dbmd4, BUF_SIZE, AUDIO_SAMPLE_RATE, number_of_channels)
		thread.start_new_thread(blink_LED,(2, 1))
		if (AUTO_PLAYBACK == True):
			convert_to_wav("audio_file.raw", AUDIO_SAMPLE_RATE, number_of_channels)
			play_audio ("0", "audio_file.wav", 100)
		Driver.WriteRegisterShort("01", "0001")
	else:
	#	print "Music ON is: " + str(music_on)
		wordID = int(Driver.ReadRegisterShort(dbmd4,SENSORY_WORDID),16)
		if (wordID < 99): #Got a trigger
			if (music_on == True and Demo_Mode == "DM1"):
	#			print "need Duc"""
				music_duc = True  
				command_recieved = False
			if (Demo_Mode == "DM2" or Demo_Mode == "DM3"):
				shell_command("arecord -D dspg -c 1 -f S16_LE -r 48000 -d 12 Audio_Buffer" + str(cnt) + ".wav &")
			#~ thread.start_new_thread(blink_LED,(4, 2))			
			write_to_log("Got a trigger!")
			play_audio ("0", "beep.wav", 100)
			cnt = cnt+1
			print "Number Of Detection : " + str(cnt)
			if (STATISTIC == True):		
				sv_score=str(int(Driver.ReadRegisterShort(dbmd4,"5d"),16))		
				write_to_log("Got a trigger! sv_score=" + sv_score)					
				write_to_log("word id: " + str(wordID))
		else:
			music_duc = False
			command_recieved = True	
			
			if (Demo_Mode == "DM1"):
				
				if (wordID == 259): #Got play music command
					write_to_log("Got a Play Music command!")
					#~ write_to_log("word id: " + str(wordID))
					play_music_command()

				elif (wordID == 258): # Got Pause Command
					write_to_log("Got a Pause Music command!")
			#		write_to_log("word id: " + str(wordID))
					pause_music_command()

				elif (wordID == 261): # Got Next Track Command
			#		print "After command music duc is: " + str(music_duc)
					write_to_log("Got a Next Song command!")
			#		write_to_log("word id: " + str(wordID))
					next_song_command()

				elif (wordID == 260): # Got Previous Track Command
					write_to_log("Got a Previous Song command!")
			#		write_to_log("word id: " + str(wordID))
					previous_song_command()

				elif (wordID == 262): # Got Volume DOWN Command
					write_to_log("Got a Volume DOWN command!")
			#		write_to_log("word id: " + str(wordID))
					volume_down_command()

				elif (wordID == 257): # Got Volume UP Command
					write_to_log("Got a Volume UP command!")
			#		write_to_log("word id: " + str(wordID))
					volume_up_command()
					
				time.sleep(0.3)
				command_recieved = False
			
############ GUI related Functions ##############

def set_trigger_offset(offset):
	new_offset = abs(int(offset))
	# print 'New Trigger offset in decimal is: ' + str(new_offset)
	new_offset = hex(new_offset).zfill(4)[2:]
	# print 'New Trigger offset in hex is: ' + str(new_offset)
	Driver.WriteRegister(dbmd4,'47',str(new_offset))

def set_verification_threshold(threshold):
	new_threshold = int(8192 * float(threshold))
	# print 'New Verification Threshold in decimal is: ' + str(threshold)
	new_threshold = hex(new_threshold).zfill(4)[2:]
	# print 'New Verification Threshold in hex is: ' + str(new_threshold)
	Driver.WriteRegister(dbmd4,'48',str(new_threshold))

def change_values_window():

	global model_sensitivity_entry
	global trigger_offset_entry
	global verification_threshold_entry
	global record_time_entry

	change_values_frame = Toplevel()
	change_values_frame.title("Change Sensitivity Parameters")
	change_values_frame.geometry ("400x200")
	#Labels

	sensitivity_label_1 = Label(change_values_frame, text="Model Sensitivity Level (1:20)", font=sensitivity_option_font)
	sensitivity_label_1.grid(column=1, row=1, sticky=W, pady=10)

	trigger_offset_label = Label(change_values_frame, text="Trigger Sensitivity Offset (-2000:0)", font=sensitivity_option_font)
	trigger_offset_label.grid(column=1, row=3, sticky=W, pady=10)


	verification_threshold_label = Label(change_values_frame, text="Verification threshold (0:1)", font=sensitivity_option_font)
	verification_threshold_label.grid(column=1, row=5, sticky=W, pady=10)

	#Entries
	model_sensitivity_entry = Entry(change_values_frame, bd =2,textvariable=StringVar(),width=5)
	model_sensitivity_entry.grid(column=2, row=1, sticky=W)

	trigger_offset_entry = Entry(change_values_frame, bd =3,textvariable=StringVar(),width=5)
	trigger_offset_entry.grid(column=2, row=3, sticky=W)

	verification_threshold_entry = Entry(change_values_frame, bd =2,textvariable=StringVar(),width=5)
	verification_threshold_entry.grid(column=2, row=5, sticky=W)

	change_sensitivity_button = Button(change_values_frame, text = "Change" , command = change_sensitivity)
	change_sensitivity_button.grid(column=3, row=1 ,sticky=W)

	change_trigger_offset_button = Button(change_values_frame, text = "Change" , command = change_trigger_offset)
	change_trigger_offset_button.grid(column=3, row=3 ,sticky=W)

	change_verification_threshold_button = Button(change_values_frame, text = "Change" , command = change_verification_threshold)
	change_verification_threshold_button.grid(column=3, row=5 ,sticky=W)

def change_uart_points_window():

	global Reg_5_entry
	global Reg_6_entry
	global Reg_7_entry
	global Reg_5_cur_Label_2
	global Reg_6_cur_Label_2
	global Reg_7_cur_Label_2


	
	uart_points_frame = Toplevel()
	uart_points_frame.title("Choose UART Recording points")
	uart_points_frame.geometry ("400x250")
	
	Reg_5_Label = Label(uart_points_frame, text="Register 0x5")
	Reg_5_Label.grid(column=1, row=1, sticky=W+N, pady=5)
	Reg_5_cur_Label_2 = Label(uart_points_frame,fg="red")
	Reg_5_cur_Label_2.grid(column=2, row=2, sticky=E+N, pady=5)
	Reg_5_new_label = Label(uart_points_frame, text="New: ")
	Reg_5_new_label.grid(column=3, row=2, sticky=E+N, pady=5, padx=10)
	Reg_6_Label = Label(uart_points_frame, text="Register 0x6 ( ASRP )")
	Reg_6_Label.grid(column=1, row=3, sticky=W+N, pady=5)
	Reg_6_cur_Label_2 = Label(uart_points_frame,fg="red")
	Reg_6_cur_Label_2.grid(column=2, row=4, sticky=E+N, pady=5)
	Reg_6_new_label = Label(uart_points_frame, text="New: ")
	Reg_6_new_label.grid(column=3, row=4, sticky=E+N, pady=5, padx=10)
	Reg_7_Label = Label(uart_points_frame, text="Register 0x7")
	Reg_7_Label.grid(column=1, row=5, sticky=W+N, pady=5)
	Reg_7_cur_Label_2 = Label(uart_points_frame,fg="red")
	Reg_7_cur_Label_2.grid(column=2, row=6, sticky=E+N, pady=5)
	Reg_7_new_label = Label(uart_points_frame, text="New: ")
	Reg_7_new_label.grid(column=3, row=6, sticky=E+N, pady=5, padx=10)
	
	Reg_5_cur_Label_2.config(text = "Current: " +  str(ASRP_Rec_points_5))
	Reg_6_cur_Label_2.config(text = "Current: " +  str(ASRP_Rec_points_6))
	Reg_7_cur_Label_2.config(text = "Current: " +  str(ASRP_Rec_points_7))
	
	Reg_5_entry = Entry(uart_points_frame, bd =2,textvariable=StringVar(),width=5)
	Reg_5_entry.grid(column=4, row=2, sticky=W)
	Reg_6_entry = Entry(uart_points_frame, bd =2,textvariable=StringVar(),width=5)
	Reg_6_entry.grid(column=4, row=4, sticky=W)
	Reg_7_entry = Entry(uart_points_frame, bd =2,textvariable=StringVar(),width=5)
	Reg_7_entry.grid(column=4, row=6, sticky=W)
	
	
	set_values_button = Button(uart_points_frame, text = "Set Points" , command = change_uart_points, fg='green')
	set_values_button.grid(column=2, row=7,sticky=W+S+E)
	

def algo_demo():
	
	global debug_btn
	global DEBUG_REC
	global user_selection
	global bit_exact
	global CODEC_MASTER
	
	Demo_Mode = str(options_var.get())
	#print str(current_model.get())

	debug = checkVar.get()
	bit_exact = checkVar2.get()

	#checkVar.get()
	#print "Debug mode is = " + str(debug)

	if (debug == 1):
		DEBUG_REC = "YES"
		if (bit_exact == 0):
			start_debug_button.config(state="normal")
	else:
		DEBUG_REC = "NO"
	if (checkVar3.get() == 1):
		CODEC_MASTER = True
			
	debug_btn.configure (state="disabled")
	bit_exact_button.configure (state="disabled")
	codec_master_button.configure (state="disabled")		
	start_button.configure(state="disabled")
	stop_button.configure(state="normal")
	change_sensitivity_parameters_button.config(state="disabled")
	select_Debug_points_button.config(state="disabled")
	
	thread.start_new_thread(main,())

def stop_algo_demo():
	
	global running
	global music_on
	global first_run

	if (DEBUG_REC == "YES"):
		stop_debug_rec()
		DEBUG_REC == "NO"
	
	use_case_0()
		
	if (music_on == True):
		
		music_on = False
		
		shell_command("pkill arecord")

		stop_audio()

	start_button.configure(state="normal")
	debug_btn.configure (state="normal")
	codec_master_button.configure (state="normal")
	GPIO.remove_event_detect(INTERRUPT_TRIGGER_GPIO)
	start_debug_button.config(state="disabled")
	bit_exact_button.config(state="normal")
	change_sensitivity_parameters_button.config(state="normal")
	stop_button.configure(state="disabled")
	select_Debug_points_button.config(state="normal")
	
	GPIO.remove_event_detect(INTERRUPT_TRIGGER_GPIO)
	running = False
	first_run = False
	
def check_sensitivity_value(new_value):

	try:
		value = int(new_value)
		if (value > 20 or value < 1):
			tkMessageBox.showinfo( "BAD VALUE", "Please choose value from 1-20")
			return False
		else:
			return True
	except ValueError:
		tkMessageBox.showinfo( "BAD VALUE", "Please choose value from 1-20")
		return False

def check_offset_value(new_value):
	
	try:
		value = int(new_value)
		if (value > 0 or value < -2000):
			tkMessageBox.showinfo( "BAD VALUE", "Please choose value from -2000 to 0")
			return False
		else:
			return True
	except ValueError:
		tkMessageBox.showinfo( "BAD VALUE", "Please choose value from -2000 to 0")
		return False

def check_threshold(new_value):

	try:
		value = float(new_value)
	#	print 'Value inside check function is : ' + str(value)
		if (value > 1 or value < 0):
	#		print 'BAD'
			tkMessageBox.showinfo( "BAD VALUE", "Please choose value from 0 to 1")
			return False
		else:
	#		print 'OK'
			return True
	except ValueError:
		tkMessageBox.showinfo( "BAD VALUE", "Please choose value from 0 to 1")
		return False

def sel():

	#print "INSIDE SEL FUNCTION"
	global SENSITIVITY_LEVEL
	global TRIGGER_OFFSET
	global VERIFICATION_THRESHOLD
	global options_var
	global Demo_Mode
	
	Demo_Mode = str(options_var.get())

	change_sensitivity_parameters_button.grid()
	current_sensitivity_label_2.grid()
	current_trigger_offset_label.grid()
	current_verification_threshold_label.grid()
	
	if (Demo_Mode != "AEC"):
		codec_master_button.configure (state="disabled")
		codec_master_button.deselect()
	else:
		codec_master_button.configure (state="normal")

	if ((Demo_Mode != 'Voice_Call_AEC') and (Demo_Mode != 'Voice_Call_NR')):
		
		current_sensitivity_label_2.config(text = 'Current Sensitivity Level is:  ' + str(SENSITIVITY_LEVEL))
		current_trigger_offset_label.config(text = 'Current Trigger Offset is:  ' + str(TRIGGER_OFFSET))
		current_verification_threshold_label.config(text = 'Current Verification threshold is:  ' + str(VERIFICATION_THRESHOLD))


	else:
		current_sensitivity_label_2.grid_remove()
		current_trigger_offset_label.grid_remove()
		current_verification_threshold_label.grid_remove()
		change_sensitivity_parameters_button.grid_remove()

def ASRP_GUI():
	
	global options_font
	global sensitivity_option_font
	global options_var
	global change_var
	global checkVar
	global checkVar2
	global checkVar3
	global vc_button
	global aec_button
	global nr_button
	global voice_call_button
	global debug_btn
	global start_button
	global stop_button
	global start_debug_button
	global change_sensitivity_parameters_button
	global current_sensitivity_label_2
	global current_trigger_offset_label
	global current_verification_threshold_label
	global select_Debug_points_button
	global bit_exact_button
	global codec_master_button
	
	root = Tk()
	root.geometry ("600x500")

	root.title("DSPG ASRP Demo")
	main_frame = Frame(root)
	main_frame.grid(column=0, row=0, sticky=(N, W, E, S))
	main_frame.columnconfigure(0, weight=1)
	main_frame.rowconfigure(0, weight=1)
	
	#Fonts Settings
	options_font = tkFont.Font(root ,family="Helvetica", size=14, weight='bold')
	sensitivity_option_font = tkFont.Font(root ,family="Helvetica", size=12, weight='bold')
	start_font = tkFont.Font(root ,family="Helvetica", size=20, weight='bold')
	font_b = tkFont.Font(root ,family="Helvetica", size=12, weight='bold')
	#Defines
	options_var = StringVar()
	change_var = StringVar()
	checkVar = IntVar()
	checkVar2 = IntVar()
	checkVar3 = IntVar()

	#Buttons
	vc_button = Radiobutton(main_frame, text="VT + VC (AEC & NR Enabled)", font = options_font, variable=options_var, value='VC',height=3, command=sel)
	vc_button.grid(column=1, row=1, sticky=W)

	aec_button = Radiobutton(main_frame, text="VT Only (AEC & NR Enabled)", font = options_font, variable=options_var, value='AEC',height=3, command=sel)
	aec_button.grid(column=1, row=2, sticky=W)

	nr_button = Radiobutton(main_frame, text="VT Only (NR Enabled)", font = options_font, variable=options_var, value='NR',height=3, command=sel)
	nr_button.grid(column=1, row=3, sticky=W)

	voice_call_button = Radiobutton(main_frame, text="Voice call Tx Only (NR Enabled)", font = options_font, variable=options_var, value='Voice_Call_NR',height=3, command=sel)
	voice_call_button.grid(column=1, row=4, sticky=W)

	voice_call_button = Radiobutton(main_frame, text="Voice call DT (AEC & NR Enabled)", font = options_font, variable=options_var, value='Voice_Call_AEC',height=3, command=sel)
	voice_call_button.grid(column=1, row=5, sticky=W)

	debug_btn = Checkbutton(main_frame, text = "Debug Mode", variable=checkVar,onvalue=1,offvalue=0)
	debug_btn.grid(column=2, row=3, pady=2, padx=2,sticky=W)

	start_button = Button(main_frame, font=start_font, text = "Start Demo", command = algo_demo,fg='green')
	start_button.grid(column=2, row=1, pady=10, padx=10, columnspan=1, rowspan=1,sticky=W+E+N+S)
	
	stop_button = Button(main_frame, font=start_font, text = "Stop Demo", command = stop_algo_demo,fg='red', state="disabled")
	stop_button.grid(column=2, row=2, pady=10, padx=10, columnspan=1, rowspan=1,sticky=W+E+N+S)

	change_sensitivity_parameters_button = Button(main_frame, text = "Change Sensitivity Parameters" , command = change_values_window)
	change_sensitivity_parameters_button.grid(column=1, row=12 ,sticky=W)

	start_debug_button = Button(main_frame, font=font_b, text = "Start DEBUG Recording", command = start_debug_rec,fg='green', state="disabled")
	start_debug_button.grid(column=2, row=4 ,sticky=W+E+N)
	
	select_Debug_points_button = Button(main_frame, text = "Select UART Recordings points" , command = change_uart_points_window, fg="green")
	select_Debug_points_button.grid(column=2, row=4 ,sticky=E+S)

	bit_exact_button = Checkbutton(main_frame, text = "Bit Exact", variable=checkVar2,onvalue=1,offvalue=0)
	bit_exact_button.grid(column=2, row=3, pady=2, padx=2,sticky=E)
	
	codec_master_button = Checkbutton(main_frame, text = "Codec Is Master", variable=checkVar3,onvalue=1,offvalue=0)
	codec_master_button.grid(column=2, row=7, pady=2, padx=2,sticky=E)

	#Labels

	current_sensitivity_label_2 = Label(main_frame,font=sensitivity_option_font , fg='blue')
	current_sensitivity_label_2.grid(column=1, row=7, sticky=W, padx=5)

	current_trigger_offset_label = Label(main_frame,font=sensitivity_option_font , fg='blue')
	current_trigger_offset_label.grid(column=1, row=8, sticky=W, padx=5)

	current_verification_threshold_label = Label(main_frame,font=sensitivity_option_font , fg='blue')
	current_verification_threshold_label.grid(column=1, row=9, sticky=W, padx=5)

	label = Label(root)

	aec_button.select()
	aec_button.invoke()

	root.mainloop()

def change_uart_points():

	global ASRP_Rec_points_5
	global ASRP_Rec_points_6
	global ASRP_Rec_points_7
	
	new_reg_5 = str(Reg_5_entry.get())
	new_reg_6 = str(Reg_6_entry.get())
	new_reg_7 = str(Reg_7_entry.get())
	
	#~ print new_reg_5
	#~ print new_reg_6
	#~ print new_reg_7
	
	#~ print ASRP_Rec_points_5
	#~ print ASRP_Rec_points_6
	#~ print ASRP_Rec_points_7
	

	if new_reg_5.strip and new_reg_5:
		try:
			int(new_reg_5,16)
			if (new_reg_5 != ASRP_Rec_points_5 and int(new_reg_5,16) <= 65535):
				ASRP_Rec_points_5 = new_reg_5
			if ((int(new_reg_5,16) > 65535)):
				tkMessageBox.showinfo( "BAD VALUE", "Please choose value less then 0xffff")
		except ValueError:
			tkMessageBox.showinfo( "BAD VALUE", "Please choose value less then 0xffff")
					
	if new_reg_6.strip and new_reg_6:
		try:
			int(new_reg_6,16)
			if (new_reg_6 != ASRP_Rec_points_6 and int(new_reg_6,16) <= 65535):
				ASRP_Rec_points_6 = new_reg_6
			if ((int(new_reg_6,16) > 65535)):
				tkMessageBox.showinfo( "BAD VALUE", "Please choose value less then 0xffff")
		except ValueError:
			tkMessageBox.showinfo( "BAD VALUE", "Please choose value less then 0xffff")
	if new_reg_7.strip and new_reg_7:
		try:
			int(new_reg_7,16)
			if (new_reg_7 != ASRP_Rec_points_7 and int(new_reg_7,16) <= 65535):
				ASRP_Rec_points_7 = new_reg_7
			if ((int(new_reg_7,16) > 65535)):
				tkMessageBox.showinfo( "BAD VALUE", "Please choose value less then 0xffff")
		except ValueError:
			tkMessageBox.showinfo( "BAD VALUE", "Please choose value less then 0xffff")
	
	Reg_5_cur_Label_2.config(text = "Current: " +  str(ASRP_Rec_points_5))
	Reg_6_cur_Label_2.config(text = "Current: " +  str(ASRP_Rec_points_6))
	Reg_7_cur_Label_2.config(text = "Current: " +  str(ASRP_Rec_points_7))
	
	#~ print ASRP_Rec_points_5
	#~ print ASRP_Rec_points_6
	#~ print ASRP_Rec_points_7

def change_sensitivity():
	
	#print "INSIDE CHANGE_SENS FUNCTION"
	global SENSITIVITY_LEVEL
	global A_MODEL

	new_value = model_sensitivity_entry.get()
	#print 'new value is = ' + change_value
	#print 'sensitivity level is = ' + sensitivity_level
	if (new_value != SENSITIVITY_LEVEL):
		
		if (check_sensitivity_value(new_value)) :
			
			SENSITIVITY_LEVEL = new_value
			A_MODEL = "Models/109_hbg_search" + str(SENSITIVITY_LEVEL) + "_p.bin"
			sel()

def change_trigger_offset():

	global TRIGGER_OFFSET

	new_value = trigger_offset_entry.get()
	#print 'new value is = ' + change_value
	#print 'trigger offset is = ' + trigger_offset

	if (new_value != TRIGGER_OFFSET):

		if (check_offset_value(new_value)) :

			TRIGGER_OFFSET = new_value
			sel()

def change_verification_threshold():

	global VERIFICATION_THRESHOLD

	new_value = verification_threshold_entry.get()

	#print 'new value is = ' + change_value
	#print 'verification threshold is = ' + verification_threshold

	if (new_value != VERIFICATION_THRESHOLD):

		if (check_threshold(new_value)) :
			
			VERIFICATION_THRESHOLD = new_value
			sel()


######## Music Player Functions ########

def find_wav_length(path,wav):
	wav = str(path) + str(wav)
	song = wave.open(wav,'r')
	rate = song.getframerate()
	frames = song.getnframes() 
	duration = frames / float(rate)
	#print duration
	return duration

def volume_control():

	global music_duc
	global running

	while (running):
	#	print "Music Duc is: " + str(music_duc)
		if (music_duc == True and command_recieved == False):
			if (music_volume >= 100):
				change_music_volume("dspg",duc_volume)
				music_duc = False
			time.sleep(2.5)
			if (wordID < 99 or wordID ==  259):
				if (music_volume > music_duc):
					music_duc = False
					change_music_volume("dspg",music_volume)
		time.sleep(0.1)

def play_music_command():
	
	global first_run
	
	#print "Music index is = " + str(music_index)
	if (not music_on):
	#	print "inside play"
		if (not first_run):
			use_case_0()
			if (Demo_Mode == "DM2"):
				use_case_2()
			else:
				use_case_3()
			
		play_audio("dspg",MUSIC_PATH + SONGS[music_index],music_volume)
		change_clock_source(48,16)
		activate_tdms("b308","0c04")
		
	else:
		write_to_log("Music Already Playing!")

def pause_music_command():

	global music_on
	global first_run

	if (music_on == True):
		
		first_run = False
		
		use_case_0()

		stop_audio()
		
		use_case_1()
		
def next_song_command():
	
	global music_index
	global first_run

	if (music_index == (len(SONGS)-1)):
		music_index = 0
	else:
		music_index = music_index + 1
	
	if (music_on == True):
		
		Enter_Idle()
		
		stop_audio()
	
	elif (not first_run):

		if (Demo_Mode == "DM2"):
			use_case_2()
		else:
			use_case_3()
		
	play_audio("dspg",MUSIC_PATH + SONGS[music_index],music_volume)		
	change_clock_source(48,16)
	activate_tdms("b108","0c04")
	
def previous_song_command():
	
	global music_index
	global first_run

	if (music_index == 0):
		music_index = music_index
	else:
		music_index = music_index - 1
	if (music_on == True):
		
		Enter_Idle()

		stop_audio()
		
	elif (not first_run):
		
		if (Demo_Mode == "DM2"):
			use_case_2()
		else:
			use_case_3()
		
	play_audio("dspg",MUSIC_PATH + SONGS[music_index],music_volume)		
	change_clock_source(48,16)
	activate_tdms("b108","0c04")	

def volume_up_command():
	global music_volume

	if (music_volume >= (255-volume_step)):
		music_volume = 240
	else:
		music_volume = music_volume + volume_step
	change_music_volume ("dspg",music_volume)

def volume_down_command():
	global music_volume

	if (music_volume < volume_step):
		music_volume = 0
	else:
		music_volume = music_volume - volume_step
	change_music_volume ("dspg",music_volume)
	
def async_music_stop():
	
	global running
	global music_index
	global music_on
	global songs_duration
	global song_completed

	count = 0 

	while (running):
		song_length = ((songs_duration[music_index]-5)*10)
	#	print "song length is = "  + str(song_length)
		if (music_on == True):
	#		print "Staring count"
			while(count < song_length and song_completed == False):
	#			print "In WHILE LOOP, SONG completed is: " + str(song_completed)
				time.sleep(0.1)
				if (song_completed == True and command_recieved == True):
					count = 0
					break
				count = count + 1
	#		print "out of while loop"
			count = 0
			time.sleep(0.3)
			if (music_on == True and command_recieved == False):
				next_song_command()
		time.sleep(0.1)

def change_music_volume (device, volume):

	global music_volume
	if (device == "dspg"):
		device == str(1)
	if (music_duc == True):
		volume = duc_volume
	write_to_log ("Changing music volume to " + str(volume))
	shell_command ("amixer -q sset -c 1 'Master' " + str(volume)) #Adjust volume)
def main():
	
	global running
	global music_on
	global music_index
	global music_volume
	global music_duc
	global command_recieved
	global volume_step
	global songs_duration
	global song_completed
	global duc_volume
	global first_run
	global cnt
	global RECORDING_TIME
	
	disable_statistic()
	
	music_on = False
	music_index = 0
	music_volume = 170
	music_duc = False
	command_recieved = False
	volume_step = 20
	songs_duration = [None]*len(SONGS)
	song_completed = False
	duc_volume = 90
	running = True
	first_run = False
	cnt=0
	
	for x in range (0,len(SONGS)):
		songs_duration[x] = find_wav_length(MUSIC_PATH,SONGS[x])      ### Arranging Song List Times
		
	shell_command("pkill -f aplay")
	
	
	Driver.Reset(dbmd4)
	Driver.Reset(dbmd2)
	time.sleep(0.3)
	Driver.LoadFw(dbmd4,FIRMWARE_2)
	
	Driver.Reset(dbmd2)
	dbmd2_pre.LoadFile(PRE_BOOT)
	time.sleep(0.1)
	Driver.LoadFw(dbmd2,FIRMWARE)

	
	init_d4()
	
	init_d2()

	shell_command("python CODEC_16K_16b_Stereo_Slave_EVB-S.py")
	
	
	Driver.PreLoadTriggerModel(dbmd4,A_MODEL)
	
	Driver.PreLoadCommandModel(dbmd4,A_MODEL_VC)
	Driver.LoadCommandModel(dbmd4,A_MODEL_VC)
	
	Driver.LoadTriggerModel(dbmd4,A_MODEL)

	Driver.WriteRegister(dbmd4,"45","0002")
	set_trigger_offset(TRIGGER_OFFSET)

	##### Choosing the right mode
	
	
	if (Demo_Mode == "DM1"):
			
		thread.start_new_thread(async_music_stop,())
		thread.start_new_thread(volume_control,())
			
		use_case_1()
			
	else:
			
		thread.start_new_thread(async_music_stop,())
		music_duc = False
		command_recieved = True
		write_to_log("Got a Play Music command!")
		play_music_command()
		time.sleep(0.3)
		command_recieved = False	

	GPIO.add_event_detect(INTERRUPT_TRIGGER_GPIO, GPIO.RISING, callback=interrupt_callback)
	
	print "\n"
	print "########  " + Demo_Mode + " Demo ##########"
	print "\n"
	
	#print "\n********************* Unified  RPI interface *****************\n"	
	#print "To create SPI device type: create_new_spi_device <device name> <chip type> <spi address> <device speed> <chunk size>" 
	#print "To create UART device type: create_new_uart_device <device name> <chip type> <device speed>" 
	#print "To create I2C device type: create_new_i2c_device <device name> <chip type> <i2c bus> <i2c address>" 
	#print "To set a device to be the current device type: set_current_device <device name>"
	#print "To get the current device type: get_current_device"
	#print "To reset the chip type: reset"
	#print "To read a register type: r <register_addrees>"
	#print "To write to a register type: w <register_addrees> <register_value>"
	#print "To exit type: q "
	
	play_audio(0, "oksay.wav")
	
	user_selection = ''
	while (user_selection != 'q'):
		user_selection = raw_input("")
		user_selection = user_selection.strip() # remove all whitespaces from end of string
		userList=user_selection.split()	
		if (user_selection == ''):
				print "Insert valid command"
		elif (userList[0] == "shell"):
			userList=user_selection.split()
			userList.remove(userList[0])
			command = ' '.join(userList)	
			shell_command (command)
		elif ((len(userList)) == 1 ) :	
			if (user_selection == 'reset'):
				Driver.Reset(CURRENT_DEVICE)
			elif (user_selection == 'disable_statistic'):
				disable_statistic()
			elif (user_selection == 'enable_statistic'):
				enable_statistic()
			elif (user_selection == 'stop_streaming'):
				stop_streaming()
			elif (user_selection == 'stop_digital_recording'):
				Driver.StopDigitalRecording()
			elif (user_selection == 'enter_hibernate'):
				Driver.EnterHibernate(CURRENT_DEVICE)
			elif (user_selection == 'exit_hibernate'):
				Driver.ExitHibernate(CURRENT_DEVICE)
			elif (user_selection == 'stop_audio'):
				stop_audio()
			elif (user_selection == 'show_values'):
				show_values()
			elif (user_selection == 'get_current_device'):
				get_current_device()
			elif (user_selection == "get_default_wakeup"):
				get_default_wakeup()		
			elif (user_selection == "get_default_sleep_time"):	
				get_default_sleep_time()
			elif (user_selection == "get_all_devices"):	
				get_all_devices()
			elif (user_selection == 'q'):
				write_to_log("quit the system")
			elif (user_selection == 'use_case_3'):
				use_case_barge_in_16b()
			else:
				print "Insert valid command"
		elif ((len(userList)) == 2 ) :		
			if (userList[0] == "set_date_time"):
				set_date_time(userList[1])
			elif (user_selection[0] == 'r'):
				Driver.ReadRegister(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'change_interface_speed'):
				Driver.ChangeInterfaceSpeed(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'import_values'):
				import_values_from_file(userList[1])
			elif (userList[0] == 'plt'):
				Driver.PreLoadTriggerModel(CURRENT_DEVICE ,userList[1])
			elif (userList[0] == 'plc'):
				Driver.PreLoadCommandModel(CURRENT_DEVICE ,userList[1])
			elif (userList[0] == 'io_r'):
				Driver.ReadIOPort(CURRENT_DEVICE, userList[1])
			elif (userList[0] == "set_current_device"):
				set_current_device(userList[1])
			elif (userList[0] == "set_default_wakeup"):
				set_default_wakeup(userList[1])
			elif (userList[0] == "set_default_sleep_time"):
				set_default_sleep_time(userList[1])
			elif (userList[0] == 'lf'):
				Driver.LoadFw(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'lt'):
				Driver.LoadTriggerModel(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'lc'):
				Driver.LoadCommandModel(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'lg'):
				Driver.LoadGoogleModel(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'la'):
				Driver.LoadAsrpParam(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'load_file'):
				Driver.LoadFile(CURRENT_DEVICE, userList[1])
			elif (userList[0] == 'start_digital_recording'):
				device_object = get_device_by_name(userList[1])
				Driver.StartDigitalRecording(device_object)
			else:
				print "Insert valid command"
		elif ((len(userList)) == 3 ) :	
			if (userList[0] == 'w'):
				Driver.WriteRegister(CURRENT_DEVICE, userList[1], userList[2])
			elif (userList[0] == 's'):
				Driver.WriteRegisterShort(CURRENT_DEVICE, userList[1], userList[2])
			elif (userList[0] == 'play_audio'):
				thread.start_new_thread(play_audio,("0", userList[1], userList[2]))
			elif (userList[0] == 'io_w'):
				Driver.WriteIOPort(CURRENT_DEVICE, userList[1], userList[2])
			elif (userList[0] == "create_new_i2c_device"):
				new_device = I2cDevice(userList[1], userList[2])
				add_device(new_device)
			elif (userList[0] == 'create_acoustic_model'):
				create_acoustic_model(userList[1], userList[2])	
			else:
				print "Insert valid command"
		elif ((len(userList)) == 4 ) :
			if (userList[0] == 'w'):
				Driver.WriteRegister(CURRENT_DEVICE, userList[1], userList[2], userList[3])
			elif (userList[0] == 's'):
				Driver.WriteRegisterShort(CURRENT_DEVICE, userList[1], userList[2], userList[3])
			elif (userList[0] == "dump"):
				thread.start_new_thread(Driver.DumpBuffer,(CURRENT_DEVICE, userList[1], userList[2], userList[3]))
			elif (userList[0] == "stream"):
				thread.start_new_thread(Driver.Stream,(CURRENT_DEVICE, userList[1], userList[2], userList[3]))
			elif (userList[0] == "convert"):		
				convert_to_wav(userList[1], userList[2],userList[3])
			elif (userList[0] == 'play_audio'):
				thread.start_new_thread(play_audio,(userList[1], userList[2], userList[3]))
			elif (userList[0] == 'write_i2c'):
				write_to_I2C_bus(userList[1], userList[2], userList[3])
			elif (userList[0] == "create_new_uart_device"):
				if not userList[3].isdigit():
					write_to_log ("Error: Wrong speed parameters")
					continue
				
				chip_type_upeer = userList[2].upper()
				if (chip_type_upeer in ChipType):
					if (chip_type_upeer == "D2") :
						new_device = UartOld(userList[1], userList[3])
					else:
						new_device = UartNew(userList[1], userList[3])
					add_device(new_device)
				else:
					raise Exception ("Chip type \"" + str(userList[2]) +"\" not exist!")
			else:
				print "Insert valid command"
		elif ((len(userList)) == 5 ) :
			if (userList[0] == "dump"):
				thread.start_new_thread(Driver.DumpBuffer,(CURRENT_DEVICE, userList[1], userList[2], userList[3], userList[4]))
			elif (userList[0] == "convert"):		
				convert_to_wav(userList[1], userList[2], userList[3], userList[4])
			elif (userList[0] == "create_new_i2c_device"):
				new_device = I2cDevice(userList[1], userList[2], userList[3], userList[4])
				add_device(new_device)
			else:
				print "Insert valid command"
		elif ((len(userList)) == 6 ) :
			if (userList[0] == "create_new_spi_device"):
				new_device = SpiDevice(userList[1], userList[2], userList[3], userList[4], userList[5])
				add_device(new_device)
			else:
				print "Insert valid command"
		elif ((len(userList)) == 7 ) :
			if (userList[0] == "create_new_spi_device"):
				new_device = SpiDevice(userList[1], userList[2], userList[3], userList[4], userList[5], userList[6])
				add_device(new_device)
			else:
				print "Insert valid command"
		else:
			print "Insert valid command"	

	
	GPIO.cleanup()
	close_file(log_file)

if __name__ == "__main__" :
	if (DEMO):
		try:
			main()
		except Exception, e:
			print "Error message from SW: " + str(e)
	else:
		try:
			ASRP_GUI()	
		except Exception, e:
			print "Error message from SW: " + str(e)


	
