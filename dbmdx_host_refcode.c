/*
 * dbmdx_host_refcode.c
 *
 * Copyright 2016  DSP Group
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */


#include <stdio.h>
#include <time.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <linux/spi/spidev.h>
#include <sys/poll.h>
#include <sys/time.h>
#include <sys/eventfd.h>
#include <pthread.h>


#define DEBUG 1
#ifdef DEBUG
	#define DEBUG_PRINT printf
#else
	#define DEBUG_PRINT(format, args...) ((void)0)
#endif

//#define SPI_EXTENSION 1

#define LOW  0
#define HIGH 1

#define DBMD5 1
#define D4SPI 1
typedef enum {FALSE = 0, TRUE = 1} bool;
#define uint unsigned int

// GPIO pins on RPI board
#define RESET_GPIO 17
#define RESET_GPIO_D4 10

#define GPIO_13 13
#define GPIO_26 26
#define GPIO_25 25


#define LED_GPIO 22
#define INTERRRUPT_TRIGGER_GPIO 23
#define INTERRRUPT_COMMAND_GPIO 18
#define FIRMWARE_LOADED_GPIO 20
#define WAKEUP_GPIO 24


// Command Registers on DBMDX
#define FW_VERSION_NUMBER 			0X00
#define OPERATION_MODE 				0X01
#define PRIMARY_ACOUSTIC_MODEL_SIZE 		0X02
#define SECONDARY_ACOUSTIC_MODEL_SIZE 		0X03
#define DIGITAL_GAIN 				0X04
#define AUDIO_BUFFER_SIZE 			0X09
#define NUMBER_OF_SAMPLES_IN_BUFFER 		0X0A
#define UART_Baud_Rate		 		0x0c
#define LOAD_NEW_ACOUSTIC_MODEL 		0X0F
#define DSP_CLOCK_CONFIGURATION 		0X10
#define Audio_Processing_Routing    		0x11
#define AUDIO_BUFFER_READOUT_CONFIGURATION 	0X12
#define Audio_Streaming_Source_Selection 	0x13
#define PHRASE_DETECTION_GPIO_CONFIGURATION 	0X15
#define Samples_For_VT 						0x17
#define Master_Clock_Frequency			0x1B
#define TDM0_SCLK_Clock_Frequency 		0x1d
#define DSP_Clock_Configuration_Extension 	0x1e
#define READ_AUDIO_BUFFER 			0X20
#define GENERAL_CONFIGURATION_1 		0X22
#define GENERAL_CONFIGURATION_2 		0X23
#define FIRST_MICROPHONE_CONFIGURATION 		0X24
#define SECOND_MICROPHONE_CONFIGURATION 	0X25
#define THIRD_MICROPHONE_CONFIGURATION 	    	0X26
#define HOST_INTERFACE_SUPPORT 			0X29
#define UART_DEBUG_RECORDING 			0X30
#define TDM_Activation_Control 			0x31

#define AUDIO_PROCESSING_CONFIGURATION	0x34
#define TDM_Rx_Configuration 			0x36
#define TDM_Tx_Configuration 			0x37
#define SENSORY_WORDID					0x5B
#define SENSORY_SCORE_VALUE				0x5D

// Sensory commands registers
#define SENSORY_DUALTHRESHOLD 			0X45
#define SENSORY_POSTPARAMAOFFSET		0X47
#define SENSORY_INITIALIZED 			0X41


// DBMDX Operation Modes command 1
#define IDLE_MODE		0X00
#define DETECTION_MODE 	0X01
#define BUFFERING_MODE 	0X02
#define HIBERNATE_MODE	0X06


#define SIZE_TO_ALLOCATE_FOR_ACOUSTIC_MODEL 		0X1000
#define SIZE_TO_ALLOCATE_FOR_TRIGGER_ACOUSTIC_MODEL 0X1000
#define GAIN_OF_12_DB 								0X18

// command for AUDIO_BUFFER_SIZE (0x9)
#define BUFFER_SIZE_3_SEC 				0X1770
#define BUFFER_SIZE_5_SEC 				0X2724

// command for LOAD_NEW_ACOUSTIC_MODEL (0xF)
#define LOAD_TRIGGER_ACOUSTIC_MODEL 					0X00
#define LOAD_COMMAND_ACOUSTIC_MODEL 					0X01
#define LOAD_COMMAND_ASRP_PARAM 					0X04
#define AUDIO_BUFFER_READOUT_START_FROM_POINT_OF_SWITCHING_TO_BUFFERING 0X00
#define AUDIO_BUFFER_READOUT_START_FROM_PHRASE_END_POINT 		0X01
#define AUDIO_BUFFER_READOUT_START_FROM_PHRASE_START_POINT 		0X02

// command for HOST_INTERFACE_SUPPORT (0x29)
#define HOST_INTERFACE_I2C  0X02
#define HOST_INTERFACE_UART 0X01
#define HOST_INTERFACE_SPI  0X20

#define DBMD2       0
#define DBMD4 	    1

// tdm addr
#define  D4_TDM_0_RX_ADDR   0x80800000
#define  D4_TDM_1_RX_ADDR   0x80801000

#define  D4_TDM_0_TX_ADDR   0x80804000
#define  D4_TDM_1_TX_ADDR   0x80805000

#define  D2_TDM_0_RX_ADDR   0x80800000
#define  D2_TDM_1_RX_ADDR   0x80801000
#define  D2_TDM_2_RX_ADDR   0x80802000
#define  D2_TDM_3_RX_ADDR   0x80803000

#define  D2_TDM_0_TX_ADDR   0x80804000
#define  D2_TDM_1_TX_ADDR   0x80805000
#define  D2_TDM_2_TX_ADDR   0x80806000
#define  D2_TDM_3_TX_ADDR   0x80807000

// File name of Firmware and Acousric model
// DBMD4 firmware
#define FirmWare_filename "Firmware/Mango_D4_ver_330_Sen333_QED_1290.bin"

// DBMD2 firmware
#define FirmWareD2_filename "Firmware/Mango_D2_ver_330_ASRP_1280.bin"

// VT ACOUSTIC MODEL
#define Acoustic_Model_filename  "Models/109_hbg_search19_p.bin"

// DBMD2 loader for using uart on 26Mhz clock system
#define I2C_to_UART_filename "I2C_to_UART_3M8n1_using_26M.bin"
#define I2C_to_SPI_filename "Firmware/D2_swich_I2C_to_SPI.bin"

// ASRP parameter files defintons per secnario
char * Asrp_Param_filename[]=
{
	//"AsrpParams_Logitech_T040_v1206.bin",
	"ASRP/AsrpParams_2Mic_Nr_1280_046.bin" ,
	"ASRP/AsrpParams_2Mic_AecNr_1280_046.bin",
};

int state = 0;
int vt_cnt = 0;

// RPI interface type
enum interafce_type {
	 UART_INTERFACE = 0 ,
	 I2C_INTERFACE ,
	 SPI_INTERFACE
};

#define UART_SPEED	3000000	
#define SPI_SPEED 	3000000

////////////////////////////////////////////////////////////////////////////////
// list of functions

void wakeup_high();
void wakeup_low();
void init_RPI_board();
int16_t swap_bytes(int16_t num);
int16_t read_register(int16_t reg);
void write_register(int16_t reg, int16_t val);
int load_file(char *filename, int fd, int skip_bytes);
void pull_buffer(int);
int poll_pin(int gpio);
int16_t  rd_d2_cmd(int16_t reg);
int16_t  rd_d4_cmd(int16_t reg);
void send_d2_cmd(int16_t reg,int16_t val);
void send_d4_cmd(int16_t reg,int16_t val);
void print_logo(void);
void testcolor(int attr, int fg, int bg);
int use_case_0(void);
int load_amodel_file(char *filename, int fd);
int16_t read_register_spi(int fd_spi, int16_t reg);
void write_register_spi(int fd_spi, uint8_t reg, int16_t val);
int spi_read(int fd, unsigned char *buf, int len);
int spi_write(int fd, unsigned char *buf, int len);
void spi_release(int fd);
int user_space_playback_16b(const char *path_to_song);
int user_space_playback_32b(const char *path_to_song);
int user_space_record(const char *path_to_file, int rate, int duration, int bits);
unsigned long get_file_size(char *filename);

int Load_A_Model();
void write_to_i2c_bus(int address, int param);

////////////////////////////////////////////////////////////////////////////////
// Global variable definitions

int i2c; 
int spi; 
int spi1; 
#define spiD4 spi
#define spiD2 spi1
int serial; 
int chnl_desc ;
int chnl_desc1 ;

int wcount = 0; 
enum interafce_type 	D2_interface ; 
enum interafce_type 	D4_interface ;
int ASRP_run_on_chip ;   
bool DBMD4_got_interrupt_flag ;

int music_pid = 0;
int record_pid = 0;
int music_on = 1;

uint16_t MIC24;
uint16_t MIC25;
uint16_t MIC26;

void delay(unsigned int msecond)
{
#if _POSIX_C_SOURCE >= 199309L
	struct timespec ts;
	ts.tv_sec = msecond / 1000;
	ts.tv_nsec = (msecond % 1000) * 1000000;
	nanosleep(&ts, NULL);
#else
	usleep(msecond * 1000);
#endif
}

static volatile int keepRunning = 1;

/* Signal handling Ctrl+C to exit program correctly */
void intHandler(int dummy)
{
	printf("[intHandler]: Got Ctrl-C.\n");
	spi_release(spi);
	spi_release(spi1);
	spi_release(i2c);
	keepRunning = 0;

	if(music_on){
		music_on = 0;
		system("killall -9 aplay");
		delay(1000);
	}
	
	if(music_pid > 0)
		system("killall -9 aplay");

	if(record_pid > 0)
		system("killall -9 arecord");

	exit(0);
}

////////////////////////////////////////////////////////////////////////////////
// Helper Function definitions
// RPI GPIO

//*****************************************************************************/
//  FUNCTION NAME: atoh__(const char *String)
//
//  DESCRIPTION:
//		Convert Ascci to Hex.
//
//  PARAMETERS:
//      const char *String
//
//  RETURNS:
//      integer value
//*****************************************************************************/

static unsigned int atoh__(const char *String)
{
    unsigned int Value = 0, Digit;
    char c;

    while ((c = *String++) != '\0')
    {
        if (c >= '0' && c <= '9')
            Digit = (uint) (c - '0');
        else if (c >= 'a' && c <= 'f')
            Digit = (uint) (c - 'a') + 10;
        else if (c >= 'A' && c <= 'F')
            Digit = (uint) (c - 'A') + 10;
        else
            break;

        Value = (Value << 4) + Digit;
    }

    return Value;
}

//*****************************************************************************/
//  FUNCTION NAME: gpio_platform_request()
//
//  DESCRIPTION:
//		Export GPIO pin number from RPI linux system.
//
//  PARAMETERS:
//      int pin
//		const char* name
//  RETURNS:
//      Execution status
//*****************************************************************************/
int gpio_platform_request(int pin, const char* name)
{
#define BUFFER_MAX 4
	char buffer[BUFFER_MAX];
	ssize_t bytes_written;
	int fd;

	fd = open("/sys/class/gpio/export", O_WRONLY);
	if(fd < 0) {
		fprintf(stderr, "Failed to open export for writing!\n");
		return(-1);
	}

	bytes_written = snprintf(buffer, BUFFER_MAX, "%d", pin);
	write(fd, buffer, bytes_written);
	close(fd);
	return(0);
}

//*****************************************************************************/
//  FUNCTION NAME: gpio_platform_get_value()
//
//  DESCRIPTION:
//		return the gpio value.
//
//  PARAMETERS:
//      int pin
//  RETURNS:
//     the gpio value or error flag -1
//*****************************************************************************/
int gpio_platform_get_value(int pin)
{
#define VALUE_MAX 30
	char path[VALUE_MAX];
	char value_str[3];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_RDONLY);
	if (fd < 0) {
		fprintf(stderr, "Failed to open gpio value for reading!\n");
		return(-1);
	}

	if (-1 == read(fd, value_str, 3)) {
		fprintf(stderr, "Failed to read value!\n");
		close(fd);
        return(-1);
	}

	close(fd);

	return(atoi(value_str));
}

//*****************************************************************************/
//  FUNCTION NAME: gpio_platform_set_value()
//
//  DESCRIPTION:
//		set the gpio value to value.
//
//  PARAMETERS:
//      int pin
//		int value
//  RETURNS:
//     0 - on sucsses
//	   else -1 on error
//*****************************************************************************/
int gpio_platform_set_value(int pin, int value)
{
	static const char s_values_str[] = "01";

	char path[VALUE_MAX];
	int fd;

	snprintf(path, VALUE_MAX, "/sys/class/gpio/gpio%d/value", pin);
	fd = open(path, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr, "Failed to open gpio value for writing!\n");
		return(-1);
	}

	if (1 != write(fd, &s_values_str[LOW == value ? 0 : 1], 1)) {
		fprintf(stderr, "Failed to write value!\n");
		close(fd);
                return(-1);
	}

	close(fd);
	return(0);
}
#define ARCH_NR_GPIOS 200

//*****************************************************************************/
//  FUNCTION NAME: gpio_platform_is_valid()
//
//  DESCRIPTION:
//		return True if gpio is valid.
//
//  PARAMETERS:
//      int pin
//  RETURNS:
//     1 - when valid
//	   else 0
//*****************************************************************************/

int gpio_platform_is_valid(int pin)
{
     return pin >= 0 && pin < ARCH_NR_GPIOS;
}

//*****************************************************************************/
//  FUNCTION NAME: gpio_platform_direction_input()
//
//  DESCRIPTION:
//		Set gpio direction to input.
//
//  PARAMETERS:
//      int pin
//  RETURNS:
//     0 - on sucsses
//	   else -1 on error
//*****************************************************************************/
int gpio_platform_direction_input(int pin)
{
#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		return(-1);
	}

	if (-1 == write(fd, "in", 2)) {
		fprintf(stderr, "Failed to set direction!\n");
		close(fd);
                return(-1);
	}

    DEBUG_PRINT("/sys/class/gpio/gpio%d/direction =====================>\n", pin);

	close(fd);
	return(0);
}

//*****************************************************************************/
//  FUNCTION NAME: gpio_platform_direction_output()
//
//  DESCRIPTION:
//		Set gpio direction to outnput.
//
//  PARAMETERS:
//      int pin
//  RETURNS:
//     0 - on sucsses
//	   else -1 on error
//*****************************************************************************/
int gpio_platform_direction_output(int pin, int value)
{
#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/direction", pin);
	fd = open(path, O_WRONLY);
	if (fd < 0) {
		fprintf(stderr, "Failed to open gpio direction for writing!\n");
		return(-1);
	}

	if (-1 == write(fd, "out", 3)) {
		fprintf(stderr, "Failed to set direction!\n");
		close(fd);
                return(-1);
	}

	close(fd);

    return gpio_platform_set_value(pin, value);
}


static const char *gpio_platform_irq_egde_names[4] = {
	"none",
	"rising",
	"falling",
	"both",
};

//*****************************************************************************/
//  FUNCTION NAME: gpio_platform_to_irq()
//
//  DESCRIPTION:
//		Set gpio change event to irq event.
//
//  PARAMETERS:
//      int pin
//  RETURNS:
//     0 - on sucsses
//	   else -1 on error
//*****************************************************************************/
int gpio_platform_to_irq(int pin, int irq_type)
{
#define DIRECTION_MAX 35
	char path[DIRECTION_MAX];
	int fd;

	snprintf(path, DIRECTION_MAX, "/sys/class/gpio/gpio%d/edge", pin);
	fd = open(path, O_RDWR);
	if (fd < 0) {
		fprintf(stderr, "Failed to open gpio edge for writing!\n");
		return(-1);
	}

	if (-1 == write(fd, gpio_platform_irq_egde_names[irq_type],
                strlen(gpio_platform_irq_egde_names[irq_type]))) {
		fprintf(stderr, "Failed to set gpio irq egde!\n");
		close(fd);
        return(-1);
	}

	close(fd);
	return(0);
}

//*********************************************************************/
//  FUNCTION NAME: poll_pin()
//
//  DESCRIPTION:
//		raise aflag if interrupt happend
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses elese error
//*********************************************************************/
int poll_pin(int gpio)
{
        struct pollfd fdlist;
        int fd;
        char buf[64] = {0};
        char c;
        int err;
        //int16_t value;
        /* int16_t X, Y; */
        struct timeval tv;

        gettimeofday(&tv, NULL);
        //printf("[%d.%d]][%s]: gpio_platform_get_value(%d) = %d\n",
        //     (int)tv.tv_sec, (int)tv.tv_usec, __func__, gpio, gpio_platform_get_value(gpio));

        sprintf(buf, "/sys/class/gpio/gpio%d/value", gpio);
        fd = open(buf, O_RDWR);

		err = read(fd, &c, sizeof(c));

#if 0
		value = read_register_spi(spiD4, FW_VERSION_NUMBER);
		if (value != 0) {
			DEBUG_PRINT("D4 FirmWare version = 0x%X\n", value);
		} else {
			printf ("FAIL read_register(FW_VERSION_NUMBER)\n");
		}

		write_register_spi(spiD4, 0x05, 0x50);
		write_register_spi(spiD4, 0x06, 0x300);

		X = read_register_spi(spiD4, 0x07);
		Y = read_register_spi(spiD4, 0x08);
		printf ("read D4 reg 0x03000050 = 0x%X\n", (Y << 16 | (X & 0xffff)));
#endif

		fdlist.fd = fd;
		fdlist.events = POLLPRI;

		err = lseek(fd, 0, SEEK_SET);
		if(-1 == err) {
			perror("lseek");
			return -1;
		}

        while(1)
        {
			read(fd, buf, 1);
			if(poll(&fdlist, 1, -1) == -1) {
				printf("poll_pin\n");
				return -1;
			} else {

				err = lseek(fd, 0, SEEK_SET);
				if(-1 == err) {
					perror("lseek");
					return -1;
				}

				err = read(fd, &c, sizeof(c));
				if((c - '0') == 1) {
					printf("event on pin %d, value = %d \n", gpio, (c - '0'));
				}

				err = lseek(fd, 0, SEEK_SET);
				if(-1 == err) {
					perror("lseek");
					return -1;
				}
				return 1;
			}
		}

	printf("poll_pin finish\n");
	return c - '0';
}


////////////////////////////////////////////////////////////////////////////////
// Helper Function definitions
// DBMDX

void spi_release(int fd)
{
	if(fd > 0) {
		close(fd);
		fd = -1;
	}
}

static void pabort(const char *s)
{
	perror(s);
	spi_release(spiD4);
	spi_release(i2c);
	abort();
}

/*
 * DBMD4 SPI read
 * Read len bytes from SPI file descriptor
 */
int spi_read(int fd, unsigned char *buf, int len)
{
	int rc = 0;
	rc = read(fd, buf, len);
	if (rc < 0){
		DEBUG_PRINT("[spi_read] return %d\n", rc);
		pabort("[spi_read]: can't read spi message.");
	}

	return rc;
}

/*
 * DBMD4 SPI write
 * Write len bytes to SPI file descriptor
 */
int spi_write(int fd, unsigned char *buf, int len)
{
	int rc = 0;
	rc = write(fd, buf, len);
	usleep(500);
	//DEBUG_PRINT("[spi_write] return %d\n", rc);
	if (rc < 0){
		DEBUG_PRINT("[spi_write] return %d\n", rc);
		pabort("[spi_read]: can't send spi message.");
	}

	return rc;
}



//*****************************************************************************/
//  FUNCTION NAME: read_register()
//
//  DESCRIPTION:
//		read dbmd4 register value using uart (DBMD4).
//
//  PARAMETERS:
//     int16_t reg
//  RETURNS:
//     register value (int16_t)
//*****************************************************************************/
int16_t read_register(int16_t reg)
{
	int16_t val;
	val = read_register_spi(spi, reg);
	return val;	
}

/*
 * DBMDX SPI read register
 * Send via SPI ascii buffer
 * ascii buffer of 3 bytes, format is:
 * 2 bytes - register address in ascii format.
 * 1 byte contains the character 'r' for read command
 *
 * Receive via SPI ascii buffer
 * Read via SPI 5 bytes
 * byte[0] - not relevant
 * bytes[1-4] - ascii string
 */
int16_t read_register_spi(int fd_spi, int16_t reg)
{
	uint16_t val = 0;
	int ret;
	unsigned char Buff[10] = {0};
	char RegSet[18] = {0};

	sprintf(RegSet, "%03xr", reg);

	ret = spi_write(fd_spi, (unsigned char*)RegSet, strlen(RegSet));
	delay(10);
	if(ret != strlen(RegSet))
		printf("[%s]: spi_write error %d.\n", __func__, ret);
	ret = spi_read(fd_spi, Buff, 5);
	if(ret != 5)
		printf("[%s]: spi_read error.\n", __func__);

	Buff[5] = 0;
	val = atoh__((char*)&Buff[1]);
	if(val < 0) {
		printf("[%s]: spi_read error.\n", __func__);
	}


	printf("[%s]: spi_read return val = 0x%X.\n", __func__, val);
	return val;
}

/*
 * DBMDX SPI write register
 * Send via SPI ascii buffer
 * ascii buffer of 7 bytes, format is:
 * 2 bytes - register address in ascii format
 * 1 byte contains the character 'w' for write command
 * 4 bytes contains the value to be written to register in ascii format.
 */
void write_register_spi(int fd_spi, uint8_t reg, int16_t val)
{
	char str[16];
	sprintf(str, "%03xw%04x", reg, (val)&0xffff);
	printf("[%s]: %s\n",__func__, str);
	spi_write(fd_spi, (unsigned char*)str, strlen(str));

#ifdef DEBUG	/* delay 10ms to let fw output uart logs*/
	delay(10);
#endif
}



//*****************************************************************************/
//  FUNCTION NAME: write_register()
//
//  DESCRIPTION:
//		write value to DBMD4 register (uart ).
//
//  PARAMETERS:
//     char data
//  RETURNS:
//*****************************************************************************/

void write_register(int16_t reg, int16_t val)
{
	write_register_spi(spi, reg, val);
}


//*****************************************************************************/
//  FUNCTION NAME: read_registerI2C()
//
//  DESCRIPTION:
//		read dbmd2 register value using I2C (DBMD2).
//
//  PARAMETERS:
//     int16_t reg
//  RETURNS:
//     register value (int16_t)
//*****************************************************************************/
int16_t read_registerI2C(int16_t reg)
{
	int16_t val; 
	val = rd_d2_cmd(reg);
	return val;	
}

//*****************************************************************************/
//  FUNCTION NAME: write_registerI2C()
//
//  DESCRIPTION:
//		write value to DBMD2 register (I2C ).
//
//  PARAMETERS:
//     char data
//  RETURNS:
//*****************************************************************************/

void write_registerI2C(int16_t reg, int16_t val)
{
	send_d2_cmd(reg, val);
}

//*****************************************************************************/
//  FUNCTION NAME: ASRP_read_register()
//
//  DESCRIPTION:
//		read dbmd2 ASRP register value using I2C (DBMD2).
//      using indirect commands since the address of registers are above 0x100
//
//  PARAMETERS:
//     int16_t reg
//  RETURNS:
//     register value (int16_t)
//*****************************************************************************/
int16_t ASRP_read_register(int16_t reg)
{
	int16_t val; 
	if (ASRP_run_on_chip == DBMD2)
	{
		send_d2_cmd(0x3d, reg);
		val = rd_d2_cmd(0x3f);
	}
	else {
		send_d4_cmd(0x3d,reg);
		val = rd_d4_cmd(0x3f);
	}
//	printf ("Register 0x%d = 0x%X = %d\n",reg, (val&0xffff), (val&0xffff));
	return val;	
		
}

//*****************************************************************************/
//  FUNCTION NAME: ASRP_write_registerI2C()
//
//  DESCRIPTION:
//		read dbmd2 ASRP register value using I2C (DBMD2).
//      using indirect commands since the address of registers are above 0x100
//  PARAMETERS:
//     char data
//  RETURNS:
//*****************************************************************************/
int16_t ASRP_write_register(int16_t reg, int16_t val)
{
	if (ASRP_run_on_chip == DBMD2)
	{
		send_d2_cmd(0x3d,reg);
		val = rd_d2_cmd(0x3f);
	}
	else {
		send_d4_cmd(0x3d,reg);
		val = rd_d4_cmd(0x3f);
	}
	write_registerI2C(0x3d,reg);
	write_registerI2C(0x3e,val);
//	printf ("Register 0x%d = 0x%X = %d\n",reg, (val&0xffff), (val&0xffff));
	return val;	
		
}


//*****************************************************************************/
//  FUNCTION NAME: read_checksum()
//
//  DESCRIPTION:
//		Send check sum command to DBMDX using uart and comprae the results
//		to the expeceted checksum.
//
//  PARAMETERS:
//     char *checksum - array of 4 char the expected checksum
//  RETURNS:
//        0 - checksum is OK
// 		  else error
//*****************************************************************************/
int read_checksum(char *checksum , int fd)
{
	char buf[] = {0x5A, 0x0E};
	char c[7];
	int rc ;

	write(fd, buf, 2);
	delay(10);
	read(fd,c,7);
	delay(1);

#if 0
	if(fd == spiD4) {
#endif		
		rc = strncmp(&c[3],checksum, 4);
		if (rc)
			printf ("[%s]: checksum error : got: = %2x %2x %2x %2x expected : %2x %2x %2x %2x\n",
				__func__, c[3],c[4],c[5],c[6],checksum[0],checksum[1],checksum[2],checksum[3]);
		else
			printf("[%s]: checksum pass\n", __func__);
#if 0		
	} else {
		rc = strncmp (&c[2],checksum,4);

		if (rc)
			printf ("[%s]: checksum error : got: = %2x %2x %2x %2x %2x expected : %2x %2x %2x %2x\n",
				__func__, c[2],c[3],c[4],c[5],c[6], checksum[0],checksum[1],checksum[2],checksum[3]);
		else
			printf("[%s]: checksum pass.\n", __func__);
	}
#endif
    return rc ;
}

/*****************************************************************************/
//  FUNCTION NAME: wakeup_high()
//
//  DESCRIPTION:
//		set wake up gpio to high
//
//  PARAMETERS:
//
//  RETURNS:
//
//*****************************************************************************/
void wakeup_high()
{
	gpio_platform_set_value(WAKEUP_GPIO, HIGH);
}

/*****************************************************************************/
//  FUNCTION NAME: wakeup_low()
//
//  DESCRIPTION:
//		set wake up gpio to low
//
//  PARAMETERS:
//
//  RETURNS:
//
//*****************************************************************************/
void wakeup_low()
{
	gpio_platform_set_value(WAKEUP_GPIO, LOW);
}

/*
 * DBMD4 spi_platform_open_file
 * Open SPI file descriptor
 */
int spi_platform_open_file(const char *device)
{
	/* Open the SPI device */
	spiD4 = open(device, O_RDWR);
	if(spiD4 == -1) {
		printf("[%s]: SPI device node open failed\n", __func__);
		return -1;
	}
	printf("[%s]: SPI successfully opened\n", __func__);
	return 0;
}


void set_iom()
{
#ifdef DBMD5
	int16_t X, Y;
	write_registerI2C(0x05, 0x50);	
	write_registerI2C(0x06, 0x300);	
	
	X = read_registerI2C(0x07);
	Y = read_registerI2C(0x08);
	printf ("read D2reg 0x03000050 = 0x%X\n", (Y << 16 | (X & 0xffff)));
	
	write_registerI2C(0x05, 0x50);	
	write_registerI2C(0x06, 0x300);	
	write_registerI2C(0x07, 0);	
	write_registerI2C(0x08, 0);	

	write_registerI2C(0x05, 0x50);	
	write_registerI2C(0x06, 0x300);	
	
	X = read_registerI2C(0x07);
	Y = read_registerI2C(0x08);
	printf ("read D2 reg 0x03000050 = 0x%X\n", (Y << 16 | (X & 0xffff)));


	write_registerI2C(0x05, 0x48);	
	write_registerI2C(0x06, 0x300);	
	
	X = read_registerI2C(0x07);
	Y = read_registerI2C(0x08);
	printf ("read D@ reg 0x03000048 = 0x%X\n", (Y << 16 | (X & 0xffff)));
	
	write_registerI2C(0x05, 0x48);	
	write_registerI2C(0x06, 0x300);	
	write_registerI2C(0x07, (X | 0x04));	
	write_registerI2C(0x08, Y);	

	write_registerI2C(0x05, 0x4c);	 // all tdm 
	write_registerI2C(0x06, 0x300);	
	write_registerI2C(0x07, 0);	
	write_registerI2C(0x08, 0x0000);	
#else
	int16_t X, Y;
	write_registerI2C(0x05, 0x50);	
	write_registerI2C(0x06, 0x300);	
	
	X = read_registerI2C(0x07);
	Y = read_registerI2C(0x08);
	printf ("read D2reg 0x03000050 = 0x%X\n", (Y << 16 | (X & 0xffff)));
	
	write_registerI2C(0x05, 0x50);	
	write_registerI2C(0x06, 0x300);	
	write_registerI2C(0x07, X);	
	write_registerI2C(0x08, (Y & 0x030F));	

	write_registerI2C(0x05, 0x50);	
	write_registerI2C(0x06, 0x300);	
	
	X = read_registerI2C(0x07);
	Y = read_registerI2C(0x08);
	printf ("read D2 reg 0x03000050 = 0x%X\n", (Y << 16 | (X & 0xffff)));


	write_registerI2C(0x05, 0x48);	
	write_registerI2C(0x06, 0x300);	
	
	X = read_registerI2C(0x07);
	Y = read_registerI2C(0x08);
	printf ("read D@ reg 0x03000048 = 0x%X\n", (Y << 16 | (X & 0xffff)));
	
	write_registerI2C(0x05, 0x48);	
	write_registerI2C(0x06, 0x300);	
	write_registerI2C(0x07, (X | 0x04));	
	write_registerI2C(0x08, Y);	

	write_registerI2C(0x05, 0x4c);	
	write_registerI2C(0x06, 0x300);	
	write_registerI2C(0x07, 0);	
	write_registerI2C(0x08, 0x0001);	
#endif
}

void set_hardware_bypass()
{
	int16_t X, Y;
	printf("[%s]: Start.\n", __func__);
	
	write_register_spi(spi1, 0x05, 0x48);	
	write_register_spi(spi1, 0x06, 0x300);	
	
	X = read_register_spi(spi1, 0x07);
	printf("read D2reg 0x07 = 0x%X\n", X);
	Y = read_register_spi(spi1, 0x08);
	printf("read D2reg 0x08 = 0x%X\n", Y);
	
	write_register_spi(spi1, 0x05, 0x48);	
	write_register_spi(spi1, 0x06, 0x300);	
	write_register_spi(spi1, 0x07, (X|0xA0)); /* TDM0 -> TDM3, TDM bypass enable*/	
	write_register_spi(spi1, 0x08, Y);	

	write_register_spi(spi1, 0x05, 0x4C);	
	write_register_spi(spi1, 0x06, 0x300);	
	write_register_spi(spi1, 0x07, 0x5500);	
	write_register_spi(spi1, 0x08, 0x0AAA);
	delay(200);
	printf("[%s]: Finish.\n", __func__);
}

/*****************************************************************************/
//  FUNCTION NAME: init_RPI_board()
//
//  DESCRIPTION:
//		init RPI  alloocate gpios , i2c channel and uart
//  PARAMETERS:
//     
//  RETURNS:
//        
//*****************************************************************************/
void init_RPI_board()
{
	int i2c_address = 0x3e;
	char *i2c_dev = "/dev/i2c-1" ;
	char *spi_dev = "/dev/spidev0.0" ;
	char *spi_dev1 = "/dev/spidev0.1" ;

	int spi_mode = SPI_MODE_0;
	int spi_speed = SPI_SPEED;
	static int bits = 8;
	
	// Open the I2C device
	i2c = open(i2c_dev, O_RDWR);
	if(i2c < 0) {
		printf("i2c < 0\n");
		exit(1);
	}
	
	//select our slave device
	if((ioctl(i2c, I2C_SLAVE, i2c_address)) < 0) {
		printf("i2c faile to set slave address");
		exit (2);
	}	

	// Open the I2C device
	printf("[init_RPI_board]: i2c %d open successfully\n", i2c);
	
	spi = open(spi_dev, O_RDWR);
	if(spi < 0) {
		printf("spi < 0\n");
		exit(1);
	}
	
	//select our slave device
	if((ioctl(spi, SPI_IOC_WR_MODE ,&spi_mode)) < 0) {
		printf("spi failed to set slave mode");
		exit (2);
	}
		
	if((ioctl(spi, SPI_IOC_WR_MAX_SPEED_HZ ,&spi_speed)) < 0) {
		printf("spi failed to set sspeed= %d",spi_speed);
		exit (2);
	}
	
	if((ioctl(spi, SPI_IOC_WR_BITS_PER_WORD, &bits)) < 0) {
		printf("spi failed to set slave mode");
		exit (2);
	}	
	
	spi1 = open(spi_dev1, O_RDWR);
	if(spi < 0) {
		printf("spi < 0\n");
		exit(1);
	}
	
	//select our slave device
	if((ioctl(spi, SPI_IOC_WR_MODE ,&spi_mode)) < 0) {
		printf("spi failed to set slave mode");
		exit (2);
	}
	
	if((ioctl(spi, SPI_IOC_WR_MAX_SPEED_HZ ,&spi_speed)) < 0) {
		printf("spi failed to set sspeed= %d",spi_speed);
		exit (2);
	}
	
	if((ioctl(spi, SPI_IOC_WR_BITS_PER_WORD, &bits)) < 0) {
		printf("spi failed to set slave mode");
		exit (2);
	}	

	wiringPiSetup();
	wiringPiSetupGpio();
	pinMode(RESET_GPIO, OUTPUT);
	
	printf("pinMode(INTERRRUPT_TRIGGER_GPIO, INPUT);\n");
	
	gpio_platform_request(INTERRRUPT_TRIGGER_GPIO, 0);
	gpio_platform_direction_input(INTERRRUPT_TRIGGER_GPIO);
	gpio_platform_request(RESET_GPIO, 0);
	gpio_platform_direction_output(RESET_GPIO,0);
	pinMode(GPIO_13, OUTPUT);
	pinMode(GPIO_26, OUTPUT);
	pinMode(GPIO_25, OUTPUT);
	gpio_platform_request(GPIO_25, 0);
	gpio_platform_direction_output(GPIO_25,0);
	digitalWrite(RESET_GPIO, LOW);
	delay(100);
}

void set_cs_d2()
{
	digitalWrite(GPIO_13, LOW);	
	digitalWrite(GPIO_26, HIGH);	
	delay(50);
}

void set_cs_codec()
{
	digitalWrite(GPIO_13, HIGH);	
	digitalWrite(GPIO_26, LOW);	
	delay(50);
}

/*****************************************************************************/
//  FUNCTION NAME: nuv_write()
//
//  DESCRIPTION:
//		write nuvoton codec register (board specific ).
//  PARAMETERS:
//    	int addr
//		int value 
//  RETURNS:
//        
//*****************************************************************************/
int nuv_write(int addr,int value)
{
	int x ;
	char buf[2];
	addr = addr << 1;
	x = value >> 8 ;
	addr = x + addr;
	buf[0] = addr;
	buf[1] = value & 0xff;
	write(i2c, buf, 2);
	return 0;
}

/*****************************************************************************/
//  FUNCTION NAME: set_codec_i2c
//
//  DESCRIPTION:
//		set the nuvoton codec on EVB (board specific ).
//  PARAMETERS:
//    	
//		
//  RETURNS:
//        
//*****************************************************************************/

void set_codec_i2c()
{
	int err;
	int i2c_nuv_addr = 0x1a; //  nuvoton
	int i2c_address = 0x3E;
//	int i2c_address = 0x34;
	
	char *i2c_dev = "/dev/i2c-1" ;
	
	if (i2c)
		close(i2c);
	delay(100);	
	
	// Open the I2C device
	i2c = open(i2c_dev, O_RDWR);
	if (i2c < 0)
	{
		printf("i2c < 0\n");
		exit(1);
	}
	tcdrain(i2c);
	// CPLD
	if((ioctl(i2c, I2C_SLAVE, i2c_nuv_addr)) < 0) {
		printf("ERROR Selecting I2C device\n");
		exit(2);
	}
	delay(10);	
	//i2c_smbus_write_byte(i2c, &ch);
	//i2c_smbus_write_byte(i2c, buf_cpld);
	//buf_cpld[0] = 0b10101010;
	nuv_write(0x0,0x0); // Reset
	nuv_write(0x1,0x3b); //# Power management 1
	nuv_write(0x4,0x091); //#I2S, stereo
	nuv_write(0x5,0x0); //#No passthrough
	nuv_write(0x7,0x0); //#48KHz filter // 6 for 16khz
	nuv_write(0xf,0x8); //#Left ADC volume
	nuv_write(0x10,0x8); //#Right ADC volume
	nuv_write(0xe,0x8); //#oversampling for better SNR 
	nuv_write(0x2f,0x100); //#Left PGA boost
	nuv_write(0x30,0x100); //# Right PGA boost
	nuv_write(0x0a,0x0); //# DAC control - no SW/auto mute, no oversampling, normal polarities
	nuv_write(0x2,0x1bf); //#Power management 2
	nuv_write(0x3,0x6f); //# Power management 3
	nuv_write(0x2c,0x33); //# microphones control (go through PGA)
	nuv_write(0x2d,0x110); //# Left PGA gain
	nuv_write(0x2e,0x110); //# Right PGA gain
	nuv_write(0x6,0x4c); //# Set PLL for 48KHz
	//#nuv_write(0x6,0xAD); # Set PLL for 16KHz
	nuv_write(AUDIO_PROCESSING_CONFIGURATION,0x9); //# Set left HP volume
	//#nuv_write(0x32, 0x16) # Left Bypass path control from LADC to LMAIN
	//#nuv_write(0x33, 0x16) # Right Bypass path control from RADC to RMAIN
	nuv_write(0x32, 0x15); //# Left path control from LADC to LMAIN
	nuv_write(0x33, 0x15) ;//# Right path control from RADC to RMAIN
	nuv_write(0xb, 0x1ff); //# Left DAC volume - no attenuation
	nuv_write(0xc, 0x1ff); //# Right DAC volume - no attenuation

	printf("Write to nuvoton ended\n");	
	delay(100);
	tcdrain(i2c);
	
	err = close(i2c);
	if (err)
		printf("ERROR writing I2C device %x",err);
	
	// Open the I2C device
	i2c = open(i2c_dev, O_RDWR);
	if (i2c < 0)
	{
		printf("i2c < 0\n");
		exit(1);
	}
	
	
	//select our slave device
	if((ioctl(i2c, I2C_SLAVE, i2c_address)) < 0)
		printf("ERROR Selecting I2C device\n");
	delay(1);
}




/*****************************************************************************/
//  FUNCTION NAME: swap_bytes()
//
//  DESCRIPTION:
//		swapbytes
//  PARAMETERS:
//    int16_t num
//  RETURNS:
//        int16_t  swaped bytes
//*****************************************************************************/

int16_t swap_bytes(int16_t num)
{
	return ( (num << 8) | ((num >> 8) & 0xFF) );
}

/*****************************************************************************/
//  FUNCTION NAME: load_fileI2C()
//
//  DESCRIPTION:
//		load file into buffer and transmit it to DBMD over the i2c
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int load_fileI2C(char *filename, int fd)
{
	FILE *file;
	unsigned long fileLen;
	char *buffer;
	int i ;

	// Open the file
	file = fopen(filename, "rb");
	if (!file) {
		fprintf(stderr, "Unable to open file %s\n", filename);
		return -1 ;
	}

	//Get file length
	fseek(file, 0, SEEK_END); // seek to the end of file
	fileLen=ftell(file); 	  // get current file pointer
	fseek(file, 0, SEEK_SET); // seek back to beginning of file

	//Allocate memory
	buffer = (char *)malloc(fileLen + 1);
	if(!buffer) {
		perror("Memory error!");
        	fclose(file);
		return -1;
	}

	//Read file contents into buffer
	fread(buffer, fileLen, 1, file);
	fclose(file);
	printf("load_file:%s,filelen:%d \n",filename,(int)fileLen);


	// Upload File
	// all buffer required support from i2c limit to 0x8000 on rpi

	for(i = 0 ; i < fileLen; i += 0x200) {
		int chunk ;
		chunk = (0x200 < ((fileLen ) - i) ? 0x200 : (fileLen ) - i);

		if(chunk != write(fd, &buffer[i],chunk)) {
			
			if(chunk == fileLen) {
				printf("load_fileI2C chunk==fileLen = %d *********\n", chunk);
			} else {
				perror("write error loading file\n");
				printf("write error loading file chunk = %d\n", chunk);
				tcdrain(fd);
				free(buffer);
				//return -1;
			}
		}
	}

 	free(buffer);
	return 0;
}

/**********************************************************************/
//  FUNCTION NAME: load_file()
//
//  DESCRIPTION:
//		load file into buffer and transmit it to DBMDX over the UART.
//      the load succssed only if the checksum is correct.
//      it also sends clear crc command before sending the file so the checksum 
//      can be checked correctly.
//      usally the firmware files containt the correct checksum at the end of the firmware file.
//		(4 last charcters of the firmware , the function reads the checksum value from the chip and compare it ). 
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses (passed checksum).
//		  error otherwise
//*********************************************************************/
int load_file(char *filename, int fd, int skip_bytes)
{
	FILE *file;
	unsigned long fileLen;
	char *buffer;
	char *checksum;
	int rc;
	long err = 1;
	int i;
        //const char clr_crc[]= {0x5a,0x03,0x52,0x0a,0x00,0x00,0x0,0x0,0x0,0x0};
        const char clr_crc1[]= {0x5a,0x0f};
	// Open the file
	file = fopen(filename, "rb");
	if(!file) {
		fprintf(stderr, "Unable to open file %s\n", filename);
		err = -1;
		goto out;
	}

	//Get file length
	fseek(file, 0, SEEK_END); 	// seek to the end of file
	fileLen=ftell(file); 		// get current file pointer
	fseek(file, 0, SEEK_SET); 	// seek back to beggining of file

	//Allocate memory
	buffer=(char *)malloc(fileLen+1);
	if(!buffer) {
		fprintf(stderr, "Memory error!");
		fclose(file);
		err = -1;
		goto out;
	}

	//Read file contents into buffer
	fread(buffer, fileLen, 1, file);


	printf("load_file:%s,filelen:%d \n",filename,(int)fileLen);
	
	// Upload File
	if(skip_bytes > 0) {
		checksum = &buffer[fileLen - skip_bytes];
		if((rc = write(fd, clr_crc1, sizeof(clr_crc1))) < 0) {
			perror("load_file: fail to write");
			err = -1;
			goto out;
		}
		printf( "[%s] FirmWare_filename %s write clr_crc1 retutn %d\n", __func__, filename, rc);
	}
		
#if 0
	rc = write(fd, buffer, fileLen - skip_bytes);
	if (rc != fileLen - skip_bytes) {
		perror("load_file: fail to write");
		err = -1;
		goto out;

	} else {
		printf( "[%s] uart write sent %d bytes\n", __func__,(int)fileLen-4);
	}
#else
{
	
	
	printf("****************** %s\n", filename);
	
	for(i = 0 ; i < (fileLen - skip_bytes); i += 0x200) {
		int chunk ;
		chunk = (0x200 < ((fileLen - skip_bytes) - i) ? 0x200 : (fileLen - skip_bytes) - i);
		if(chunk != write(fd, &buffer[i], chunk)) {
			perror("error loading file\n");
			free(buffer);
			err = -1;
			goto out;
		}
	}
}
#endif	

	if(skip_bytes > 0)
		rc = read_checksum(checksum, fd);

	fclose(file);
	free(buffer);	

out:
	return err;
	
}

int load_amodel_file(char *filename, int fd)
{
	FILE *file;
	unsigned long fileLen;
	unsigned char *buffer;
	long err = 1;
	unsigned char header_buffer[10];
	uint32_t num_of_words;
	int i;

	/* Open the file */
	file = fopen(filename, "rb");
	if (!file) {
		fprintf(stderr, "Unable to open file %s\n", filename);
		err = -1;
		goto out;
	}

	/* Get file length */
	fseek(file, 0, SEEK_END); 	/* seek to the end of file */
	fileLen = ftell(file); 		/* get current file pointer */
	fseek(file, 0, SEEK_SET); 	/* seek back to beggining of file */

	printf("[load_amodel_file]: %s, fileLen:%d \n", filename, (unsigned int)fileLen);

	for(i = 0; i < 2; i++) {

		fread(header_buffer, sizeof(header_buffer), 1, file);
		if(header_buffer[0] != 0x5A && (header_buffer[1] != 0x01 || header_buffer[1] != 0x02)) {
			printf("[load_amodel_file]: header error.\n");
			err = -1;
			goto out;
		}

		spi_write(fd, &header_buffer[0], sizeof(header_buffer));
		delay(100);

		num_of_words = ((uint32_t)header_buffer[5] << 24)
				| ((uint32_t)header_buffer[4] << 16)
				| ((uint32_t)header_buffer[3] << 8)
				| ((uint32_t)header_buffer[2] << 0);

		printf("[load_amodel_file]: num_of_words:%d \n", num_of_words);

		/* Allocate memory */
		buffer = (unsigned char*)malloc(num_of_words*2);
		if (!buffer) {
			fprintf(stderr, "Memory error!");
			fclose(file);
			err = -1;
			goto out;
		}

		fread(buffer, num_of_words * 2, 1, file);
		spi_write(fd, &buffer[0], num_of_words * 2);
		delay(100);

		free(buffer);
	} /* for(i = 0; i < 2; i++) */

	fclose(file);

out:
	return err;
}

int load_fileD2(char *filename,int fd)
{
	FILE *file;
	unsigned long fileLen;
	char *buffer;
	char *checksum;
	int rc ;
        const char clr_crc[]= {0x5a,0x03,0x52,0x0a,0x00,0x00,0x0,0x0,0x0,0x0};
        //const char clr_crc1[]= {0x5a,0x0f};
	// Open the file
	file = fopen(filename, "rb");
	if (!file)
	{
		fprintf(stderr, "Unable to open file %s\n", filename);
		return 0;
	}

	//Get file length
	fseek(file, 0, SEEK_END); 	// seek to the end of file
	fileLen=ftell(file); 		// get current file pointer
	fseek(file, 0, SEEK_SET); 	// seek back to beggining of file

	//Allocate memory
	buffer=(char *)malloc(fileLen+1);
	if (!buffer)
	{
		fprintf(stderr, "Memory error!");
		fclose(file);
		return 0;
	}

	//Read file contents into buffer
	fread(buffer, fileLen, 1, file);


	printf("load_file:%s,filelen:%d \n",filename,(int)fileLen);
	// Upload File
	checksum = &buffer[fileLen - 4];

	rc = write(fd, clr_crc, sizeof(clr_crc));
	printf( "[%s] FirmWare_filename %s write clr_crc1 retutn %d\n", __func__, filename, rc);
		
#if 0
	rc = write(fd, buffer, fileLen - 4);
	if (rc != fileLen - 4)
	{
		perror("load_file: fail to write");
		exit(0);
	}
	else 
	{
		printf( "[%s] uart write sent %d bytes\n", __func__,(int)fileLen-4);
	}
#else
{
	int i;
	printf("****************** %s\n", filename);
	
	for ( i = 0 ; i < (fileLen - 4) ; i += 0x200)
	{
		int chunk ;
		chunk = (0x200 < ((fileLen - 4) - i) ? 0x200 : (fileLen - 4) - i);

		if ( chunk != write(fd, &buffer[i], chunk))
		{
			perror("error loading file\n");
			tcdrain(fd);
			free(buffer);
		}
	}
}
#endif	

	rc = read_checksum(checksum, fd);

	fclose(file);
	free(buffer);	
	return rc ;
}

/**********************************************************************/
//  FUNCTION NAME: dump_audio_data(char *buf, int max_size)
//
//  DESCRIPTION:
//	read the streamimg from DBMD4 chip and collect them into file 
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        
//*********************************************************************/
int dump_audio_data(char *buf, int max_size)
{
        uint16_t remained_samples_in_d4;
        int     bytes_to_read;
        uint8_t header[2];
        uint8_t *d4_buf;
		uint8_t retry_counter = 0;

        d4_buf = malloc(max_size + 4);
        if(d4_buf <= 0 )
        {
                printf("%s malloc failed. \n",__func__);
                return 0;
        }

		while(1){
        	remained_samples_in_d4 = read_register_spi(spi, NUMBER_OF_SAMPLES_IN_BUFFER);

			if ((remained_samples_in_d4 == 0) && (retry_counter < 10) ){
				retry_counter += 1;
				delay(20);
				continue;
			}
			else if(retry_counter >= 10){
				printf("Buffer is full of Zero !\n ");
				free(d4_buf);
				return 0;
			}
			
	        if(remained_samples_in_d4 == 0xffff)
	        {
	            printf("audio buffer is not in streaming. \n");
				free(d4_buf);
	            return 0; /* no more data. */
	        }

			break;
		}

		printf("d4 saved buffer samples(%d).\n",remained_samples_in_d4);
        bytes_to_read = remained_samples_in_d4 * 16 ;

        if(bytes_to_read > max_size)
		bytes_to_read = max_size;

        printf("bytes_to_read:%d, max_size:%d \n",bytes_to_read, max_size);
        write_register_spi(spi, READ_AUDIO_BUFFER, (bytes_to_read/16));
        spi_read(spi, header, 2);
        spi_read(spi, d4_buf, bytes_to_read);
        memcpy(buf, d4_buf, bytes_to_read);
		free(d4_buf);
        return bytes_to_read;
}

/*****************************************************************************/
//  FUNCTION NAME: pull_buffer()
//
//  DESCRIPTION:
//				read the streamimg from DBMD4 chip and collect them into file 
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        
//*****************************************************************************/
void pull_buffer(int mode)
{
    FILE *audio_file = NULL;
    int buffer_time_in_sec = 5;
    int data_chunk_size = 0xff;
    int Fifo_size = buffer_time_in_sec*16000*16/8; /*(time * sample_rate * bits_per_sample / convert_from_bits_to_bytes) */
    int current_data_count = 0 ;
    char *buf;
    int read_len;
    int total_bytes;
    //int ret;
    //int word_id;

    audio_file = fopen ("audio_file.raw", "wb");
    if (audio_file == NULL)
    {
            printf ("ERROR in opening file \n");
            return;
    }

    buf = malloc(data_chunk_size*16 + 4);
    if(buf <= 0)
    {
            printf("%s malloc error. \n",__func__);
            return;
    }
#if 0	
    printf ("Start downloading %d seconds of audio from buffer.\n",buffer_time_in_sec);
    printf ("Please wait ...\n");

	if (mode == 1 ) {
		write_register_spi(spi1, TDM_Activation_Control,0x3008);
		write_register_spi(spi, TDM_Activation_Control,0x0C04);
	} else if(mode == 2) {
		send_d2_cmd(TDM_Activation_Control, 0xb108);
		send_d4_cmd(TDM_Activation_Control, 0x0C04);
	}	
 			
	ret = poll_pin(INTERRRUPT_TRIGGER_GPIO);
	if(ret != 1) {
		printf ("[%s]: poll_pin return \n", __func__);
		use_case_0();		
		return ;
	}
	
	word_id = read_register_spi(spi, 0x5c);
	printf ("detection  word id = %x\n", word_id);
#endif

	printf ("Start downloading %d seconds of audio from buffer.\n",buffer_time_in_sec);
	printf ("Please wait ...\n");
	delay(500);

    total_bytes = Fifo_size;

    while(total_bytes >= (data_chunk_size * 16))
    {
        delay(1);
        read_len = dump_audio_data(buf,data_chunk_size * 16);
		if(read_len <= 0){
			printf ("pulling audio data fail!\n");
			break;
		}
        fwrite(buf,read_len,1,audio_file);
        current_data_count += read_len;
        total_bytes -= read_len;
    	printf("[Step1]current_data_count:%d,Fifo_size(%d),total_bytes(%d) \n",current_data_count,Fifo_size,total_bytes);
    }

    while(total_bytes > 0)
    {
        delay(1);
        read_len = dump_audio_data(buf,total_bytes);
		if(read_len <= 0){
			printf ("pulling audio data fail!\n");
			break;
		}		
        fwrite(buf,read_len,1,audio_file);
        current_data_count += read_len;
        total_bytes -= read_len;
   		printf("[Step2] current_data_count:%d,Fifo_size(%d),total_bytes(%d) \n",current_data_count,Fifo_size,total_bytes);
    }
    printf("[END]current_data_count:%d,Fifo_size(%d),total_bytes(%d) \n",current_data_count,Fifo_size,total_bytes);

	free(buf);
	printf ("End of pulling audio data!\n");
	fclose (audio_file);
}

#define RESET		0
#define BRIGHT 		1
#define DIM 		2
#define UNDERLINE 	3
#define BLINK 		4
#define REVERSE 	6
#define HIDDEN 		8

#define BLACK		0
#define RED 		1
#define GREEN 		2
#define YELLOW 		3
#define BLUE 		4
#define MAGENTA 	5
#define CYAN 		6
#define WHITE 		7

/* Commands */
#define D2FWVER "d2 fwver"
#define D4FWVER "d4 fwver"
#define RECD4 "d4 rec"
#define RECD2 "d2 rec"
#define RECASRP "asrp rec"
#define RSTOP "d4 stop"

#define D2OPMODE "d2 opmode"
#define D4OPMODE "d4 opmode"

void usage()
{
	testcolor(BRIGHT, RED, BLACK);
	printf("\n-------------- Operation Instructions ------------\n");
	//printf("[%s] / [%s]\n", D2FWVER, D4FWVER);
	//printf("[%s] / [%s]\n", D2OPMODE, D4OPMODE);
	
	printf("[d2/d4 fwver]\n");
	printf("[d2/d4 opmode]\n");
	printf("[DM1]	- (D4&D5) Voice Trigger\n");
	printf("[DM2]	- (D4&D5) Voice Trigger and Buffering\n");
	printf("[DM3]	- (D5) Music, Voice Trigger and Buffering\n");	
	printf("Use [exit] or [q] to close the program.\n\n");
	printf ("Enter command:\n");
	testcolor(RESET, WHITE, BLACK);
}

/*****************************************************************************/
//  FUNCTION NAME: send_d4_cmd()
//
//  DESCRIPTION:
//		send command to dbmd4 using uart
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
void send_d4_cmd(int16_t reg, int16_t val)
{
	write_register_spi(spi, reg, val);
}

/*****************************************************************************/
//  FUNCTION NAME: send_d2_cmd()
//
//  DESCRIPTION:
//		send command to dbmd2 using i2c
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
void send_d2_cmd(int16_t reg, int16_t val)
{
	write_register_spi(spi1, reg, val);
}

/*****************************************************************************/
//  FUNCTION NAME: rd_d2_cmd()
//
//  DESCRIPTION:
//		read cmd reg from DBMD2
//  PARAMETERS:
//    int16_t reg
//  RETURNS:
//        int16_t val the register vakue
//*****************************************************************************/
int16_t  rd_d2_cmd(int16_t reg)
{
	int16_t val = 0;
	val = read_register_spi(spi1, reg);
	return val ;
}

/*****************************************************************************/
//  FUNCTION NAME: rd_d4_cmd()
//
//  DESCRIPTION:
//		read cmd reg from DBMD4
//  PARAMETERS:
//    int16_t reg
//  RETURNS:
//        int16_t val the register vakue
//*****************************************************************************/
int16_t  rd_d4_cmd(int16_t reg)
{
	int16_t val = 0;
	val = read_register_spi(spi, reg);
	return val ;
}

/*****************************************************************************/
//  FUNCTION NAME: set_tdm_hw()
//
//  DESCRIPTION:
//		send command to tdm hw
//  PARAMETERS:
//    	int chip 
//	int tdm_addr
//	long cfg
//	long ph12 
//	 long frcfg)	
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
void set_tdm_hw(int chip , int tdm_addr , long cfg , long ph12 , long frcfg)
{
    if (chip == DBMD2) {
	   write_register_spi(spi1, 0x5,(tdm_addr & 0xffff));
	   delay(2);
	   write_register_spi(spi1, 0x6,tdm_addr>>16);
	   delay(2);
	   write_register_spi(spi1, 0x7,cfg & 0xffff);
	   delay(2);
	   write_register_spi(spi1, 0x8,cfg >> 16);
	   delay(2);
	   write_register_spi(spi1, 0x5,(tdm_addr & 0xffff) +4);
	   delay(2);	   
	   write_register_spi(spi1, 0x6,tdm_addr>>16);
	   delay(2);	   
	   write_register_spi(spi1, 0x7,ph12 & 0xffff);
	   delay(2);
	   write_register_spi(spi1, 0x8,ph12 >> 16);
	   delay(2);	 
	   write_register_spi(spi1, 0x5,(tdm_addr & 0xffff) +6);
	   delay(2);	   
	   write_register_spi(spi1, 0x6,tdm_addr>>16);
	   delay(2);	   
	   write_register_spi(spi1, 0x7,frcfg & 0xffff);
	   delay(2);	   
	   write_register_spi(spi1, 0x8,frcfg >> 16);
	   delay(2);	   
   } else {
	   write_register_spi(spi, 0x5,(tdm_addr & 0xffff) );
	   delay(2);	   
	   write_register_spi(spi, 0x6,tdm_addr>>16);
	   delay(2);	   
	   write_register_spi(spi, 0x7,cfg & 0xffff);
	   delay(2);	   
	   write_register_spi(spi, 0x8,cfg >> 16);
	   delay(2);
	   write_register_spi(spi, 0x5,(tdm_addr & 0xffff) +4);
	   delay(2);	   
	   write_register_spi(spi, 0x6,tdm_addr>>16);
	   delay(2);	   
	   write_register_spi(spi, 0x7,ph12 & 0xffff);
	   delay(2);
	   write_register_spi(spi, 0x8,ph12 >> 16);
	   delay(2);
	   write_register_spi(spi, 0x5,(tdm_addr & 0xffff) +6);
	   delay(2);
	   write_register_spi(spi, 0x6,tdm_addr>>16);
	   delay(2);
	   write_register_spi(spi, 0x7,frcfg & 0xffff);
	   delay(2);
	   write_register_spi(spi, 0x8,frcfg >> 16);
	   delay(2);	   
    }

}

void set_tdm_hw_mcee(int chip, int tdm_addr, long mcee)
{
	write_register_spi(spiD4, 0x5,(tdm_addr & 0xffff) + 10);
	delay(2);
	write_register_spi(spiD4, 0x6,tdm_addr>>16);
	delay(2);
	write_register_spi(spiD4, 0x7,mcee & 0xffff);
	delay(2);
	write_register_spi(spiD4, 0x8,mcee >> 16);
	delay(2);
}

/*****************************************************************************/
//  FUNCTION NAME: Init_D2()
//
//  DESCRIPTION:
//		INIT the DBMD2 
//  PARAMETERS:
//    enum interafce_type interface
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int Init_D2(enum interafce_type interface)
{
	// release D2 from reset
	D2_interface = interface;
	
	printf ("[Init_D2]: D2_interface = %d\n", D2_interface);

	if(interface == I2C_INTERFACE) 
	{
		chnl_desc = i2c;
#ifdef DBMD5
		printf ("Start uploading firmware D2 via I2C\n");
		if ( load_fileI2C(FirmWareD2_filename,chnl_desc)){
			perror ("fail uploading pre boot to D2 via I2c\n");
			exit (1);
		}
#else
		printf ("Start uploading pre-boot to D2 via I@C\n");
		if ( load_fileI2C(I2C_to_UART_filename,chnl_desc)){
			perror ("fail uploading pre boot to D2 via I2c\n");
			exit (1);
		}
		
#endif
	}
	
	else if(interface == SPI_INTERFACE) 
	{
		printf ("Start uploading pre-boot to D2 via I@C\n");
		if ( load_fileI2C(I2C_to_SPI_filename,i2c)){
			perror ("fail uploading pre boot to D2 via I2c\n");
			exit (1);
		}
		delay(300);
		printf ("Start uploading FirmWare to D2 via SPI *******\n");
		
		load_fileD2(FirmWareD2_filename, spi1);
	}
	//let the fw some time to start !!!
	delay(8);
	return 0;
}
	
/*****************************************************************************/
//  FUNCTION NAME: Init_D4()
//
//  DESCRIPTION:
//		Init the DBMD4
//  PARAMETERS:
//    enum interafce_type interface
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int Init_D4(enum interafce_type interface)
{
        int fail_cnt = 0 ;
	D4_interface = interface ;
	
	if(interface == SPI_INTERFACE) {
		chnl_desc1 = spi;
	} else if(interface == I2C_INTERFACE){
		chnl_desc1 = i2c;
	}else{ 	
		chnl_desc1 = serial;
	}

	while ( fail_cnt < 5 ) {
		// release D2 from reset
		printf ("Start uploading FirmWare to D4 using spi1\n");
		//####### Load Firmware #######
		printf ("Start uploading FirmWare to D4\n");
		if(!load_file(FirmWare_filename, chnl_desc1, 4)) {
			fail_cnt++;
			
			if ( fail_cnt == 5) {	
				printf ("FAiled uploading FirmWare to D4\n");
				exit(2);
			}
		}
		else
			break;
	}
	
	return 0;
}

/*****************************************************************************/
//  FUNCTION NAME: Run_D4()
//
//  DESCRIPTION:
//		run d4 
//  PARAMETERS:
//    
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int Run_D4()
{
	char buf_run[] = {0x5A, 0x0B};
	write (chnl_desc1, buf_run, 2); 
	delay (8);
	return 0;
}

/*****************************************************************************/
//  FUNCTION NAME: Config_D4()
//
//  DESCRIPTION:
//		config D4
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int Config_D4()
{
	printf("Config D4\n");
	
#ifdef DEBUG	
	write_register_spi(spi, HOST_INTERFACE_SUPPORT, 0x1020);	//Debug
#else
	write_register_spi(spi, HOST_INTERFACE_SUPPORT, HOST_INTERFACE_SPI);
#endif	
	write_register_spi(spi, Master_Clock_Frequency ,0x6000); //24,576Mhz
	write_register_spi(spi, UART_Baud_Rate ,0x7530); //UART is 3M for log.	
	
	write_register_spi(spi, GENERAL_CONFIGURATION_1, 0x1086);
	write_register_spi(spi, GENERAL_CONFIGURATION_2, 0x0000);
	write_register_spi(spi, PHRASE_DETECTION_GPIO_CONFIGURATION, 0x9090); /*D4 GP16 - f*/
	write_register_spi(spi, AUDIO_BUFFER_READOUT_CONFIGURATION,0x0002);

	write_register_spi(spi, DSP_CLOCK_CONFIGURATION, 0x3000);

	write_register_spi(spi,Samples_For_VT,0x0008);
	/* let the pll some time*/
	delay(80);

	write_register_spi(spi, AUDIO_BUFFER_SIZE,0x1000);

	printf("Config D4 ended\n");
	
	return 0 ;
}

/*****************************************************************************/
//  FUNCTION NAME: Run_D2()
//
//  DESCRIPTION:
//		
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int Run_D2()
{
	int ret;
	char buf[] = {0x5A, 0x0B};
	
	ret = write(spi1, buf, 2);
	if(ret != 2) {
		printf("[%s]: spi D2 write return %d\n", __func__, ret);
	}
	
	ret = read_register_spi(spi1, FW_VERSION_NUMBER);
	if(ret != 0) {
		printf("[%s]: D2 Firmware version 0x%X\n", __func__, ret & 0xffff);
	} else {
		printf("[%s]: Failed read D2 Firmware version \n", __func__);
	}
	
	return 0;
}

/*****************************************************************************/
//  FUNCTION NAME: Config_D2()
//
//  DESCRIPTION:
//		
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int Config_D2()
{
	printf("Config D2\n");
	// Init D2 system
#ifdef DEBUG	
	write_register_spi(spi1, HOST_INTERFACE_SUPPORT, 0x1070);
#else
	write_register_spi(spi1, HOST_INTERFACE_SUPPORT, HOST_INTERFACE_SPI);
#endif	
	write_register_spi(spi1, Master_Clock_Frequency, 0x6000);
	write_register_spi(spi1, UART_Baud_Rate ,0x7530); //UART is 3M for log.
	write_register_spi(spi1, GENERAL_CONFIGURATION_1, 0x0000);
	write_register_spi(spi1, GENERAL_CONFIGURATION_2, 0x0200);
	write_register_spi(spi1, DSP_CLOCK_CONFIGURATION, 0xc001);
	delay(80);
	write_register_spi(spi1, AUDIO_BUFFER_SIZE,0x20); //no need audio buffer at D2

	printf("Config D2 ended\n");
	
	return 0;
}

/*****************************************************************************/
//  FUNCTION NAME: Load_A_Model()
//
//  DESCRIPTION:
//		Load VT acoustic model to memory
//  PARAMETERS:
//    char *filename
//  RETURNS:
//        0 on sucsses else error
//*****************************************************************************/
int Load_A_Model()
{
	int value ;
	///* ####### allocate memory in the DBMD4 for acoustic models  ######## */
	write_register_spi(spi, PRIMARY_ACOUSTIC_MODEL_SIZE, ((get_file_size(Acoustic_Model_filename)+3)/4*4)/16); 	
	///* ####### Load Trigger Acoustic Model ####### */
	write_register_spi(spi, LOAD_NEW_ACOUSTIC_MODEL, LOAD_TRIGGER_ACOUSTIC_MODEL);
	printf ("Start uploading Trigger Acoustic Model to D4\n");

	load_amodel_file(Acoustic_Model_filename, spi);

	printf ("Done uploading Trigger Acoustic model\n\n");

	Run_D4();
	// verify loading acoustic model 		
	value = read_register_spi(spi, SENSORY_INITIALIZED);
	if (value != 0x01) {
		printf ("Trigger Acoustic model was not successfully uploaded %d !\n",value);
		return -1;
	} else {
		printf ("Trigger Acoustic model was successfully uploaded %d !\n",value);
	}
	
	return 0 ;
}

/*****************************************************************************/
//  FUNCTION NAME: Load_ASRP_param()
//
//  DESCRIPTION:
//		
//  PARAMETERS:
//    int param
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int Load_ASRP_param(int param)
{
	int value;
	long err = 1;
	///* ####### allocate memory in the DBMD2 for acoustic models  ######## */
	write_register_spi(spi1, AUDIO_BUFFER_SIZE, 0x20); //no need audio buffer at D2
	delay(10);
	///* ####### Load Trigger Acoustic Model ####### */
	write_register_spi(spi1, LOAD_NEW_ACOUSTIC_MODEL, LOAD_COMMAND_ASRP_PARAM);
	printf("Start uploading ASRP param D2\n");

	if(!load_file(Asrp_Param_filename[param], spi1, 0)) {
		perror ("fail uploading ASRP MODEL\n");
		err = -1;
		goto out;
	}

	printf ("Done uploading Trigger Acoustic model\n\n");
	delay(10);

	// at the moment the param file containt the run command
	// verify loading acoustic model 		
	value = ASRP_read_register(0x100);
	printf ("ASRP_read reg 0x100=  0x%X \n",value);
	
	value = ASRP_read_register(0x101);
	printf ("ASRP_read reg 0x101 =  0x%X \n",value);

#if 0
	delay(10);
	write_register_spi(spi1, 0x18, 0xa);
#endif	
	
out:
	return err;
}

/*****************************************************************************/
//  FUNCTION NAME: change_clock_src()
//
//  DESCRIPTION:
//		sets the DBMD2 & DBMD2 clocks to work from TDM0 clocks
//       according to the tdm clock and number of bits.
//       We cahnge the clock src and works from the tdm 0 clock.
//       NOTE : the tdm 0 clock must be present before calling this function(music clock)
//  PARAMETERS:
//    int freq
//	  int num_of_bits
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int change_clock_src(int freq, int bit_depth)
{
#if 1
	int d2_clock = 294912;
	int d4_clock = 73728;
	int m_clck = 24576;
	int d2_pll_factor = 0;
	int d4_pll_factor = 0;

	int sclck = freq * bit_depth * 2;

	if (sclck != 0){
		d2_pll_factor = d2_clock / sclck;
		d4_pll_factor = d4_clock / sclck;

		send_d2_cmd(TDM0_SCLK_Clock_Frequency, sclck);
		send_d2_cmd(DSP_Clock_Configuration_Extension, (0xd000+d2_pll_factor));
		send_d4_cmd(TDM0_SCLK_Clock_Frequency, sclck);
		send_d4_cmd(DSP_Clock_Configuration_Extension, (0xd000+d4_pll_factor));
	}else{
		d2_pll_factor = d2_clock / m_clck;
		d4_pll_factor = d4_clock / m_clck;

		send_d2_cmd(TDM0_SCLK_Clock_Frequency, sclck);
		send_d2_cmd(DSP_Clock_Configuration_Extension, (0x9000+d2_pll_factor));
		send_d4_cmd(TDM0_SCLK_Clock_Frequency, sclck);
		send_d4_cmd(DSP_Clock_Configuration_Extension, (0x9000+d4_pll_factor));
	}

#else 	
	int pll_factor;

	if ( freq == 0 ) {
		pll_factor = 0x0000;
		write_register_spi(spi1, 0x1d,0x0);
		write_register_spi(spi, 0x1d ,0x0);
		write_register_spi(spi1, 0x1e ,0x900b | pll_factor);
		write_register_spi(spi, 0x1e ,0x9003);
	} else if ( freq == 48 ) {
		pll_factor = (num_of_bits == 32 ? 0x60 : 0xc0);
		send_d2_cmd(0x1d, freq * num_of_bits * 2  );
		write_register_spi(spi1, 0x1e ,0xd000 | pll_factor);
		send_d4_cmd(0x1d, freq * num_of_bits * 2  );
		write_register_spi(spi, 0x1e ,(num_of_bits == 32 ? 0xd019 : 0xd032));
	} else if ( freq == 16 ) {
		send_d2_cmd(0x1d, freq * num_of_bits * 2  );

		if ( num_of_bits == 32)
			pll_factor = 0X120;
	   	else
			pll_factor = 0X240;
	   	
		send_d4_cmd(0x1d, freq * num_of_bits * 2  );
		write_register_spi(spi1, 0x1e ,0xd000 | pll_factor);
		write_register_spi(spi, 0x1e ,(num_of_bits == 32 ? 0xd048 : 0xd090));
	}
#endif	
	
	delay(480);
	return 0;
}

/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
//  USE CASES


/*****************************************************************************/
//  FUNCTION NAME: use_case_0()
//
//  DESCRIPTION:
//		set use case 1 - 26MHZ clock IDLE
//            Close all open intrfaces and modules
//    		
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/


int use_case_0()
{
//    int value ;
   send_d2_cmd(TDM_Activation_Control,0x0000);
   delay(20);
   
    
   send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0000);
   send_d2_cmd(TDM_Tx_Configuration,0x0000);
   delay(1);
//    change_clock_src ( 0, 0);
   send_d2_cmd(TDM_Rx_Configuration,0x0000);
  
   write_registerI2C(AUDIO_PROCESSING_CONFIGURATION,0x0);
   send_d4_cmd(TDM_Activation_Control,0x0000);
   
   send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0000);
    
   send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION,0x0);
   send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION,0x0);
   send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION,0x0);

 
   // set D4 tdm 1 transmit master 
   send_d4_cmd(TDM_Activation_Control,0x0000);
   send_d4_cmd(TDM_Tx_Configuration,0x0);
   send_d4_cmd(TDM_Rx_Configuration,0x0);

   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Tx_Configuration,0x0000);
   send_d4_cmd(TDM_Rx_Configuration,0x0);

 
   send_d2_cmd(TDM_Activation_Control,0x0001);
   send_d2_cmd(TDM_Tx_Configuration,0x0);
   send_d2_cmd(TDM_Rx_Configuration,0x0);

   send_d2_cmd(TDM_Activation_Control,0x0003);
   send_d2_cmd(TDM_Tx_Configuration,0x0);
   send_d2_cmd(TDM_Rx_Configuration,0x0);

   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Tx_Configuration,0x0000);
   send_d2_cmd(TDM_Rx_Configuration,0x0);

   send_d2_cmd(GENERAL_CONFIGURATION_2,0x0000);
   send_d4_cmd(GENERAL_CONFIGURATION_2,0x0000);
 
   change_clock_src ( 0, 0);

   return 0;
 }



/*****************************************************************************/
//  FUNCTION NAME: use_case_1()
//
//  DESCRIPTION:
//		set use case 1 - 26MHZ clock 16Khz mics
//            D4 MIC -16 Khz - > BEAM forming  -> D4 TDM 1 -> 
//			  -> D2 TDM 3 > ASRP -> D2 TDM3 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int use_case_1()
{
	uint16_t value;
	printf("[%s]: Start.\n", __func__);
	Load_ASRP_param(0);

	value = read_register_spi(spi, OPERATION_MODE);
	printf ("[%s]: OPERATION_MODE reg =  0x%X \n",__func__, value);
 
	send_d4_cmd(GENERAL_CONFIGURATION_1, 0x1086);  // beam forming
	send_d4_cmd(GENERAL_CONFIGURATION_2, 0x0000);  // mic 16Khz
	send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION, 0x0040);    // beam forming and enable vt
	send_d4_cmd(Audio_Streaming_Source_Selection, 0xfff4);  // beam forming and enable vt
    
	// mic configuration
	send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION, 0xa075);
	send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION, 0x5062);
	send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION, 0x5074);

	//D4 set tdm 1 transmit master 
	send_d4_cmd(TDM_Activation_Control, 0x0001);
	send_d4_cmd(TDM_Tx_Configuration, 0xa203); 
	set_tdm_hw(DBMD4 , D4_TDM_1_TX_ADDR, 0x00800010, 0x00073007, 0x101f003f);

	//D4 set tdm 1 receive 
	send_d4_cmd(TDM_Activation_Control, 0x0001);
	send_d4_cmd(TDM_Rx_Configuration, 0x0013);
	set_tdm_hw(DBMD4, D4_TDM_1_RX_ADDR, 0x00800015, 0x00070007, 0x101f003F);
   
	// set audio routing
	send_d2_cmd(Audio_Processing_Routing, 0x0210);
	send_d2_cmd(Audio_Processing_Routing, 0x1eee);
	send_d2_cmd(Audio_Processing_Routing, 0x2e03);
	send_d2_cmd(Audio_Processing_Routing, 0x3eee);
 
	send_d2_cmd(GENERAL_CONFIGURATION_2, 0x0000);
	send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION, 0x0003);
	delay(100); /* configure AUDIO_PROCESSING_CONFIGURATION needs time */
   
	//D2 set tdm 2 recieve  master 
	send_d2_cmd(TDM_Activation_Control, 0x0002);
	send_d2_cmd(TDM_Rx_Configuration, 0xa203);
	set_tdm_hw(DBMD2, D2_TDM_2_RX_ADDR, 0x00800015, 0x00000027, 0x101F003F);

	//D2 set tdm 2 Transmit
	send_d2_cmd(TDM_Activation_Control, 0x0002);
	send_d2_cmd(TDM_Tx_Configuration, 0x000F);
	set_tdm_hw(DBMD2, D2_TDM_2_TX_ADDR ,0x00800015, 0x00072007, 0x100f003f);
	
	write_register_spi(spi1, TDM_Activation_Control,0x3008);
	write_register_spi(spi, TDM_Activation_Control,0x0C04);	

	printf("[%s]: Finish.\n", __func__);
	return 0;
}

int use_case_1_exit()
{
	printf("[%s]: Start.\n", __func__);
	write_register_spi(spiD4, OPERATION_MODE, IDLE_MODE);
	write_register_spi(spiD4, AUDIO_PROCESSING_CONFIGURATION, 0x0000);
	write_register_spi(spiD4, SECOND_MICROPHONE_CONFIGURATION,0x0000);
	write_register_spi(spiD4, FIRST_MICROPHONE_CONFIGURATION,0x0000);
	write_register_spi(spiD4, GENERAL_CONFIGURATION_2, 0x0000);
	printf ("[%s]: Finish!\n", __func__);
	return 0;
}


/*****************************************************************************/
//  FUNCTION NAME: use_case_1_a()
//
//  DESCRIPTION:
//		set use case 1 26Mhz clcok 48Khz mics
//            D4 MIC -48 Khz - > BEAM forming  -> D4 TDM 1 (dec 16Khz ) -> 
//			  -> D2 TDM 3 > ASRP -> D2 TDM3 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int use_case_1_a()
{
    Load_ASRP_param(0);
 
    send_d4_cmd(GENERAL_CONFIGURATION_1,0x1080);  // beam forming
    send_d4_cmd(GENERAL_CONFIGURATION_2,0x0200); // set mic freq 48khz
   
    send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION,0x43);  // beam forming and enable vt
    send_d4_cmd(Audio_Streaming_Source_Selection,0xfff4);  // beam forming and enable vt

     
    // mic configuration
    send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION,0x4076);
    send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION,0xa061);
    send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION,0xa073);

   // set tdm 1 transmit master 

    send_d4_cmd(TDM_Activation_Control,0x0001);
    send_d4_cmd(TDM_Tx_Configuration,0xa243);
    set_tdm_hw(DBMD4 , D4_TDM_1_TX_ADDR ,0x00800010 , 0x00073007, 0x101f0040) ;

   // set tdm 1 receive 

   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Rx_Configuration,0x0013);
   set_tdm_hw(DBMD4 , D4_TDM_1_RX_ADDR ,0x00800015 , 0x00070007, 0x101f0040) ;
 
   // set audio routing

   send_d2_cmd(Audio_Processing_Routing,0x0103);
   delay(1);
   send_d2_cmd(Audio_Processing_Routing,0x1982);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x2aa3);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x3aaa);
   delay(8);

   send_d2_cmd(GENERAL_CONFIGURATION_2,0x0000);
   send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x3);

   // set tdm 3 recieve  master 

   send_d2_cmd(TDM_Activation_Control,0x0003);
   send_d2_cmd(TDM_Rx_Configuration,0xa203);
   set_tdm_hw(DBMD2, D2_TDM_3_RX_ADDR ,0x00800015 , 0x00000027, 0x101f0040) ;

      // set tdm 3 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0003);
   send_d2_cmd(TDM_Tx_Configuration,0x000f);
   set_tdm_hw(DBMD2, D2_TDM_3_TX_ADDR ,0x00800015 , 0x00072007, 0x101f0040) ;

   return 0;
}



/*****************************************************************************/
//  FUNCTION NAME: use_case_2()
//
//  DESCRIPTION:
//		set use case 2 - Play music only 48Khz 16 bits use extrnal clock from TDM0
//			  -> D2 TDM 0 > CROSSOVER  -> D2 TDM1 / D2 TDM2 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int use_case_2()
{
	printf("[%s]: Start.\n", __func__);
	// start play music here we move to tdm clock 0
	//send_d4_cmd(TDM_Activation_Control,0x0000);
	//send_d4_cmd(TDM_Tx_Configuration,0x0);

	//send_d2_cmd(TDM_Activation_Control,0x0000); // enable tdm 0 input
	//send_d2_cmd(TDM_Rx_Configuration,0x01);

	// load asrp params
	Load_ASRP_param(1);
	// change clock to TDM 0 must be active !!!
	//change_clock_src(48 , 32);

	send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0000);
	send_d2_cmd(GENERAL_CONFIGURATION_2,0x0200); // 48khz
	send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0003); 
	
	//configure audio routing
	send_d2_cmd(Audio_Processing_Routing,0x0210);
	send_d2_cmd(Audio_Processing_Routing,0x154e);
	send_d2_cmd(Audio_Processing_Routing,0x2e03);
	send_d2_cmd(Audio_Processing_Routing,0x3eee);

	//D2 set tdm 0 recieve  slave
	send_d2_cmd(TDM_Activation_Control,0x0000);
	send_d2_cmd(TDM_Rx_Configuration,0x9a13);   
	set_tdm_hw(DBMD2, D2_TDM_0_RX_ADDR ,0x0080001d , 0x00072027, 0x100f001f) ;

	//D2 set tdm 0 transmit  slave
	send_d2_cmd(TDM_Activation_Control,0x0000);
	send_d2_cmd(TDM_Tx_Configuration,0x8bc3);   
	set_tdm_hw(DBMD2, D2_TDM_0_TX_ADDR ,0x0080001d , 0x00070027, 0x100f001f) ;

	//D2 set tdm 3 Transmit
	send_d2_cmd(TDM_Activation_Control,0x0003);
	send_d2_cmd(TDM_Tx_Configuration,0x9a13); 
	set_tdm_hw(DBMD2, D2_TDM_3_TX_ADDR ,0x0080001d, 0x00070027, 0x100f001f) ;
  
	//D4
	send_d4_cmd(GENERAL_CONFIGURATION_2,0x0000);
	send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0443);  
	delay(100); /* configure AUDIO_PROCESSING_CONFIGURATION needs time */
	
	send_d4_cmd(Audio_Streaming_Source_Selection,0xfff4);

	//D4 mic configuration
	send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION,0xa055);  
	send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION,0x5042);
	send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION,0x5054);
 
	// set tdm 1 transmit master 
	send_d4_cmd(TDM_Activation_Control,0x0001);
	send_d4_cmd(TDM_Tx_Configuration,0xa203);      
	set_tdm_hw(DBMD4 , D4_TDM_1_TX_ADDR ,0x00800010, 0x00073007, 0x101f003f) ;

	// set tdm 1 receive 
	send_d4_cmd(TDM_Activation_Control,0x0001);
	send_d4_cmd(TDM_Rx_Configuration,0x0013);
	set_tdm_hw(DBMD4 , D4_TDM_1_RX_ADDR ,0x00800015 , 0x00070007, 0x101f003f) ;

	// set tdm 2 recieve  master 
	send_d2_cmd(TDM_Activation_Control,0x0002);
	send_d2_cmd(TDM_Rx_Configuration,0xa203);
	set_tdm_hw(DBMD2 , D2_TDM_2_RX_ADDR ,0x00800015 , 0x00000027, 0x101f003f) ;

	// set tdm 2 Transmit
	send_d2_cmd(TDM_Activation_Control,0x0002);   
	send_d2_cmd(TDM_Tx_Configuration,0x000f);
	set_tdm_hw(DBMD2 , D2_TDM_2_TX_ADDR ,0x00800015 , 0x00072007, 0x101f003f) ;

	user_space_playback_16b("Music_playlist_48K/Silence_1_Hour.wav");
	delay(500);

	change_clock_src(48,16);
	write_register_spi(spi, TDM_Activation_Control,0x0C04);		
	write_register_spi(spi1, TDM_Activation_Control,0xb308);	

	printf("[%s]: Finish.\n", __func__);
	return 0;
}

/*****************************************************************************/
//  FUNCTION NAME: use_case_2()
//
//  DESCRIPTION:
//		set use case 2 - Play music only 48Khz 16 bits use extrnal clock from TDM0
//			  -> D2 TDM 0 > CROSSOVER  -> D2 TDM1 / D2 TDM2 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int use_case_2_16()
{
  
  // start play music here we move to tdm clock o
  
   send_d2_cmd(GENERAL_CONFIGURATION_1,0x0);
   send_d2_cmd(GENERAL_CONFIGURATION_2,0x0000); // 48khz
   send_d2_cmd(0x70,0x2);      // set parameter for long on cross_over
   send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x8);
   delay(180);
  
   // config connection points
   send_d2_cmd(Audio_Processing_Routing,0x4454);
    delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x5765);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x6876);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x7ee9);
   delay(8);
   // set tdm 0 recieve  slave
   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Rx_Configuration,0x9213);
   set_tdm_hw(DBMD2, D2_TDM_0_RX_ADDR ,0x0080001f , 0x00072027, 0x100f001f) ;
    
   // set D2 tdm 1 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0001);
   send_d2_cmd(TDM_Tx_Configuration,0x921b);
   set_tdm_hw(DBMD2, D2_TDM_1_TX_ADDR ,0x00800017 , 0x00070027, 0x100f001f) ;

#if 0
   // set D2 tdm 2 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0002);
   send_d2_cmd(TDM_Tx_Configuration,0x9a13);
   set_tdm_hw(DBMD2, D2_TDM_2_TX_ADDR ,0x0080001d , 0x00072027, 0x100f001f) ;
#endif
    // start play music here we move to tdm clock 0
   send_d4_cmd(TDM_Activation_Control,0x0000);
   send_d4_cmd(TDM_Tx_Configuration,0x1);

   send_d4_cmd(TDM_Activation_Control,0x0000);
   send_d4_cmd(TDM_Tx_Configuration,0x0000);
  return 0;
}

/*****************************************************************************/
//  FUNCTION NAME: use_case_2_long()
//
//  DESCRIPTION:
//		set use case 2 - Play music only 48Khz 32 bits use extrnal clock from TDM0
//			  -> D2 TDM 0 > CROSSOVER  -> D2 TDM1 / D2 TDM2 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int use_case_2_long()
{
	printf("[%s]: Start.\n", __func__);

	// start play music here we move to tdm clock 0
	send_d2_cmd(GENERAL_CONFIGURATION_1,0x0);
	send_d2_cmd(0x70,0x3);      // set parameter for long on cross_over
	send_d2_cmd(GENERAL_CONFIGURATION_2,0x0200); // 48khz
	send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x8);
	delay(180);

	// configur ecross over connection points
	// config connection points
	send_d2_cmd(Audio_Processing_Routing,0x4654);
	delay(8);
	send_d2_cmd(Audio_Processing_Routing,0x5547);
	delay(8);
	send_d2_cmd(Audio_Processing_Routing,0x68ee);
	delay(8);
	send_d2_cmd(Audio_Processing_Routing,0x7ee9);
	delay(8);

	// configure TDM 0 
	send_d2_cmd(TDM_Activation_Control,0x0000);
	send_d2_cmd(TDM_Rx_Configuration,0xda13);
	set_tdm_hw(DBMD2, D2_TDM_0_RX_ADDR ,0x0080001d , 0x00072027, 0x101f003f) ;

	// set D2 tdm 1 Transmit
	send_d2_cmd(TDM_Activation_Control,0x0003);
	send_d2_cmd(TDM_Tx_Configuration,0xda1b);
	set_tdm_hw(DBMD2, D2_TDM_3_TX_ADDR ,0x00800015 , 0x00002027, 0x101f003f) ;
	
	printf("[%s]: Stop.\n", __func__);
	return 0;
}

int use_case_2_exit()
{
	printf("[%s]: Start.\n", __func__);
	wakeup_low(); /* wake */
	delay(100);
	write_register_spi(spiD4, FIRST_MICROPHONE_CONFIGURATION, MIC24);
	write_register_spi(spiD4, SECOND_MICROPHONE_CONFIGURATION, MIC25);
	write_register_spi(spiD4, THIRD_MICROPHONE_CONFIGURATION, MIC26);
	printf ("[%s]: Finish!\n", __func__);
	return 0;
}



/*****************************************************************************/
//  FUNCTION NAME: use_case_3()
//
//  DESCRIPTION:
//		set use case 3 - Play music only 48Khz 32 bits use extrnal clock from TDM0
//				And activate ASRP NS and AEC
//				After DEtection UART STREAMING
//              D4 mics 16bits
//
//			  -> D2 TDM 0 > CROSSOVER  -> D2 TDM1 / D2 TDM2 
//                 D2 CROSSOVER  -> ASRP REFFERNCE
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//            D4 MIC -16 Khz - > BEAM forming  -> D4 TDM 1 -> 
//			  -> D2 TDM 3 > ASRP -> D2 TDM3 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS+AEC 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/
int use_case_3()
{
	printf("[%s]: Start.\n", __func__);
	// start play music here we move to tdm clock 0
	//send_d4_cmd(TDM_Activation_Control,0x0000);
	//send_d4_cmd(TDM_Tx_Configuration,0x0);

	//send_d2_cmd(TDM_Activation_Control,0x0000); // enable tdm 0 input
	//send_d2_cmd(TDM_Rx_Configuration,0x01);

	// load asrp params
	Load_ASRP_param(1);
	// change clock to TDM 0 must be active !!!
	//change_clock_src(48 , 32);

	send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0000);
	send_d2_cmd(GENERAL_CONFIGURATION_2,0x0200); // 48khz
	send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0003); 
	delay(100); /* configure AUDIO_PROCESSING_CONFIGURATION needs time */
	
	//configure audio routing
	send_d2_cmd(Audio_Processing_Routing,0x0210);
	send_d2_cmd(Audio_Processing_Routing,0x154e);
	send_d2_cmd(Audio_Processing_Routing,0x2e03);
	send_d2_cmd(Audio_Processing_Routing,0x3eee);

	//D2 set tdm 0 recieve  slave
	send_d2_cmd(TDM_Activation_Control,0x0000);
	send_d2_cmd(TDM_Rx_Configuration,0x9a13);   
	set_tdm_hw(DBMD2, D2_TDM_0_RX_ADDR ,0x0080001d , 0x00072027, 0x100f001f) ;

	//D2 set tdm 0 transmit  slave
	send_d2_cmd(TDM_Activation_Control,0x0000);
	send_d2_cmd(TDM_Tx_Configuration,0x8bc3);   
	set_tdm_hw(DBMD2, D2_TDM_0_TX_ADDR ,0x0080001d , 0x00070027, 0x100f001f) ;

	//D2 set tdm 3 Transmit
	send_d2_cmd(TDM_Activation_Control,0x0003);
	send_d2_cmd(TDM_Tx_Configuration,0x9a13); 
	set_tdm_hw(DBMD2, D2_TDM_3_TX_ADDR ,0x0080001d, 0x00070027, 0x100f001f) ;
  
	//D4
	send_d4_cmd(GENERAL_CONFIGURATION_2,0x0000);
	send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0443);  
	send_d4_cmd(Audio_Streaming_Source_Selection,0xfff4);

	//D4 mic configuration
	send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION,0xa455);  
	send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION,0x5442);
	send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION,0x5454);
 
	// set tdm 1 transmit master 
	send_d4_cmd(TDM_Activation_Control,0x0001);
	send_d4_cmd(TDM_Tx_Configuration,0xa203);      
	set_tdm_hw(DBMD4 , D4_TDM_1_TX_ADDR ,0x00800010, 0x00073007, 0x101f003f) ;

	// set tdm 1 receive 
	send_d4_cmd(TDM_Activation_Control,0x0001);
	send_d4_cmd(TDM_Rx_Configuration,0x0013);
	set_tdm_hw(DBMD4 , D4_TDM_1_RX_ADDR ,0x00800015 , 0x00070007, 0x101f003f) ;

	// set tdm 2 recieve  master 
	send_d2_cmd(TDM_Activation_Control,0x0002);
	send_d2_cmd(TDM_Rx_Configuration,0xa203);
	set_tdm_hw(DBMD2 , D2_TDM_2_RX_ADDR ,0x00800015 , 0x00000027, 0x101f003f) ;

	// set tdm 2 Transmit
	send_d2_cmd(TDM_Activation_Control,0x0002);   
	send_d2_cmd(TDM_Tx_Configuration,0x000f);
	set_tdm_hw(DBMD2 , D2_TDM_2_TX_ADDR ,0x00800015 , 0x00072007, 0x101f003f) ;

	user_space_playback_16b("Music_playlist_48K/Dont_Stop_Me_Now.wav");
	delay(500);

	change_clock_src(48,16);
	write_register_spi(spi, TDM_Activation_Control,0x0C04);	
	write_register_spi(spi1, TDM_Activation_Control,0xb308);

	printf("[%s]: Finish.\n", __func__);
	return 0;
}


/*****************************************************************************/
//  FUNCTION NAME: use_case_3_long()
//
//  DESCRIPTION:
//		set use case 3 - Play music only 48Khz 32 bits use extrnal clock from TDM0
//				And activate ASRP NS and AEC
//				After DEtection UART STREAMING
//              D4 mics 48khz
//
//			  -> D2 TDM 0 > CROSSOVER  -> D2 TDM1 / D2 TDM2 
//                 D2 CROSSOVER  -> ASRP REFFERNCE
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//            D4 MIC -48 Khz - > BEAM forming  -> D4 TDM 1(resample 16Khz) -> 
//			  -> D2 TDM 3 > ASRP -> D2 TDM3 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS+AEC 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int use_case_3_long()
{
   // start play music here we move to tdm clock o
   send_d4_cmd(TDM_Activation_Control,0x0000);
   send_d4_cmd(TDM_Tx_Configuration,0x1);

   send_d4_cmd(TDM_Activation_Control,0x0000);
   send_d4_cmd(TDM_Tx_Configuration,0x0000);

   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Rx_Configuration,0x01);

 
   // load asrp paramters AEC+NS
   Load_ASRP_param(1);

   //play_music(); //on d2
   // clocks setting
   // tdm to input mode (io setting for clock input from tdm 0)
   change_clock_src ( 48 , 32 );

  
   // Audio routing
   send_d2_cmd(Audio_Processing_Routing,0x0103);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x1982);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x2ee3);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x3eee);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x4454);
    delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x5765);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x6e98);
   delay(8);

   send_d2_cmd(GENERAL_CONFIGURATION_2,0x0200); // 48khz
   send_d4_cmd(GENERAL_CONFIGURATION_2,0x0200); // 48khz

   send_d2_cmd(0x70,0x3);      // set parameter for 16bits on cross_over

   send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0b);      // enable asrp and cross over	
   delay(180);
 

   // set tdm 0 recieve  slave
   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Rx_Configuration,0xda13);   // connection point 4 on dbmd2   
   set_tdm_hw(DBMD2, D2_TDM_0_RX_ADDR ,0x0080001f , 0x00002027, 0x101f003f) ;
      
   // set D2 tdm 1 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0001);
   send_d2_cmd(TDM_Tx_Configuration,0xda1b);  // connection point 6 on dbmd2 
   set_tdm_hw(DBMD2, D2_TDM_1_TX_ADDR ,0x00800017 , 0x00072027, 0x101f003f) ;
#if 0
   // set D2 tdm 2 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0001);
   send_d2_cmd(TDM_Tx_Configuration,0xda13);  // connection point 4 on dbmd2
   set_tdm_hw(DBMD2, D2_TDM_1_TX_ADDR ,0x0080001d , 0x00072027, 0x101f003f) ;
#endif  
	// D4 configuration
    send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION,0x43);  // beam forming and enable vt
    send_d4_cmd(Audio_Streaming_Source_Selection,0xfff4);  // beam forming and enable vt
    
    // mic configuration
    send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION,0x4076);     // connection point 0 on dbmd4
    send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION,0xa061);	  // connection point 1 on dbmd4
    send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION,0xa073);	  // connection point 3 on dbmd4
 
  
   // set tdm 1 transmit master 
   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Tx_Configuration,0xa243);      // connection point 0 ,1,2,3 on dbmd4 to TDM 1
   set_tdm_hw(DBMD4 , D4_TDM_1_TX_ADDR ,0x0080001a , 0x00073007, 0x101f003f) ;



   // set tdm 1 receive 

   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Rx_Configuration,0x0013);      // connection point 4 on dbmd4 to TDM 1
   set_tdm_hw(DBMD4 , D4_TDM_1_RX_ADDR ,0x00800015 , 0x00070007, 0x101f003f) ;

   // set tdm 3 recieve  master 
   send_d2_cmd(TDM_Activation_Control,0x0002);    // connection point 0 on dbmd2 to TDM 3
   send_d2_cmd(TDM_Rx_Configuration,0xa203);
   set_tdm_hw(DBMD2 , D2_TDM_2_RX_ADDR ,0x00800015 , 0x00000027, 0x101f003f) ;
 
   // set tdm 3 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0002);    // connection point 3 on dbmd2 to TDM 3
   send_d2_cmd(TDM_Tx_Configuration,0x000f);
   set_tdm_hw(DBMD2 , D2_TDM_2_TX_ADDR ,0x00800015 , 0x00072007, 0x101f003f) ;


   return 0;
}


/*****************************************************************************/
//  FUNCTION NAME: use_case_4()
//
//  DESCRIPTION: (conversion )
//		set use case 4 - Play music only 16kh 16 bits use extrnal clock from TDM0
//				And activate ASRP NS and AEC
//				
//               D4 mics 16bits
//
//			  -> D2 TDM 0 > CROSSOVER  -> D2 TDM1 / D2 TDM2 
//                 D2 CROSSOVER  -> ASRP REFFERNCE
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//            D4 MIC -16 Khz - > BEAM forming  -> D4 TDM 1 -> 
//			  -> D2 TDM 3 > ASRP -> D2 TDM3 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS+AEC 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int use_case_4()
{
   // start play music here we move to tdm clock 0
   send_d4_cmd(TDM_Activation_Control,0x0000);
   send_d4_cmd(TDM_Rx_Configuration,0x0);

//   send_d4_cmd(TDM_Activation_Control,0x0001);
//   send_d4_cmd(TDM_Tx_Configuration,0x0000);

   send_d2_cmd(TDM_Activation_Control,0x0000); // enable tdm 0 input
   send_d2_cmd(TDM_Rx_Configuration,0x01);

 // load asrp params
 //  write_registerI2C(0x18,0x0a); // display memory allocation
   Load_ASRP_param(2);

   // change clock to TDM 0 must be active !!!
   //play_music(); //on d2
//  write_registerI2C(0x18,0x0a);  // display memory allocation
   change_clock_src ( 16 , 16 );

  
   //  configure audio routing 
 
   // Audio routing
   send_d2_cmd(Audio_Processing_Routing,0x0210);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x1983);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x2ee3);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x3eee);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x4654);
    delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x5547);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x67ee); 
   delay(8);
  send_d2_cmd(Audio_Processing_Routing,0x7ee9); 
   delay(8);

   send_d2_cmd(GENERAL_CONFIGURATION_2,0x0000); // 16khz
   send_d4_cmd(GENERAL_CONFIGURATION_2,0x0000); // 16khz mics

    // cross over module configuration
   send_d2_cmd(0x70,0x2);      // set parameter for 16 bits 16Khz on cross_over
  
   send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0b);      // enable asrp and cross over	
   delay(180);
 

   // set tdm 0 recieve  slave
   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Rx_Configuration,0x9213);   // connection point 4 on dbmd2   
   set_tdm_hw(DBMD2, D2_TDM_0_RX_ADDR ,0x0080001f , 0x00002027, 0x100f001f) ;
 
   // set D2 tdm 0 Transmit
 
   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Tx_Configuration,0x000f);  // connection point 3 on dbmd2 
   set_tdm_hw(DBMD2, D2_TDM_0_TX_ADDR ,0x0080001f , 0x00072027, 0x100f001f) ;

#if 1
    // set D2 tdm 1 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0001);
   send_d2_cmd(TDM_Tx_Configuration,0x000f);  // connection point 6 on dbmd2 
   set_tdm_hw(DBMD2, D2_TDM_1_TX_ADDR ,0x00800017 , 0x00072007, 0x100f001f) ;
#else
  // set D2 tdm 1 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0003);
   send_d2_cmd(TDM_Tx_Configuration,0x920b);
   set_tdm_hw(DBMD2, D2_TDM_3_TX_ADDR ,0x00800017 , 0x00072027, 0x100f001f) ;
#endif  
 
#if 0
   // set D2 tdm 2 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0002);
   send_d2_cmd(TDM_Tx_Configuration,0x9213);  // connection point 4 on dbmd2
   set_tdm_hw(DBMD2, D2_TDM_2_TX_ADDR ,0x0080001d , 0x00072027, 0x100f001f) ;
#endif
   send_d4_cmd(0x70,0x2);      // set parameter for 16 bits 16Khz on cross_over
 
   send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION,0x03);  // beam forming and enable vt
   send_d4_cmd(Audio_Streaming_Source_Selection,0xffff);  // beam forming and enable vt
    
    // mic configuration
   send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION,0x4076);     // connection point 0 on dbmd4
   send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION,0xa061);	  // connection point 1 on dbmd4
   send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION,0xa073);	  // connection point 3 on dbmd4
 
    // set tdm 1 transmit master 
   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Tx_Configuration,0xa203);      // connection point 0 ,1,2,3 on dbmd4 to TDM 1
   set_tdm_hw(DBMD4 , D4_TDM_1_TX_ADDR ,0x0080001a , 0x00073007, 0x101f003f) ;


   // set tdm 1 receive 

   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Rx_Configuration,0x0013);      // connection point 4 on dbmd4 to TDM 1
   set_tdm_hw(DBMD4 , D4_TDM_1_RX_ADDR ,0x00800015 , 0x00070007, 0x101f003f) ;

 
   // set tdm 3 recieve  master 
   send_d2_cmd(TDM_Activation_Control,0x0002);    // connection point 0 on dbmd2 to TDM 3
   send_d2_cmd(TDM_Rx_Configuration,0xa203);
   set_tdm_hw(DBMD2 , D2_TDM_2_RX_ADDR ,0x00800015 , 0x00000027, 0x101f003f) ;

      // set tdm 3 Transmit
  
   send_d2_cmd(TDM_Activation_Control,0x0002);    // connection point 3 on dbmd2 to TDM 3
   send_d2_cmd(TDM_Tx_Configuration,0x000f);
   set_tdm_hw(DBMD2 , D2_TDM_2_TX_ADDR ,0x00800015 , 0x00072007, 0x101f003f) ;

   return 0;
}



/*****************************************************************************/
//  FUNCTION NAME: use_case_4()
//
//  DESCRIPTION: (conversion )
//		set use case 4 - Play music only 16kh 16 bits use extrnal clock from TDM0
//				And activate ASRP NS and AEC
//				
//               D4 mics 16bits
//
//			  -> D2 TDM 0 > CROSSOVER  -> D2 TDM1 / D2 TDM2 
//                 D2 CROSSOVER  -> ASRP REFFERNCE
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//            D4 MIC -16 Khz - > BEAM forming  -> D4 TDM 1 -> 
//			  -> D2 TDM 3 > ASRP -> D2 TDM3 - >
//			  ->  D4 TDM 1 -> VT -> UART STREAMING
//  PARAMETERS:
//    		ASRP use NS+AEC 
//  RETURNS:
//        0 on sucsses elese error
//*****************************************************************************/

int use_case_4_a()
{
   // start play music here we move to tdm clock 0
   send_d4_cmd(TDM_Activation_Control,0x0000);
   send_d4_cmd(TDM_Rx_Configuration,0x1);

   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Tx_Configuration,0x0000);

   send_d2_cmd(TDM_Activation_Control,0x0000); // enable tdm 0 input
   send_d2_cmd(TDM_Rx_Configuration,0x01);

   // load asrp params
  // Load_ASRP_param(2);
   // change clock to TDM 0 must be active !!!
   //play_music(); //on d2
   change_clock_src ( 16 , 16 );

  
   //  configure audio routing 
 
   // Audio routing
   send_d2_cmd(Audio_Processing_Routing,0x0103);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x1982);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x2ee3);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x3eee);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x4454);
    delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x5765);
   delay(8);
   send_d2_cmd(Audio_Processing_Routing,0x6e98);
   delay(8);

   send_d2_cmd(GENERAL_CONFIGURATION_2,0x0000); // 16khz
   send_d4_cmd(GENERAL_CONFIGURATION_2,0x0000); // 16khz mics

   send_d2_cmd(AUDIO_PROCESSING_CONFIGURATION,0x0b);      // enable asrp and cross over	
   delay(180);
 
   // cross over module configuration
   send_d2_cmd(0x70,0x3);      // set parameter for 16bits on cross_over

   // set tdm 0 recieve  slave
   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Rx_Configuration,0x9213);   // connection point 4 on dbmd2   
   set_tdm_hw(DBMD2, D2_TDM_0_RX_ADDR ,0x0080001f , 0x00002027, 0x100f001f) ;
 
   // set D2 tdm 0 Transmit
 
   send_d2_cmd(TDM_Activation_Control,0x0000);
   send_d2_cmd(TDM_Tx_Configuration,0x000f);  // connection point 3 on dbmd2 
   set_tdm_hw(DBMD2, D2_TDM_0_TX_ADDR ,0x0080001d , 0x00072027, 0x100f001f) ;

    // set D2 tdm 1 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0003);
   send_d2_cmd(TDM_Tx_Configuration,0x000f);  // connection point 6 on dbmd2 
   set_tdm_hw(DBMD2, D2_TDM_3_TX_ADDR ,0x0080001d , 0x00072027, 0x100f001f) ;
  
 
#if 0
   // set D2 tdm 2 Transmit
   send_d2_cmd(TDM_Activation_Control,0x0001);
   send_d2_cmd(TDM_Tx_Configuration,0x9213);  // connection point 4 on dbmd2
   set_tdm_hw(DBMD2, D2_TDM_1_TX_ADDR ,0x0080001d , 0x00072027, 0x100f001f) ;
#endif
 
   send_d4_cmd(AUDIO_PROCESSING_CONFIGURATION,0x43);  // beam forming and enable vt
   send_d4_cmd(Audio_Streaming_Source_Selection,0xfff4);  // beam forming and enable vt
    
    // mic configuration
   send_d4_cmd(FIRST_MICROPHONE_CONFIGURATION,0x4076);     // connection point 0 on dbmd4
   send_d4_cmd(SECOND_MICROPHONE_CONFIGURATION,0xa061);	  // connection point 1 on dbmd4
   send_d4_cmd(THIRD_MICROPHONE_CONFIGURATION,0xa073);	  // connection point 3 on dbmd4
 
    // set tdm 1 transmit master 
   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Tx_Configuration,0xa203);      // connection point 0 ,1,2,3 on dbmd4 to TDM 1
   set_tdm_hw(DBMD4 , D4_TDM_1_TX_ADDR ,0x0080001a , 0x00073007, 0x101f003f) ;


   // set tdm 1 receive 

   send_d4_cmd(TDM_Activation_Control,0x0001);
   send_d4_cmd(TDM_Rx_Configuration,0x0013);      // connection point 4 on dbmd4 to TDM 1
   set_tdm_hw(DBMD4 , D4_TDM_1_RX_ADDR ,0x00800015 , 0x00070007, 0x101f003f) ;

 
   // set tdm 3 recieve  master 
   send_d2_cmd(TDM_Activation_Control,0x0002);    // connection point 0 on dbmd2to TDM 3
   send_d2_cmd(TDM_Rx_Configuration,0xa203);
   set_tdm_hw(DBMD2 , D2_TDM_2_RX_ADDR ,0x00800015 , 0x00000027, 0x101f003f) ;

      // set tdm 3 Transmit
  
   send_d2_cmd(TDM_Activation_Control,0x0002);    // connection point 3 on dbmd2 to TDM 3
   send_d2_cmd(TDM_Tx_Configuration,0x000f);
   set_tdm_hw(DBMD2 , D2_TDM_2_TX_ADDR ,0x00800015 , 0x00072007, 0x101f003f) ;

   return 0;
}

int use_case_4_exit()
{
	printf("[%s]: Start.\n", __func__);
	write_register_spi(spiD4, OPERATION_MODE, IDLE_MODE);
	printf ("[%s]: Finish!\n", __func__);
	return 0;
}

int use_case_5()
{
	printf("[%s]: Start.\n", __func__);
	write_register_spi(spiD4, TDM0_SCLK_Clock_Frequency, 0x0c00);
	delay(400);
	write_register_spi(spiD4, GENERAL_CONFIGURATION_2, 0x0280);
	write_register_spi(spiD4, AUDIO_PROCESSING_CONFIGURATION, 0x0000);

	write_register_spi(spiD4, DSP_Clock_Configuration_Extension, 0xd018);
	delay(400);

	write_register_spi(spiD4, TDM_Activation_Control,0x0001);
	write_register_spi(spiD4, TDM_Tx_Configuration,0xda03);
	set_tdm_hw(DBMD4, D4_TDM_1_TX_ADDR ,0x00800015, 0x00002027, 0x101f003f);
	set_tdm_hw_mcee(DBMD4, D4_TDM_1_TX_ADDR, 0x00000000);

	write_register_spi(spiD4, TDM_Activation_Control,0x0000);
	write_register_spi(spiD4, TDM_Tx_Configuration,0xda0b);
	set_tdm_hw(DBMD4, D4_TDM_0_TX_ADDR ,0x0080001d, 0x00002027, 0x101f003f);
	set_tdm_hw_mcee(DBMD4, D4_TDM_0_TX_ADDR, 0x00000000);

	write_register_spi(spiD4, 0x30, 0x0100);
	write_register_spi(spiD4, 0x30, 0x0200);
	write_register_spi(spiD4, 0x30, 0x0000);

	// D4 mic configuration
	write_register_spi(spiD4, FIRST_MICROPHONE_CONFIGURATION,0x50a6);
	write_register_spi(spiD4, SECOND_MICROPHONE_CONFIGURATION,0xa0a1);
	write_register_spi(spiD4, THIRD_MICROPHONE_CONFIGURATION,0xa0b3);

	write_register_spi(spiD4, TDM_Activation_Control, 0x0a04);
	delay(50);
	printf ("[%s]: Finish!\n", __func__);
	return 0;
}

int use_case_3_5_exit()
{
	printf("[%s]: Start.\n", __func__);
	write_register_spi(spiD4, TDM_Activation_Control,0x0000);
	write_register_spi(spiD4, TDM_Tx_Configuration,0x0000);
	write_register_spi(spiD4, TDM_Activation_Control,0x0001);
	write_register_spi(spiD4, TDM_Tx_Configuration,0x0000);

	write_register_spi(spiD4, THIRD_MICROPHONE_CONFIGURATION, 0x0);
	write_register_spi(spiD4, SECOND_MICROPHONE_CONFIGURATION, 0x0);
	write_register_spi(spiD4, FIRST_MICROPHONE_CONFIGURATION, 0x0);

	write_register_spi(spiD4, DSP_Clock_Configuration_Extension, 0xa00a);
	delay(600);
	read_register_spi(spiD4, DSP_Clock_Configuration_Extension);

	write_register_spi(spiD4, 0x2A, 0x0000);
	delay(100);
	write_to_i2c_bus(1, 0x0);
	delay(100);

	if(record_pid > 0) {
		system("killall -9 arecord");
	}

	delay(100);
	printf ("[%s]: Finish!\n", __func__);
	return 0;
}

int use_case_exit(int use_case)
{
	printf("[%s]: Start.\n", __func__);
	switch(use_case) {
		case 1:
			use_case_1_exit();
			break;
		case 2:
			use_case_2_exit();
			break;
		case 3:
			use_case_3_5_exit();
			break;
		case 4:
			use_case_4_exit();
			break;
		case 5:
			use_case_3_5_exit();
			break;
		default :
			printf ("[%s]: state = %d, do nothing.\n", __func__, use_case);
			break;
	}
	printf ("[%s]: Finish!\n", __func__);
	return 0;
}

unsigned long get_file_size(char *filename)
{
	FILE *file;
	unsigned long fileLen;

	// Open the file
	file = fopen(filename, "rb");
	if(!file) {
		fprintf(stderr, "Unable to open file %s\n", filename);
		return -1;
	}

	//Get file length
	fseek(file, 0, SEEK_END); 	// seek to the end of file
	fileLen = ftell(file); 		// get current file pointer
	fseek(file, 0, SEEK_SET); 	// seek back to beggining of file
	return fileLen;
}

void write_to_i2c_bus(int address, int param)
{
	int err;
	int i2c_address = 0x3E;
	int i2c_addressCPLD = 0x33;
	char buf[2];

	char *i2c_dev = "/dev/i2c-1" ;

	printf("[%s]: Start.\n", __func__);

	if (i2c)
		close(i2c);
	delay(100);

	// Open the I2C device
	i2c = open(i2c_dev, O_RDWR);
	if (i2c < 0)
	{
		printf("i2c < 0\n");
		exit(1);
	}
	tcdrain(i2c);
	// CPLD
	if((ioctl(i2c, I2C_SLAVE, i2c_addressCPLD)) < 0) {
		printf("ERROR Selecting I2C device\n");
		exit(2);
	}
	delay(10);

	buf[0] = address;
	buf[1] = param;
	write(i2c, buf, 2);

	delay(100);
	tcdrain(i2c);

	err = close(i2c);
	if (err)
		printf("ERROR writing I2C device %x",err);

	// Open the I2C device
	i2c = open(i2c_dev, O_RDWR);
	if(i2c < 0) {
		printf("i2c < 0\n");
		exit(1);
	}

	//select our slave device
	if((ioctl(i2c, I2C_SLAVE, i2c_address)) < 0)
		printf("ERROR Selecting I2C device\n");
	delay(1);
	printf("[%s]: Finish.\n", __func__);
}

#define COND_CHECK(func, cond, retv, errv) \
if ( (cond) ) \
{ \
   fprintf(stderr, "\n[CHECK FAILED at %s:%d]\n| %s(...)=%d (%s)\n\n",\
              __FILE__,__LINE__,func,retv,strerror(errv)); \
   exit(EXIT_FAILURE); \
}

#define ErrnoCheck(func,cond,retv)  COND_CHECK(func, cond, retv, errno)
#define PthreadCheck(func,rc) COND_CHECK(func,(rc!=0), rc, rc)

void detach_state(pthread_t tid,  // thread to check detach status
   const char *tname // thread name
   )
{
   int rc; // return code

   rc = pthread_join(tid, NULL);
   if(rc == EINVAL ) {
      printf("%s is detached\n", tname);
   } else if(rc == 0) {
      printf("%s was joinable\n", tname);
   } else {
      printf("%s: pthread_join() = %d (%s)\n", tname, rc, strerror(rc));
   }
}

void *interrupt_thread(void *ignore)
{
	int value, ret;
	int sv_score;

	signal(SIGINT, intHandler);
	
	do {
		ret = poll_pin(INTERRRUPT_TRIGGER_GPIO);
		if(ret != 1) {
			printf ("[%s]: poll_pin return \n", __func__);
			return NULL;
		}

		value = read_register_spi(spiD4, SENSORY_WORDID);
		sv_score = read_register_spi(spiD4, SENSORY_SCORE_VALUE);
		if(value != 0) {
			vt_cnt++;
			printf("\n\nNumber Of Detection : %d\n",vt_cnt);
			printf("D4 WordID 0x%X\n", value & 0xffff);	
			printf("Got a trigger! sv_score=%d\n\n", sv_score);		

			if(state == 1){
				write_register_spi(spiD4, OPERATION_MODE, DETECTION_MODE);
				delay(10);
			}else if(state == 2 || state == 3){
				char system_str[128] = {0};
				sprintf(system_str, "arecord -D plughw:CARD=sndrpihifiberry,DEV=0 -c 1 -f S16_LE -r 48000 -d 5 Audio_Buffer%d.wav", vt_cnt);
				system(system_str);
				//pull_buffer(2);
				printf("Audio Buffer saved\n");
				write_register_spi(spiD4, OPERATION_MODE, DETECTION_MODE);
				printf("\n");
				delay(10);
			}
		} else {
			printf("[%s]: Failed read WORD ID.\n", __func__);
			delay(3000);
		}
		fflush(stdout);		
		
	} while(1);

	return NULL;
}

void *user_space_playback_thread(void *path_to_song)
{	
	char system_str[64] = {0};

	DEBUG_PRINT("[%s]: user_space_playback_thread start.\n", __func__);
	
	while(music_on){
		DEBUG_PRINT("[%s]: music playback start. music_on=%d\n", __func__, music_on);
		//putenv("HOME=/home/pi/");
		sprintf(system_str, "aplay -vD hw:CARD=sndrpihifiberry,DEV=0 %s", (char *)path_to_song);
		int result = system(system_str);
        //if((WIFSIGNALED(result) == SIGINT) || (WIFSIGNALED(result) == SIGQUIT)) {
        if(WIFSIGNALED(result)) {
			printf("Exited with signal %d\n", WTERMSIG(result));
			music_on = 0;
          	intHandler(0);
        }
		DEBUG_PRINT("[%s]: music playback end\n", __func__);
	}

	DEBUG_PRINT("[%s]: user_space_playback_thread end\n", __func__);
	return NULL;
}
int user_space_playback_16b(const char *path_to_song)
{
#if 1
	pthread_t music_pid = 0;
	pthread_attr_t attr;
	int rc;

	rc = pthread_attr_init(&attr);
	PthreadCheck("pthread_attr_init", rc);
	rc = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	PthreadCheck("pthread_attr_setdetachstate", rc);

	rc = pthread_create(&music_pid, &attr, user_space_playback_thread, (void *)path_to_song);
	printf ("[%s]: music_pid = %d \n", __func__, (int)music_pid);
	PthreadCheck("pthread_create", rc);
	detach_state(music_pid, "music_pid");	
#else
	char system_str[64] = {0};
	if((music_pid = fork()) == 0) {
		setgid(1000);
		setuid(1000);
		while(music_on){
			DEBUG_PRINT("[%s]: I'm in the child. music_pid = %d start.\n", __func__, music_pid);
			//putenv("HOME=/home/pi/");
			sprintf(system_str, "aplay -vD hw:CARD=sndrpihifiberry,DEV=0 %s", path_to_song);
			system(system_str);
			DEBUG_PRINT("[%s]: I'm in the child. music_pid = %d.\n", __func__, music_pid);
		}
		exit(0);
	} else {
		DEBUG_PRINT("[%s]: I'm in the parrent. music_pid = %d.\n", __func__, music_pid);
	}
#endif	
	return 0;
}

int user_space_playback_32b(const char *path_to_song)
{
	char system_str[64] = {0};
	if((music_pid = fork()) == 0) {
		setgid(1000);
		setuid(1000);
		putenv("HOME=/home/pi/");
		sprintf(system_str, "aplay -vD plughw:CARD=sndrpihifiberry,DEV=0 %s", path_to_song);
		system(system_str);
		DEBUG_PRINT("[%s]: I'm in the child. music_pid = %d.\n", __func__, music_pid);
		exit(0);
	} else {
		DEBUG_PRINT("[%s]: I'm in the parrent. music_pid = %d.\n", __func__, music_pid);
	}

	return 0;
}

int user_space_record(const char *path_to_file, int rate, int duration, int bits)
{
	char system_str[64] = {0};
	if((record_pid = fork()) == 0) {
		setgid(1000);
		setuid(1000);
		putenv("HOME=/home/pi/");
		sprintf(system_str, "arecord -v -D dspg -r %d -f S%d_LE -d %d -c 2 %s", rate, bits, duration, path_to_file);
		system(system_str);
		exit(0);
	} else {
		DEBUG_PRINT("[%s]: I'm in the parrent. music_pid = %d.\n", __func__, record_pid);
	}

	return 0;
}

/*
		#########################################################
		####################### MAIN CODE #######################
		#########################################################
*/
int main(int argc, char **argv)
{
	int value = 0;
	pthread_t interrupt_pid = 0;
	pthread_attr_t attr;
	int rc;

	system("killall -9 aplay");
	system("killall -9 arecord");
	delay(1000);
	
	rc = pthread_attr_init(&attr);
	PthreadCheck("pthread_attr_init", rc);
	rc = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	PthreadCheck("pthread_attr_setdetachstate", rc);

	signal(SIGINT, intHandler);

	printf ("DBMD5 Demo with I2C/SPI transport channel.\n");

	//####### INIT RPI Board ########
	ASRP_run_on_chip = DBMD2;
	
	init_RPI_board();

	// init nuvoton codec 
	set_codec_i2c();
	
	//reset
	digitalWrite(RESET_GPIO, HIGH);
	digitalWrite(GPIO_25, HIGH);
	delay(600);

	Init_D4(SPI_INTERFACE);
	
	// START DBMD4
	Run_D4();
	delay(100);

	value = read_register_spi(spi, FW_VERSION_NUMBER);
	if(value != 0) {
		printf("[%s]: D4 Firmware version 0x%X\n", __func__, value & 0xffff);
	} else {
		printf("[%s]: Failed read D2 Firmware version \n", __func__);
		exit(0);
	}
	
	set_cs_d2();
	// load  & init DBMD2
	Init_D2(SPI_INTERFACE);
	// START DBMD2
	Run_D2();
	
	Config_D4();
	Config_D2();	
	
	// LOad VT acoustic model
	Load_A_Model();

	send_d4_cmd(SENSORY_DUALTHRESHOLD,0x0002);
	send_d4_cmd(SENSORY_POSTPARAMAOFFSET,0x04b0);	
	
	// verify DBMD2 is alive and read version
	value = read_register_spi(spi1, 0x19);
	if (value != 0)	{
		printf ("*********************************\n");
		printf ("*********************************\n");
		printf ("******     D2 IS RUNNING     ****\n");
		printf ("*********************************\n");
		printf ("*********************************\n");
		printf ("Chip version = 0x%04X\n", value & 0xffff);

	} else {
		printf ("FAIL read_registerI2C(0x19)\n");
	}
	
	value = read_register_spi(spi1, FW_VERSION_NUMBER);
	if(value != 0) {
		printf ("FirmWare version = 0x%04X\n", value);

	} else {
		printf ("FAIL read_registerI2C(FW_VERSION_NUMBER)\n");
	}

	// verify DBMD4 is alive and read version
	delay(1);
	value = read_register_spi(spi, 0x19);
	if(value != 0) {
		printf ("*********************************\n");
		printf ("*********************************\n");
		printf ("******     D4 IS RUNNING     ****\n");
		printf ("*********************************\n");
		printf ("*********************************\n");
		printf ("Chip version = 0x%04X\n", value & 0xffff);
	} else {
		printf ("FAIL read_register(0x19)\n");
	}
	
	delay(1);
	value = read_register_spi(spi, FW_VERSION_NUMBER);
	if(value != 0) {
		printf ("FirmWare version = 0x%X\n", value & 0xffff);
	} else {
		printf ("FAIL read_register(FW_VERSION_NUMBER)\n");
	}
	
	gpio_platform_to_irq(INTERRRUPT_TRIGGER_GPIO, 1);
	printf("Ready for action !\n\n");

	rc = pthread_create(&interrupt_pid, &attr, interrupt_thread, NULL);
	printf ("[%s]: interrupt_pid = %d \n", __func__, (int)interrupt_pid);
	PthreadCheck("pthread_create", rc);
	detach_state(interrupt_pid, "interrupt_pid");	
	
	print_logo();
	usage();
	char line[1024] = {0};
	while(keepRunning) {
		//int ret = -1;
		fgets(line, 1024, stdin);
		if(strncmp(line, "DM0", strlen("DM0")) == 0) {
		  	use_case_0(); // IDLE Mode
			printf("\n\n########  DM0 Demo ##########\n\n");
		}else if(strncmp(line, "DM1", strlen("DM1")) == 0) {
			use_case_exit(state);
			use_case_1();
			state = 1;
			printf("\n\n########  DM1 Demo ##########\n\n");
		}else if(strncmp(line, "DM2", strlen("DM2")) == 0) {
			//use_case_0();
		  	use_case_2();
			state = 2;
		  	printf("\n\n########  DM2 Demo ##########\n\n");			   
		}else if(strncmp(line, "DM3", strlen("DM3")) == 0) {
			//use_case_exit(state);		
			//use_case_0();
			use_case_3();
			state = 3;			
			printf("\n\n########  DM3 Demo ##########\n\n");
		} else if(strncmp(line, "use case l3", strlen("use case l3")) == 0) {
			  use_case_3_long();
#if 1
			   pull_buffer(2);
#else
		 write_registerI2C(TDM_Activation_Control,0xc908);
		 write_register(TDM_Activation_Control,0x0C04);
#endif
		}else if(strncmp(line, "use case l1", strlen("use case l1")) == 0){
		  	use_case_1_a();
		  	pull_buffer(1);
		}else if(strncmp(line, "use case s2", strlen("use case s2")) == 0) {
			  use_case_2_16();
			  change_clock_src ( 16 , 16 );
 
			   /* activate D2 */
			  write_registerI2C(TDM_Activation_Control,0x0908);
			   /* record channel */
		}else if(strncmp(line, "use case l2", strlen("use case l2")) == 0) {
			  use_case_2_long();
			  change_clock_src ( 48 , 32 );
 
		  /* activate D2 */
			  write_register_spi(spi1, TDM_Activation_Control,0x8108);
			   /* record channel */
		} else if(strncmp(line, "use case 4", strlen("use case 4")) == 0) {
			  use_case_4();
//			  pull_buffer(17);
			  write_registerI2C(TDM_Activation_Control,0x3b08);
			  write_register(TDM_Activation_Control,0x0C04);
		} else if(strncmp(line, "bypass", strlen("bypass")) == 0) {
			  set_hardware_bypass();
		} else if(strncmp(line, "log on", strlen("log on")) == 0) {
			  write_registerI2C(0x18, 0x5);
			    write_register(0x18, 0x5);

		}
		else if(strncmp(line, "log off", strlen("log off")) == 0) {
			   write_registerI2C(TDM_Activation_Control, 0x0);
			   write_register(TDM_Activation_Control, 0x0);

		}

		else if(strncmp(line, D4FWVER, strlen(D4FWVER)) == 0) {
			value = read_register(FW_VERSION_NUMBER);
			if (value >= 0)
			{
				printf ("D4 FirmWare version = 0x%X\n", value);

			}
			else
			{
				printf ("FAIL read_register(FW_VERSION_NUMBER)\n");
				//serialClose(serial);
			}
		} else if(strncmp(line, D2FWVER, strlen(D2FWVER)) == 0) {
			value = read_registerI2C(FW_VERSION_NUMBER);
			if ((value&0xffff) >= 0)
			{
				printf ("D2 FirmWare version = 0x%X\n", (value&0xffff));

			}
			else
			{
				printf ("FAIL read_register(FW_VERSION_NUMBER) value = %d\n", (value&0xffff));
			}
		} else if(strncmp(line, D2OPMODE, strlen(D2OPMODE)) == 0) {
			value = read_register_spi(spi1, 0x01);
			if ((value & 0xffff) >= 0) {
				printf ("D2 Operation mode = 0x%X\n", (value&0xffff));

			} else {
				printf ("FAIL errno = %d D2 read_register 0x01\n", errno);
			}
		} else if(strncmp(line, D4OPMODE, strlen(D4OPMODE)) == 0) {
			value = read_register_spi(spi, 0x01);
			if(value >= 0) {
				printf ("D4 Operation mode = 0x%X\n", value);
			} else {
				printf ("FAIL D4 read_register 0x01\n");
			}
		}else if(strncmp(line, RECD4, strlen(RECD4)) == 0) {
		   write_register(0x5,0x000f);
		   write_register(0x6,0x000);
		   write_registerI2C(0x7, 0x0);
		   write_register(0x30,0x21);
		} else if(strncmp(line, RSTOP, strlen(RSTOP)) == 0) {
			   write_register(0x30,0x0);
			   write_registerI2C(0x30,0x0);
		} else if(strncmp(line, RECASRP, strlen(RECASRP)) == 0) {
			   write_registerI2C(0x5,0x000);
			   write_registerI2C(0x6,0x7700);
			   write_registerI2C(0x7, 0x0);
			   write_registerI2C(0x30, 0x09);
			} else if(strncmp(line, RECD2, strlen(RECD2)) == 0) {
			   write_registerI2C(0x5,0x3800);
			   write_registerI2C(0x6,0x0000);
			   write_registerI2C(0x7, 0x0);
			   write_registerI2C(0x30, 0x09);
			}  else if(strncmp(line, "exit", 4) == 0 ||
				  strncmp(line, "q", 1) == 0) {
			break;
		} else { /* default */
			printf ("Invalid command: %s\n", line);
#if 0

			write_register_spi(spi1, 0x18,0x1);
			delay(80);
			write_register_spi(spi1, 0x18,0x2);
			delay (80);
			write_register_spi(spi, 0x18,0x1);
			delay(80);
			write_register_spi(spi, 0x18,0x2);
			delay(480);
			write_register_spi(spi, 0x18,0x13);
			write_register_spi(spi1, 0x18,0x13);
			delay(280);
			write_register_spi(spi1, 0x18,0x10);
			delay(80);
			write_register_spi(spi1, 0x18,0x11);
 #endif 
		}
	}


	//spi_release(spi);
	//spi_release(spi1);
	//spi_release(i2c);
	intHandler(0);
	return 0;
}


void testcolor(int attr, int fg, int bg)
{
	char command[13];
	sprintf(command, "%c[%d;%d;%dm", 0x1B, attr, fg+30, bg+40);
	printf("%s", command);
}


void print_logo()
{
testcolor(BRIGHT, GREEN, BLACK);
printf("**********************************************************************\n");
printf("*                           \\\\\\|/// \n");
printf("*                         \\\\  ~ ~  // \n");
printf("*                          (  @ @  ) \n");
printf("*------------------------oOOo-(_)-oOOo--------------------------------\n");
printf("* \n");
printf("* DBMDx Test Application: enter command and press Enter or exit, or Ctrl+C\n");
printf("*\n");
printf("*---------------------------------Oooo.-------------------------------\n");
printf("*                       .oooO     (   ) \n");
printf("*                       (   )      ) / \n");
printf("*                        \\ (      (_/ \n");
printf("*                         \\_) \n");
printf("**********************************************************************\n");
testcolor(RESET, WHITE, BLACK);
}
