#!/usr/bin/env python

# Version 1.5
# 21-DEC-2016

import os.path
import sys
import time
import RPi.GPIO as GPIO
from smbus import SMBus
import serial
import spidev
from subprocess import call
import datetime
import wave
import thread
import subprocess
import shlex
import gc
import shutil
from array import *
from abc import ABCMeta, abstractmethod

execfile ("Devices.py")



class Driver (object):
	@staticmethod
	def Reset(device):
		if (device == dbmd4):
			#~ device.Wakeup()
			GPIO.output(RESET_GPIO, False)
			time.sleep(0.01)
			GPIO.output(RESET_GPIO, True)
			time.sleep(0.01)
			
			# turn leds off
			GPIO.output(LED_GPIO,False)
			write_to_log ("Reset the chip successfully")
		else:
			device.Wakeup()
			GPIO.output(RESET_GPIO_D2, False)
			time.sleep(0.01)
			GPIO.output(RESET_GPIO_D2, True)
			time.sleep(0.10)
			
	@staticmethod
	def ReadRegister(device, reg_address, use_wakeup=None):
		reg_value = device.ReadRegister(reg_address, use_wakeup)
		if (type(reg_address) == str):
			reg_address = hex(int (reg_address, 16))[2:].zfill(3)
		elif (type(reg_address) == int):
			reg_address = hex(reg_address)[2:].zfill(3)
		write_to_log("Reading reg: 0x" + reg_address + " ; value: 0x" + str(reg_value))
		return reg_value

	@staticmethod
	def ReadRegisterShort(device, reg_address, use_wakeup=None):
		reg_value = device.ReadRegister(reg_address, use_wakeup)
		return reg_value
	
	@staticmethod
	def ReadIOPort (device, reg_address, use_wakeup=None):
		global READ_IO_IN_PROGRESS	
		while (1):
			if (READ_IO_IN_PROGRESS == False):
				READ_IO_IN_PROGRESS = True
				
				if ((type(reg_address) == long) or (type(reg_address) == int)) :
					reg_address = (hex(reg_address)[2:10]).zfill(8)
				else:
					reg_address = (str(reg_address)).zfill(8)
				
				address_msb = reg_address [:4]
				address_lsb = reg_address [4:8]
				if use_wakeup:
					device.Wakeup()
				device.WriteRegister(IO_PORT_ADDRESS_LOW, address_lsb, 0, False)
				device.WriteRegister(IO_PORT_ADDRESS_HIGH, address_msb, 0, False)
				value_lsb = device.ReadRegister(IO_PORT_VALUE_LOW, False)
				value_msb = device.ReadRegister(IO_PORT_VALUE_HIGH, False)
				reg_value = str(value_msb) + str(value_lsb)
				
				if use_wakeup:
					device.Sleep()
				
				if STATISTIC:	
					write_to_log("Reading reg: 0x" + str(reg_address) + " ; value: 0x" + reg_value)
					
				READ_IO_IN_PROGRESS = False
				return reg_value
			else:
				write_to_log("reading in use... please wait...")			
				time.sleep(0.01)

	@staticmethod
	def ReadBytes(device, num_of_bytes, use_wakeup=None):
		value = device.ReadBytes(num_of_bytes, use_wakeup=None)
		return value
	
	@staticmethod
	def WriteRegister(device, reg_address, reg_value, sleep_time=None, use_wakeup=None):
		device.WriteRegister(reg_address, reg_value, sleep_time, use_wakeup)
		
		if (type(reg_address) == str):
			reg_address = hex(int (reg_address, 16))[2:].zfill(3)
		elif (type(reg_address) == int):
			reg_address = hex(reg_address)[2:].zfill(3)
		
		if (type(reg_value) == str):
			reg_value = hex(int (reg_value, 16))[2:].zfill(4)
		elif (type(reg_value) == int):
			reg_value = hex(reg_value)[2:].zfill(4)
		write_to_log("Writing reg: 0x" + reg_address + " ; value: 0x" + str(reg_value))
		Driver.ReadRegister(device, reg_address)	
	
	@staticmethod
	def WriteRegisterShort(device, reg_address, reg_value, sleep_time=None, use_wakeup=None):
		device.WriteRegister(reg_address, reg_value, sleep_time, use_wakeup)
		
		if (type(reg_address) == str):
			reg_address = hex(int (reg_address, 16))[2:].zfill(3)
		elif (type(reg_address) == int):
			reg_address = hex(reg_address)[2:].zfill(3)
		
		if (type(reg_value) == str):
			reg_value = hex(int (reg_value, 16))[2:].zfill(4)
		elif (type(reg_value) == int):
			reg_value = hex(reg_value)[2:].zfill(4)
			
		if STATISTIC:
			write_to_log("Writing reg: 0x" + reg_address + " ; value: 0x" + reg_value)
				
	@staticmethod
	def WriteBytes(device, data_list, use_wakeup=None):
		device.WriteBytes(data_list, use_wakeup=None)

	@staticmethod			
	def WriteIOPort (device, reg_address, reg_value, sleep_time=None, use_wakeup=None):
		global WRITE_IO_IN_PROGRESS
		while (1):
			if (WRITE_IO_IN_PROGRESS == False):
				WRITE_IO_IN_PROGRESS = True

				if ((type(reg_address) == long) or (type(reg_address) == int)) :
					reg_address = hex(reg_address)[2:10]
				if ((type(reg_value) == long) or (type(reg_value) == int)):
					reg_value = hex(reg_value)[2:10]
				
				reg_address = (str(reg_address)).zfill(8)	
				address_msb = reg_address [:4]
				address_lsb = reg_address [4:8]
				reg_value = (str(reg_value)).zfill(8)		
				value_msb = reg_value [:4]
				value_lsb = reg_value [4:8]
				
				if (sleep_time == None):
					sleep_time = DEFAULT_SLEEP_TIME
				elif ((type(sleep_time) == str) or ((type(sleep_time) == int)) or ((type(sleep_time) == float))):
					sleep_time = float(sleep_time)
				
				if use_wakeup:
					device.Wakeup()		
				device.WriteRegister(IO_PORT_ADDRESS_LOW, address_lsb, 0, False)
				device.WriteRegister(IO_PORT_ADDRESS_HIGH, address_msb, 0, False)
				device.WriteRegister(IO_PORT_VALUE_LOW, value_lsb, 0, False)
				device.WriteRegister(IO_PORT_VALUE_HIGH, value_msb, 0, False)
				if (sleep_time):
					time.sleep(sleep_time)
				
				if use_wakeup:
					device.Sleep()
				
				if STATISTIC:
					write_to_log("Writing reg: 0x" + reg_address + " ; value: 0x" + reg_value)
					Driver.ReadIOPort (device, reg_address, use_wakeup)
				
				WRITE_IO_IN_PROGRESS = False
				break
			else:
				write_to_log("writing in use... please wait...")			
				time.sleep(0.01)	
	
	@staticmethod
	def PreLoadTriggerModel(device, trigger_model):
		if (trigger_model in LIST_OF_VALUES):
			trigger_model = LIST_OF_VALUES[trigger_model]
			
		if os.path.isfile(trigger_model):
			size = str(hex((os.path.getsize(trigger_model))/16+3).rstrip("L")[2:])
			Driver.WriteRegisterShort(device, PRIMARY_ACOUSTIC_MODEL_SIZE, size)
		else:
			write_to_log("Trigger Acoustic model not found: " + trigger_model)
	
	@staticmethod
	def PreLoadCommandModel(device, command_model):
		if os.path.isfile(command_model):
			size = str(hex((os.path.getsize(command_model))/16+3).rstrip("L")[2:])
			Driver.WriteRegisterShort(device, SECONDARY_ACOUSTIC_MODEL_SIZE, size)
		else:
			write_to_log("Command Acoustic model not found: " + command_model)
	
	@staticmethod		
	def LoadFile(device, filename):
		if not(os.path.isfile(filename)):
			write_to_log("File not found !")
					
		write_to_log("About to load file: " + str(filename))
		device.LoadFile (filename)
		write_to_log (filename + " loaded successfully")
		
	@staticmethod		
	def LoadFw(device, fw_filename):
		if not(os.path.isfile(fw_filename)):
			write_to_log("Firmware file not found !")
					
		write_to_log("Booting Starts ...")
		write_to_log("About to load FW file: " + str(fw_filename))
		device.LoadFile (fw_filename, 4)
		time.sleep(0.05)
		
		# Checksum test
		checksum_from_fw = device.ReadChecksumFromFw()
		calculated_checksum = calc_checksum(fw_filename, 4)
		checksum_from_file = read_checksum_from_fw_file(fw_filename)
		write_to_log("The checksum for FirmWare is: " + str(checksum_from_fw))
		if ((checksum_from_file == checksum_from_fw) and (checksum_from_fw == calculated_checksum)):
			write_to_log ("FW: Checksum test - Pass")
		else:
			raise Exception ("FW: Checksum test - Fail")
			
		device.ActivateBinFile()
		time.sleep(0.05)
		
		# verify boot succeeded
		fw_id = device.ReadRegister(FW_ID_NUMBER)
		fw_version = device.ReadRegister(FW_VERSION_NUMBER)
		
		if ((fw_id[:3]).upper() == 'DBD'):
			thread.start_new_thread(blink_LED,(4, 0)) # blink the led to inform the user that the FW loaded succssefully
			write_to_log ("Chip Type = " + str(fw_id) + ", FW version: " + str(fw_version) + ", Interface: " + str(device) )
			write_to_log ("FW file " + fw_filename + " loaded successfully")
			write_to_log("Boot Succeeded")	
		else:	
			raise Exception ("Boot Failed")

	@staticmethod
	def LoadModel(device, model_filename, mode):
		if (model_filename in LIST_OF_VALUES):
			model_filename = LIST_OF_VALUES[model_filename]
		
		if os.path.isfile(model_filename):
			device.WriteRegister (LOAD_NEW_ACOUSTIC_MODEL, mode)
			time.sleep(0.01)
			write_to_log("About to load bin file: " + str(model_filename))
			if USE_WAKEUP:
				device.Wakeup()
			device.LoadModelFile(model_filename, 2)
			
			# Checksum test
			calculated_checksum = calc_checksum(model_filename, 2)
			checksum_from_fw = device.ReadChecksumFromFw()
			write_to_log("The checksum for Acoustic model is: " + str(checksum_from_fw))
			
			if (calculated_checksum == checksum_from_fw):
				write_to_log ("Acoustic Model: Checksum test - Pass")
			else:
				raise Exception ("Acoustic Model: Checksum test - Fail")
			
			time.sleep(0.05)
			device.ActivateBinFile()
			
			mode= int(mode)
			if (mode == 0): # for trigger model
				#time.sleep(0.1)
				value = device.ReadRegister(SENSORY_INITIALIZED)
				if (int(value) == 0):
					raise Exception ("Trigger acoustic model failed to load")
			
			elif (mode == 4): # for ASRP params
				value = device.ReadRegister(0X101)
				if (value == "ffff"):
					raise Exception ("ASRP Params file failed to load")
			
			write_to_log("Acoustic model loaded successfully: " + model_filename)
			if USE_WAKEUP:
				device.Sleep()
		else:
			write_to_log("Acoustic model not found: " + model_filename)
	
	@staticmethod
	def LoadTriggerModel(device, trigger_model):
		Driver.LoadModel(device, trigger_model, "0")
	
	@staticmethod	
	def LoadSplitTriggerModel(device, am_file, search_file):
		if (create_acoustic_model(am_file, search_file)):
			Driver.LoadModel(device, "A-Model.bin" , "0")
	
	@staticmethod		
	def LoadCommandModel(device, command_model):
		Driver.LoadModel(device, command_model, "1")
	
	@staticmethod		
	def LoadGoogleModel(device, google_model):
		Driver.LoadModel(device, google_model, "4")
	
	@staticmethod	
	def LoadAsrpParam(device, asrp_param):
		Driver.LoadModel(device, asrp_param, "4")

	@staticmethod
	def Stream (device, time_in_sec, sample_rate, channels, file_name=RAW_AUDIO_FILENAME):
		device.WriteRegister(OPERATION_MODE, BUFFERING_MODE)
		Driver.DumpBuffer(device, time_in_sec, sample_rate, channels, file_name)

	@staticmethod
	def EnterHibernate(device):		
		global MIC24
		global MIC25
		global MIC26
		value_24 = device.ReadRegister(FIRST_MICROPHONE_CONFIGURATION)
		if(value_24 == "0000"):
			write_to_log("The value of reg 0x24 is '0x0000'")
		else:
			MIC24 = value_24
			MIC25 = device.ReadRegister(SECOND_MICROPHONE_CONFIGURATION)
			MIC26 = device.ReadRegister(THIRD_MICROPHONE_CONFIGURATION)
			
			write_to_log("Reg 0x24 = 0x" + MIC24)
			write_to_log("Reg 0x25 = 0x" + MIC25)		
			write_to_log("Reg 0x26 = 0x" + MIC26)	
				
			write_to_log("Mic settings saved")
			
		device.WriteRegister(THIRD_MICROPHONE_CONFIGURATION, "0")
		device.WriteRegister(SECOND_MICROPHONE_CONFIGURATION, "0")
		device.WriteRegister(FIRST_MICROPHONE_CONFIGURATION, "0")
		#if USE_WAKEUP:
		device.Wakeup()
		device.Sleep()
		device.WriteRegister(OPERATION_MODE, HIBERNATE_MODE, None, False)
		write_to_log("Entering hibernate mode.... bye....")
	
	@staticmethod	
	def ExitHibernate(device):
		device.Wakeup()
		time.sleep(0.01)
		device.WriteRegister(FIRST_MICROPHONE_CONFIGURATION, MIC24)
		device.WriteRegister(SECOND_MICROPHONE_CONFIGURATION, MIC25)
		device.WriteRegister(THIRD_MICROPHONE_CONFIGURATION, MIC26)
		write_to_log("Mic settings configured")	
		write_to_log("Exiting hibernate mode.... bye....")

	@staticmethod
	def DumpBuffer (device, time_in_sec, sample_rate, number_of_channels, file_name=RAW_AUDIO_FILENAME):
		global STOP_STREAMING_FLAG
		STOP_STREAMING_FLAG = False
		retry_counter = 0

		write_to_log("Start dumping")
		wanted_fifo_size = int(16*(round(float(time_in_sec), 2))*(int(sample_rate))*(int(number_of_channels))/8) # size in Bytes
		current_fifo_size= 0		# size in Bytes
		
		device_type = str(device)
		if (device_type == "UART"):
			max_bytes_to_read_from_buffer = 2304 	#0x900
			delta = 0
		elif (device_type == "I2C"):
			max_bytes_to_read_from_buffer = 512		#0x200
			delta = 0
		elif (device_type == "SPI"):
			max_bytes_to_read_from_buffer = 1020 	#0x3fc 
			delta = 2
		
		
		data_array_from_buffer = ''
		data_list = ''
		audio_file = open (file_name, "wb")
		
		if USE_WAKEUP:
			device.Wakeup()
		while(current_fifo_size < wanted_fifo_size):
			if (STOP_STREAMING_FLAG == True): 
				write_to_log("User has requested to interrupt the streaming")
				break

			try:
				fw_buffer_size = device.ReadRegister(0x0a, False)
				
				if STATISTIC:
					write_to_log("buffer size = " + str(fw_buffer_size))
				
				if ((fw_buffer_size == '0000') and (retry_counter < 10) ):
					retry_counter +=1
					continue
				elif (retry_counter >= 10): 
					write_to_log("Buffer is full of Zero !\n ")
					break
				else:
					retry_counter = 0
					
				if (fw_buffer_size == 'ffff'):
					write_to_log("Buffer is empty !")
					break

				fw_buffer_size_int = int(fw_buffer_size, 16)
				fw_buffer_size_bytes = 16*fw_buffer_size_int
				
			except Exception, e:
				write_to_log ("Error while reading data: " + str(e))
				continue
			
			if (fw_buffer_size_int < max_bytes_to_read_from_buffer) :
				device.WriteRegister(READ_AUDIO_BUFFER, fw_buffer_size, None, False)
				data = device.ReadBytes(fw_buffer_size_bytes+delta, False)
				current_fifo_size += fw_buffer_size_bytes
				
				data_len = len(data)
				if (data_len-(delta/2) != fw_buffer_size_bytes):
					write_to_log ("Error ! missing " + str( fw_buffer_size_bytes - data_len +(delta/2) ) + " bytes")
				
			else:
				device.WriteRegister(READ_AUDIO_BUFFER, hex(max_bytes_to_read_from_buffer), None, False)
				data = device.ReadBytes(max_bytes_to_read_from_buffer*16+delta, False)
				current_fifo_size += 16*max_bytes_to_read_from_buffer
				
				data_len = len(data)
				if (data_len-(delta/2) != 16*max_bytes_to_read_from_buffer):
					write_to_log ("Error ! missing " + str( 16*max_bytes_to_read_from_buffer - data_len +(delta/2) ) + " bytes")
			
			data = data[(delta/2):]
			audio_file.write(data)
			
		time.sleep(0.01)
		audio_file.close()
		if USE_WAKEUP:
			device.Sleep()
		write_to_log("Dumping Finished")

	@staticmethod
	def ChangeInterfaceSpeed(device, interface_speed):
		device.ChangeInterfaceSpeed(interface_speed)
	
	@staticmethod
	def StartDigitalRecording(debug_device):
		print "debug_device =", debug_device
		global RECORDING_TO_FILE
		RECORDING_TO_FILE = True
		write_to_log("Start digital recording")
		thread.start_new_thread(debug_device.WriteToFile,())
		
	@staticmethod
	def StopDigitalRecording():
		global RECORDING_TO_FILE
		RECORDING_TO_FILE =	False
		
		# wait to finish writing to file
		while (not debug_log_file.closed):
			time.sleep(0.1)
			
		write_to_log("Stop digital recording")

	@staticmethod
	def PreBootDevice (device, data, use_wakeup=None):
		device.WriteBytes(data, use_wakeup)
		time.sleep(0.001)
		
		# Clear checksum
		#device.WriteBytes([0x5A, 0x0F], use_wakeup)
		
