from smbus import SMBus
i2c = SMBus(1)

i2c_addr = 0x1a


def nuv_write(addr,value):
	addr = addr << 1;
	x = value >> 8
	addr = x + addr;
	i2c.write_byte_data(i2c_addr, addr, value)
	return

nuv_write(0x0,0x0); # Reset
nuv_write(0x1,0x3b); # Power management 1
nuv_write(0x4,0x090); #I2S, stereo
nuv_write(0x5,0x0); #No passthrough
nuv_write(0x7,0x0); #48KHz filter
nuv_write(0xf,0x8); #Left ADC volume
nuv_write(0x10,0x8); #Right ADC volume
nuv_write(0xe,0x8); #oversampling for better SNR 
nuv_write(0x2f,0x100); #Left PGA boost
nuv_write(0x30,0x100); # Right PGA boost
nuv_write(0x0a,0x0); # DAC control - no SW/auto mute, no oversampling, normal polarities
nuv_write(0x2,0x1bf); #Power management 2
nuv_write(0x3,0x6f); # Power management 3
nuv_write(0x2c,0x33); # microphones control (go through PGA)
nuv_write(0x2d,0x110); # Left PGA gain
nuv_write(0x2e,0x110); # Right PGA gain
nuv_write(0x6,0x4D); # Set PLL for 48KHz
nuv_write(0x34,0x9); # Set left HP volume
nuv_write(0x31,0x1e); # 5V
nuv_write(0x45,0x5); # 5V
nuv_write(0x32, 0x15) # Left Bypass path control from LADC to LMAIN
nuv_write(0x33, 0x15) # Right Bypass path control from RADC to RMAIN
nuv_write(0xb, 0x1ff) # Left DAC volume - no attenuation
nuv_write(0xc, 0x1ff) # Right DAC volume - no attenuation

print "#### CODEC IS UP #####"
