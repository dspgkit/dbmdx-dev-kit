Two host reference code:
- Python
		-	Edit Parameter.py, change Demo_Mode = "DM1" to one of following value:
				DM1   - Voice Trigger D4&D5
				DM2   - Voice Trigger and Buffering   D4&D5
				DM3   - Music, Voice Trigger and Buffering    D5
		-	At the bash shell of user pi, run "python demo.py"
		- Say ��Hello Blue Genie��
		- Check the terminal that it gets voice trigger interrupt notification.
		- For case DM2/DM3, check the buffered audio files with name Audio_Buffer*.wav
		-	Please don't run "python demo.py" as root or sudo 

- C
		- Compile with make
		-	Run sudo ./demo
		- After the initalization, enter one of the following commands to select the  
				DM1   - Voice Trigger D4&D5
				DM2   - Voice Trigger and Buffering   D4&D5
				DM3   - Music, Voice Trigger and Buffering    D5
		- Say ��Hello Blue Genie��
		- Check the terminal that it gets voice trigger interrupt notification.				
		- For case DM2/DM3, check the buffered audio files with name Audio_Buffer*.wav
		